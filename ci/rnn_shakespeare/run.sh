#!/usr/bin/bash
set -ex
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export ROOT=$CWD/../..

echo "========== Tokenizing text =========="
mkdir -p $CWD/data
text_tokenizer $ROOT/data/shakespeare/shakespeare.txt \
    -o $CWD/data/shakespeare.npy \
    -o_meta $CWD/data/shakespeare_metadata.json \
    --encoding character \
    --mode ascii \
    --overwrite

echo "========== Pre-training model =========="
lm_pretrain \
    --config_path $CWD/cfgs/rnn.yaml \
    --dataset_path $CWD/data/shakespeare.npy \
    --tokenizer_metadata_path $CWD/data/shakespeare_metadata.json \
    --experiments_dir $CWD/experiments

echo "========== Testing inference with pre-trained model =========="
pre_ckpt=$(find $CWD/experiments/rnn/ -name "last.ckpt" | sort | tail -n 1)
lm_inference $pre_ckpt --seed "Who are you?" --n 100

echo "========== Supervised fine tuning (SFT) from pre-trained model =========="
lm_chat_ft_train \
    --dataset_path $ROOT/data/chat_shakepseare/chat_shakespeare.json \
    --validation_set_path $ROOT/data/chat_shakepseare/chat_shakespeare.json \
    --fine_tune_from $pre_ckpt \
    --experiments_dir $CWD/experiments/rnn_chat_ft \
    --batch_size 2 \
    --max_epochs 2

echo "========== Testing chatting with SFT model =========="
ft_ckpt=$(find $CWD/experiments/rnn_chat_ft/ -name "last.ckpt" | sort | tail -n 1)
echo -n "hello" | lm_chat $ft_ckpt
