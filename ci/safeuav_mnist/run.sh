#!/usr/bin/bash
set -ex
export CWD=$(dirname $(readlink -f ${BASH_SOURCE[0]}))
export ROOT=$CWD/../..

export MAX_EPOCHS=1
export BATCH_SIZE=50

$ROOT/mlexp/vision_models/pretraining/pretrain.py \
  --config_path $CWD/cfgs/safeuav_pretrain.yaml \
  --dataset mnist \
  --experiments_dir $CWD/experiments
ckpt_pretrain=$(find $CWD/experiments/ -name "last.ckpt" | grep safeuav_pretrain | sort | tail -n 1)

$ROOT/mlexp/vision_models/classification/train.py \
  --config_path $CWD/cfgs/safeuav.yaml \
  --dataset mnist \
  --experiments_dir $CWD/experiments \
  --fine_tune_from $ckpt_pretrain

ckpt=$(find $CWD/experiments/ -name "last.ckpt" | grep -v safeuav_pretrain | sort | tail -n 1)
$ROOT/mlexp/vision_models/classification/evaluate.py \
  $ckpt \
  --replace_cfg data.loader_params.batch_size 100
