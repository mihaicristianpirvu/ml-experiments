#!/usr/bin/env python3
from datasets import load_dataset
import sys
import numpy as np
from tqdm import tqdm
import json

def main():
    assert sys.argv[1] in ("train", "validation"), sys.argv[1]
    assert sys.argv[2][-5:] == ".json", sys.argv[2]
    dataset = load_dataset("OpenAssistant/oasst2")
    print(sys.argv[1])
    data = dataset[sys.argv[1]]

    texts = np.array(data["text"])
    trees = np.array(data["message_tree_id"])
    langs = np.array(data["lang"])
    message_ids = np.array(data["message_id"])
    parent_ids = np.array(data["parent_id"])

    langs_ix = langs == "en"
    print(f"Kept {langs_ix.sum()} out of {len(langs)} english texts")
    langs, trees, texts, message_ids, parent_ids = langs[langs_ix], trees[langs_ix], texts[langs_ix], \
        message_ids[langs_ix], parent_ids[langs_ix]
    diffs = [0, *np.where(trees[1:] != trees[0:-1])[0] + 1, len(langs)]

    res = []
    n_qa, n_ch = 0, 0
    for l, r in tqdm(zip(diffs[0:-1], diffs[1:]), total=len(diffs) - 1):
        assert all(trees[l:r] == trees[l])
        tree_ids = message_ids[l: r - 1]
        tree_parents = parent_ids[l: r - 1]
        tree_texts = texts[l: r - 1]

        id_to_parent = {_id: parent for _id, parent in zip(tree_ids, tree_parents)}
        id_to_text = {_id: text for _id, text in zip(tree_ids, tree_texts)}
        num_refs = {_id: 0 for _id in tree_ids}
        num_refs[None] = 0
        skip_tree = False
        for parent in tree_parents:
            try:
                num_refs[parent] += 1
            except KeyError:
                print(f"Id '{parent}' doesn't exist for tree '{trees[l]}'")
                skip_tree = True
        if skip_tree:
            continue
        leafs = [k for k, v in num_refs.items() if v == 0]

        res_current_tree = []
        for leaf in leafs:
            current = leaf
            leaf_current = []
            while current is not None:
                leaf_current.append(id_to_text[current])
                current = id_to_parent[current]
            # if the last question was not answered, remove it as we want to end with responses always.
            if len(leaf_current) % 2 == 1:
                leaf_current = leaf_current[1:]
            if len(leaf_current) > 1:
                res_current_tree.append(leaf_current[::-1])
            n_qa, n_ch = n_qa + len(leaf_current), n_ch + sum(len(x) for x in leaf_current)
        res.extend(res_current_tree)
    print(f"n_conversations: {len(res)}, avg n_QA: {n_qa/len(res):2f}, n_ch: {n_ch/len(res):2f}")

    res_final = []
    for qa in res:
        this_res = []
        for i, text in enumerate(qa):
            this_res.append({('question' if i % 2 == 0 else 'answer'): text})
        res_final.append(this_res)
    open(sys.argv[2], "w").write(json.dumps(res_final, indent=4))
    print(f"Written to '{sys.argv[2]}' ({len(res_final)} rows)")

if __name__ == "__main__":
    main()
