#!/usr/bin/env python3
from datasets import load_dataset
import json
import sys
from pathlib import Path
from tqdm import trange

def main():
    # for gpt-2 1024 context length -> around 4000 max, conservatively use 3000.
    assert len(sys.argv) == 4, "Usage: prepare.py train/validation train/val.json MAX_SEQ [~=4*ctx length]"
    dataset_type, out_file, max_seq = sys.argv[1], sys.argv[2], int(sys.argv[3])
    assert max_seq > 0, max_seq
    assert not Path(out_file).exists(), f"Exists: '{out_file}'"
    assert dataset_type in ("train", "validation"), dataset_type
    dataset = load_dataset("HuggingFaceH4/ultrachat_200k")
    data = dataset["train_sft"] if sys.argv[1] == "train" else dataset["test_sft"]

    res = []
    n_ch, n_conv = 0, 0
    for i in trange(len(data)):
        item = data[i]["messages"]
        if len(item) <= 1:
            continue
        item = item[0:-1] if len(item) % 2 == 1 else item # remove last question to end with answers always
        contents = [{("question" if i % 2 == 0 else "answer"): x["content"]} for i, x in enumerate(item)]
        n_chs = [len(next(iter(x.values()))) for x in contents]
        while sum(n_chs) > max_seq and len(n_chs) > 0:
            n_chs = n_chs[0:-2]
        contents = contents[0: len(n_chs)]
        if len(contents) == 0:
            continue
        res.append(contents)
        n_conv, n_ch = n_conv + len(contents), n_ch + sum(n_chs)
        if (i % 10_000 == 0 and i != 0):
            open(out_file, "a").write(json.dumps(res, indent=4))
            print(f"Written to '{out_file}'. Rows: {len(res)}. Avg ch: {n_ch/len(res):.2f}, "
                  f"conv: {n_conv/len(res):.2f}")
            res, n_ch, n_conv = [], 0, 0
    if len(res) > 0:
        open(out_file, "a").write(json.dumps(res, indent=4))
        print(f"Written to '{out_file}'. Rows: {len(res)}. Avg ch: {n_ch/len(res):.2f}, conv: {n_conv/len(res):.2f}")

if __name__ == "__main__":
    main()
