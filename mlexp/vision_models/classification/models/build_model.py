"""Build models for classification"""
from pathlib import Path
from functools import partial
from typing import Callable
import json
import torch as tr
from torch import nn
from torch.nn import functional as F
from omegaconf import DictConfig
from lightning_module_enhanced import LME, ModelAlgorithmOutput
from torchvision.models.mobilenetv3 import mobilenet_v3_small
from loggez import loggez_logger as logger

from mlexp.utils import patch_tensor
from .vit import VITFeaturizer, VITInputHandler
from .safeuav import SafeUAVEncoder, conv

def print_diff(a: DictConfig, b: DictConfig):
    _str = "Diffs:"
    diff_keys_a, diff_keys_b = set(a.keys()).difference(b.keys()), set(b.keys()).difference(a.keys())
    if len(diff_keys_a) > 0: _str += f"\n- Keys in a only: {diff_keys_a}"
    if len(diff_keys_b) > 0: _str += f"\n- Keys in a only: {diff_keys_b}"
    for k in set(a.keys()).intersection(b.keys()):
        if a[k] != b[k]: _str += f"\n- Key: '{k}'. Values: '{a[k]}' != '{b[k]}'"
    return _str

class SafeUAVClassificationDecoder(nn.Module):
    def __init__(self, d_out: int, num_filters: int, n_convs: int, input_shape: tuple[int, int, int]):
        assert input_shape[1] >= 2**n_convs, f"Input shape {input_shape} too small for {n_convs} convs."
        assert input_shape[2] >= 2**n_convs, f"Input shape {input_shape} too small for {n_convs} convs."
        self.d_out = d_out
        self.num_filters = num_filters
        self.input_shape = input_shape
        super().__init__()

        self.convs = nn.ModuleList()
        for i in range(n_convs):
            self.convs.append(conv(d_in=num_filters, d_out=num_filters if i < n_convs - 1 else d_out,
                                   kernel_size=3, padding=1, stride=1, dilation=1))
        x = int(input_shape[1] / (2**n_convs)) * int(input_shape[2] / (2**n_convs))
        self.fc_out = nn.Linear(d_out * x, d_out)

    def forward(self, x_enc: tr.Tensor) -> tr.Tensor:
        y = x_enc
        for _conv in self.convs:
            y = F.max_pool2d(_conv(y), kernel_size=(2, 2))
        y_out = self.fc_out(y.flatten(start_dim=1))
        return y_out

    def __str__(self):
        return f"Decoder Map2Map. d_out: {self.d_out}. NF: {self.num_filters}."

class VitClassificationDecoder(nn.Module):
    def __init__(self, emb_size: int, num_classes: int, classification_mode: str, hidden_shapes: list[int]):
        super().__init__()
        assert len(hidden_shapes) > 0, hidden_shapes
        self.classification_mode = classification_mode

        hidden_shapes = [emb_size, *hidden_shapes]
        self.fcs = nn.ModuleList(nn.Linear(l, r, bias=False) for l, r in zip(hidden_shapes[0:-1], hidden_shapes[1:]))
        self.fc_out = nn.Linear(hidden_shapes[-1], num_classes, bias=False)

        if classification_mode == "class_token":
            self.ln_blocks = nn.LayerNorm(emb_size)

    def forward(self, x_enc: tr.Tensor) -> tr.Tensor:
        # Classifier part at the end of the transformer
        # Global pool doesn't have the last ln_block (https://arxiv.org/pdf/2104.02057.pdf, page 6)
        if self.classification_mode == "class_token":
            x_enc = self.ln_blocks(x_enc)
            y_classification = x_enc[:, 0]
        elif self.classification_mode == "global_pool":
            y_classification = x_enc.mean(dim=1)

        y_fc_blocks = y_classification
        for fc in self.fcs:
            y_fc_blocks = F.relu(fc(y_fc_blocks))
        y_out = self.fc_out(y_fc_blocks)
        return y_out

def vit_algorithm(model: LME, batch: dict, patch_size: tuple[int, int]) -> ModelAlgorithmOutput:
    """
    Generic step for computing the forward pass, loss and metrics. Simple feed-forward algorithm by default.
    Must return a dict of type: {metric_name: metric_tensor} for all metrics.
    'loss' must be in there as well unless you update `training_step` as well in your module.
    """
    x = patch_tensor(batch["data"], patch_size=patch_size).permute(0, 4, 1, 2, 3) # (B, T, C, ph, pw)
    y_enc = model.encoder(x)
    y_dec = model.decoder(y_enc)
    metrics = model.lme_metrics(y_dec, batch["labels"], include_loss=True)
    return y_dec, metrics, (x, y_enc), batch["labels"]

def _build_vit(cfg: DictConfig, classes: list[str], images_shape: tuple[int, int, int] | tuple[int, int],
               weights_path: Path | None) -> nn.Module:
    n_ch = 1 if len(images_shape) == 2 else images_shape[-1]
    assert len(cfg.train.patch_size) == 2 and all(isinstance(x, int) for x in cfg.train.patch_size)
    patch_shape: tuple[int, int, int] = (n_ch, cfg.train.patch_size[0], cfg.train.patch_size[1])
    # sequence size is kinda fixed for VIT, differently from language modeling
    sequence_size = images_shape[0] // cfg.train.patch_size[0] * images_shape[1] // cfg.train.patch_size[1]
    if weights_path is not None:
        data = tr.load(weights_path, map_location="cpu")
        loaded_cfg = DictConfig(json.loads(data["hyper_parameters"]["model_cfg"])).model.encoder
        # this is most likely false in pre-training but can be in FT (if not using global pool)
        loaded_cfg.input_handler.use_class_token = cfg.model.encoder.input_handler.use_class_token
        assert loaded_cfg.parameters == cfg.model.encoder.parameters, (loaded_cfg.encoder, "\n", cfg.model.encoder)
        assert (_a := loaded_cfg.input_handler) == (_b := cfg.model.encoder.input_handler), print_diff(_a, _b)
        assert (_a := loaded_cfg.parameters) == (_b := cfg.model.encoder.parameters), print_diff(_a, _b)

    encoder = VITFeaturizer(VITInputHandler(patch_shape=patch_shape, max_sequence_size=sequence_size,
                                            **cfg.model.encoder.input_handler), **cfg.model.encoder.parameters)
    classification_mode = "class_token" if encoder.vit_input_handler.use_class_token else "global_pool"
    decoder = VitClassificationDecoder(emb_size=cfg.model.emb_size, num_classes=len(classes),
                                       classification_mode=classification_mode, **cfg.model.decoder.parameters)
    if weights_path is not None:
        sd = data["state_dict"]
        relevant_data = {k.removeprefix("encoder."): v for k, v in sd.items() if k.startswith("encoder.")}
        if "vit_input_handler.class_token" in encoder.state_dict().keys():
            relevant_data["vit_input_handler.class_token"] = encoder.vit_input_handler.class_token
        encoder.load_state_dict(relevant_data, strict=True)

    return nn.ModuleDict({"encoder": encoder, "decoder": decoder})

def _build_safeuav(cfg: DictConfig, classes: list[str], images_shape: tuple[int, int, int] | tuple[int, int],
                   weights_path: Path | None) -> nn.Module:
    n_ch = 1 if len(images_shape) == 2 else images_shape[-1]
    input_shape = (n_ch, *images_shape[0:2])
    encoder = SafeUAVEncoder(d_in=n_ch, **cfg.model.encoder.parameters)
    decoder = SafeUAVClassificationDecoder(d_out=len(classes), input_shape=input_shape, **cfg.model.decoder.parameters)

    if weights_path is not None:
        data = tr.load(weights_path, map_location="cpu")["state_dict"]
        relevant_data = {k.removeprefix("encoder."): v for k, v in data.items() if k.startswith("encoder.")}
        encoder.load_state_dict(relevant_data, strict=True, assign=True)
    return nn.Sequential(encoder, decoder)

def build_model(cfg: DictConfig, classes: list[str], images_shape: tuple[int, int, int],
                weights_path: Path | None = None) -> nn.Module:
    """builds the model for the classification task"""
    n_ch = 1 if len(images_shape) == 2 else images_shape[-1]
    logger.info(f"Loading weights from '{weights_path}'") if weights_path is not None else None
    if cfg.model.type == "vit":
        model = _build_vit(cfg, classes, images_shape, weights_path)
    elif cfg.model.type == "mobilenet_v3_small":
        assert weights_path is None, f"Cannot fine-tune mobilenet. Got {weights_path=}."
        model = mobilenet_v3_small(num_classes=len(classes), **cfg.model.parameters)
        if n_ch != 3:
            # we need this to be able to train a mobilenet on grayscale images
            model.features[0][0] = nn.Conv2d(in_channels=n_ch, out_channels=16, kernel_size=(3, 3),
                                                  stride=(2, 2), padding=(1, 1), bias=False)
    elif cfg.model.type == "safeuav":
        model = _build_safeuav(cfg, classes, images_shape, weights_path)
    else:
        raise ValueError(f"Unknown model type: '{cfg.model.type}'")

    return model

def build_model_algorithm(cfg: DictConfig) -> Callable:
    if cfg.model.type == "vit":
        return partial(vit_algorithm, patch_size=tuple(cfg.train.patch_size))
    elif cfg.model.type == "mobilenet_v3_small":
        return lambda model, batch: (y := model(batch["data"]), model.lme_metrics(y, batch["labels"]), *batch.values())
    elif cfg.model.type == "safeuav":
        return lambda model, batch: (y := model(batch["data"]), model.lme_metrics(y, batch["labels"]), *batch.values())
    else:
        raise ValueError(f"Unknown model type: '{cfg.model.type}'")
