# Cassification models

Various benchmarks are maintained in this Google Sheet: [link](https://docs.google.com/spreadsheets/d/1g1KO0dXhIqGhnmWet7_B54T9Mkut1gx1fTgDI_Put6U/edit?usp=sharing).

# Models

Implemented (or used) models:
- [Visual Transformer](models/vit.py). Based on the [original paper](https://arxiv.org/pdf/2010.11929v2.pdf) and my
[transformer implementation](../../models/transformer.py).
- [MobileNetV3](models/build_model.py). Simply used from `torchvision` as baseline.
- [SafeUAV](../../models/safeuav.py). Based on the [original paper](https://openaccess.thecvf.com/content_ECCVW_2018/papers/11130/Marcu_SafeUAV_Learning_to_estimate_depth_and_safe_landing_areas_for_ECCVW_2018_paper.pdf). The original architecture
only does a final conv1d to process the encoder into the final number of channels for regression or
pixel-classification. I have adapted it for classification by adding a small convnet + fc decoder after the SafeUAV
encoder.

# Training

Command:
```bash
./train.py --config_path cfgs/vit_87k_classtoken_4x4.yaml --dataset DATASET [--fine_tune_from /path/to/pretrained.ckpt] [--experiments_dir DIR]
```

Valid dataset (`--dataset`): `cifar10`, `mnist`.

TODO: It's not yet possible to use other dataset without any extra coding.

See `cfgs/` for some implemented configs.

# Evaluation

```bash
./evaluate.py WEIGHTS_PATH [--batch_size N] [--experiments_dir DIR]
```

This will evaluate the checkpoint on the test set of the `--dataset`.

TODO: It's not yet possible to evaluate on other dataset without extra coding.
