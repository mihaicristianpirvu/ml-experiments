#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace
from pathlib import Path
from datetime import datetime
from omegaconf import OmegaConf
from loggez import loggez_logger as logger
import sys
import json
import torch as tr
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger, WandbLogger
from pytorch_lightning.utilities import rank_zero_only
from torch.nn import functional as F
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, PlotCallbackGeneric
from torchmetrics.functional import accuracy, f1_score
from omegaconf import OmegaConf
from functools import partial

from mlexp.utils import to_image, image_resize, image_add_title, image_write, collage_fn

from readers import build_reader, build_train_val_readers
from models import build_model, build_model_algorithm

class ClassificationPlotCallback(PlotCallbackGeneric):
    """Above implementation + assumption about data/labels keys"""
    def __init__(self, classes: list[str]):
        self.classes = classes

    def _do_call(self, pl_module: LME, batch: dict, batch_idx: int, key: str, n_batches: int):
        if batch_idx != 0:
            return
        if len(pl_module.trainer.loggers) == 0:
            logger.warning("No lightning logger found. Not calling PlotCallbacks()")
            return
        prediction: tr.Tensor = self._get_prediction(pl_module)[0]
        out_file = Path(f"{pl_module.trainer.loggers[0].log_dir}/pngs/{key}/{pl_module.trainer.current_epoch + 1}.png")
        out_file.parent.mkdir(exist_ok=True, parents=True)
        MB = len(prediction)
        x = batch["data"].cpu()
        y = prediction.softmax(dim=-1).detach().cpu()
        gt = batch["labels"].cpu()
        res = []
        for i in range(min(MB, 25)):
            ix = gt[i].item()
            prediction = y[i][ix]
            img = image_resize(to_image(x[i]), height=120, width=120)
            img = image_add_title(img, f"{self.classes[ix]} ({prediction:.2f})", size_px=20, top_padding=15)
            res.append(img)
        collage = collage_fn(res, rows_cols=(5, 5))
        image_write(collage, out_file)

def load_for_fine_tune(model: LME, weights_path: Path):
    logger.info(f"Fine tuning from '{weights_path}'")
    state_dict = tr.load(weights_path, map_location="cpu")["state_dict"]
    model_type = model.hparams["model_cfg"]["model"]["type"]
    if any(lambda x: "decoder" in x for x in state_dict.keys()):
        logger.info(f"Loading a pre-trained model. Keeping only the encoder. Type: '{model_type}'")
        if model_type == "vit":
            enc_state_dict = {k: v for k, v in state_dict.items() if "decoder" not in k}
            enc_state_dict = {k.replace("encoder.", ""): v for k, v in enc_state_dict.items()}
            model.base_model.vit_featurizer.load_state_dict(enc_state_dict, strict=True)
        elif model_type == "safeuav":
            enc_state_dict = {k: v for k, v in state_dict.items() if "decoder" not in k}
            enc_state_dict = {k.replace("encoder.", ""): v for k, v in enc_state_dict.items()}
            model.base_model.encoder.load_state_dict(enc_state_dict, strict=True)
    else:
        logger.info("Loading a pre-trained model without a decoder. Loading the whole model")
        model.load_state_dict(state_dict, strict=True)

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True)
    parser.add_argument("--dataset", choices=["mnist", "cifar10"], required=True)
    parser.add_argument("--experiments_dir", type=Path)
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--fine_tune_from", type=Path, help="Path to model to fine tune from")
    group.add_argument("--resume_from", type=Path, help="Path to model to resume from")
    parser.add_argument("--wandb_project", help="If set, will use wandb to log the training process")
    parser.add_argument("--wandb_run", help="If set, will use or reuse an existing version. Used to resume trains")
    args = parser.parse_args()
    if args.experiments_dir is None:
        args.experiments_dir = Path(__file__).parent / "experiments" / args.dataset
    return args

def main():
    """main fn"""
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    base_reader = build_reader(args.dataset)

    model = LME(build_model(cfg, classes=(classes := base_reader.classes),
                            images_shape=(images_shape := base_reader.images_shape), weights_path=args.fine_tune_from))
    model.model_algorithm = build_model_algorithm(cfg)
    model.optimizer = getattr(tr.optim, cfg.train.optimizer.type)(model.parameters(), **cfg.train.optimizer.parameters)
    model.metrics = {"accuracy": (partial(accuracy, task="multiclass", num_classes=len(classes)), "max"),
                     "f1": (partial(f1_score, task="multiclass", num_classes=len(classes), average="macro"), "max")}
    model.criterion_fn = lambda y, gt: F.cross_entropy(y.reshape(-1, y.shape[-1]), gt.reshape(-1))
    model.checkpoint_monitors = ["loss", "accuracy", "f1"]
    model.callbacks = [PlotMetrics(), ClassificationPlotCallback(classes=classes)]
    model.hparams["model_cfg"] = json.dumps(OmegaConf.to_container(cfg, resolve=True))
    model.hparams["cli"] = [str(x) for x in sys.argv]
    model.hparams["dataset"] = args.dataset
    model.hparams["classes"] = tuple(classes)
    model.hparams["images_shape"] = images_shape
    model.to("cuda" if tr.cuda.is_available() else "cpu")
    print(model.summary)

    train_reader, val_reader = build_train_val_readers(base_reader, cfg.train.train_val_ratio)
    train_loader = DataLoader(train_reader, **{**cfg.data.loader_params, "shuffle": True})
    val_loader = DataLoader(val_reader, **cfg.data.loader_params)

    pl_loggers = [CSVLogger(save_dir=f"{args.experiments_dir}/{args.config_path.stem}",
                            name=datetime.strftime(datetime.now(), "%Y%m%d"),
                            version=datetime.strftime(datetime.now(), "%H%M%S"))]
    Path(pl_loggers[0].log_dir).mkdir(parents=True, exist_ok=True)
    OmegaConf.save(cfg, f"{pl_loggers[0].log_dir}/config.yaml", resolve=True)
    if args.wandb_project and rank_zero_only.rank == 0:
        wandb_logger = WandbLogger(project=args.wandb_project, version=args.wandb_run)
        wandb_logger.experiment.config.update(OmegaConf.to_container(cfg), allow_val_change=True)
        pl_loggers.append(wandb_logger)
    Trainer(logger=pl_loggers, **cfg.train.trainer_params).fit(model, train_loader, val_loader)

if __name__ == "__main__":
    main()
