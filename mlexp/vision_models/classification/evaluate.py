#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from functools import partial
import torch as tr
import json
from torch.nn import functional as F
from omegaconf import DictConfig
from loggez import loggez_logger as logger
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from datetime import datetime
from torch.utils.data import DataLoader
from torchmetrics.functional import accuracy, f1_score
from lightning_module_enhanced import LME

from mlexp.utils import oc_argparse_overwrite

from models import build_model, build_model_algorithm
from readers import build_reader

def get_args():
    parser = ArgumentParser()
    parser.add_argument("weights_path", type=Path)
    parser.add_argument("--experiments_dir", type=Path)
    parser.add_argument("--replace_cfg", action="append", nargs="*", default=[])
    args = parser.parse_args()
    if args.experiments_dir is None:
        args.experiments_dir = Path(__file__).parent / "experiments"
    return args

@tr.no_grad
def main():
    args = get_args()
    ckpt_data = tr.load(args.weights_path, map_location="cpu")
    hparams = ckpt_data["hyper_parameters"]
    cfg = oc_argparse_overwrite(DictConfig(json.loads(hparams["model_cfg"])), args)

    model = LME(build_model(cfg, classes := hparams["classes"], images_shape=hparams["images_shape"]))
    model.load_state_dict(ckpt_data["state_dict"], strict=True)
    model.model_algorithm = build_model_algorithm(cfg)
    model.metrics = {"accuracy": (partial(accuracy, task="multiclass", num_classes=len(classes)), "max"),
                     "f1": (partial(f1_score, task="multiclass", num_classes=len(classes), average="macro"), "max")}
    model.criterion_fn = lambda y, gt: F.cross_entropy(y.reshape(-1, y.shape[-1]), gt.reshape(-1))
    model.eval().to("cuda" if tr.cuda.is_available() else "cpu")

    print(model.summary)
    logger.info(f"Loaded weights from '{args.weights_path}'")

    test_reader = build_reader(hparams["dataset"], train=False)
    test_loader = DataLoader(test_reader, batch_size=cfg.train.batch_size, shuffle=False)
    pl_logger = CSVLogger(save_dir=f"{args.experiments_dir}/test",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))
    Trainer(logger=pl_logger).test(model, test_loader)

if __name__ == "__main__":
    main()
