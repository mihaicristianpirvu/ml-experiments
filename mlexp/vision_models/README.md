# Vision Models

This submodule handles the implementation of various computer vision models, such as Convolutional Neural Networks,
Vision Transformers. The [classification](./classification-models) module supports training of such models on
classification tasks. For now, we work with MNIST and CIFAR10 only. The [pretraining](./pretraining) module supports
unsupervised pre-training tasks, such as [Masked Auto Encoders](./pretraining/algorithms/). Encoders of pretrained
models can work out of the box on the classification tasks by loading only the pretrained weights. See
`--fine_tune_from` mode in the [training script](./classification-models/train.py).

Supported models can be found [here](./models).

The general flow of this code is the following: Given a [config file](./classification-models/cfgs/safeuav_1.1M.yaml)
one can use the [train script](./classification-models/train.py) for a given dataset, resulting in a best checkpoint
file in a unique experiment dir (usually timestamped and dataset & model specific). Then, using only this best
checkpoint (w.r.t the desired metric(s)), one can use the [evaluation script](./classification-models/evaluate.py)
for a specific dataset (usually the test set of the same dataset that was used for training). All the model-specific
information required to load the checkpoint and set the right hyperparameters should be handled by the evaluation
script, we only need to point to the checkpoint file.

Important experiments are also logged in wandb ([example](https://wandb.ai/meehai/cifar10_benchmarks)), as well as
written in the README file ([example](./classification-models/README.md)).

