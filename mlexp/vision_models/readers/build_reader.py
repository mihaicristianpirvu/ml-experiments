"""dataset builder"""
from loggez import loggez_logger as logger
from torch.utils.data import Dataset, Subset
from torchvision.datasets import MNIST, CIFAR10

from mlexp.utils import get_project_root

from .basic_reader import BasicReader

def build_reader(dataset_type: str, **kwargs) -> BasicReader:
    """builds a bare reader given a dataset type. Only 'mnist' and 'cifar10' supported so far"""
    root = get_project_root() / "data" / dataset_type
    if dataset_type == "mnist":
        dataset = MNIST(root=root, download=True, **kwargs)
    elif dataset_type == "cifar10":
        dataset = CIFAR10(root=root, download=True, **kwargs)
    else:
        raise ValueError(f"Unknown dataset type: '{dataset_type}'")
    logger.info(f"Loaded {type(dataset)} with {len(dataset)} samples")
    return BasicReader(dataset, classes=dataset.classes, images_shape=dataset.data.shape[1:])

def build_train_val_readers(dataset: Dataset, train_val_ratio: float) -> tuple[Dataset, Dataset]:
    """get train and val readers using Subset and a fixed split ratio"""
    assert hasattr(dataset, "classes"), f"Dataset '{dataset}' must have a 'classes' attribute"
    train_reader = Subset(dataset, range(0, int(len(dataset) * train_val_ratio)))
    val_reader = Subset(dataset, range(int(len(dataset) * train_val_ratio), len(dataset)))
    train_reader.classes = dataset.classes
    val_reader.classes = dataset.classes
    logger.info(f"Split {type(dataset)} in train: {len(train_reader)} and val: {len(val_reader)}")
    return train_reader, val_reader
