"""BasicReader"""
from torchvision.transforms.functional import pil_to_tensor
from torch.utils.data import Dataset
import torch as tr

class BasicReader(Dataset):
    """BasicReader"""
    def __init__(self, base_reader: Dataset, classes: list[str], images_shape: tuple[int, ...]):
        self.base_reader = base_reader
        self.classes = classes
        self.images_shape = images_shape

    def __getitem__(self, ix: int) -> dict[str, tr.Tensor]:
        res = self.base_reader[ix]
        x = pil_to_tensor(res[0]).float() / 255
        return {"data": x, "labels": res[1]}

    def __len__(self) -> int:
        return len(self.base_reader)
