#!/usr/bin/env python3
"""Visual Transformer implementation for classification"""
from __future__ import annotations
import math
import torch as tr
from torch import nn

from mlexp.models.transformer import Transformer

class PositionalEncoding1D(nn.Module):
    """Pre-compute positional encodings using sin/cos"""
    def __init__(self, sequence_size: int, emb_size: int):
        super().__init__()
        assert emb_size % 2 == 0, "Must be odd for sin/cos positional encoding"
        self.register_buffer("pe", tr.zeros(sequence_size, emb_size))
        position = tr.arange(0, sequence_size).unsqueeze(1).float()
        div_term = (tr.arange(0, emb_size, 2, dtype=tr.float) * -(math.log(10000) / emb_size)).exp()
        self.pe[:, 0::2] = tr.sin(position * div_term)
        self.pe[:, 1::2] = tr.cos(position * div_term)

    def __call__(self, ix: tr.Tensor) -> tr.Tensor:
        return self.pe[ix.to("cpu")]

    def __repr__(self):
        return f"[PositionalEncoding1D] sequence_size={self.pe.shape[0]} emb_size={self.pe.shape[1]}"

class VITInputHandler(nn.Module):
    """
    Handles the inputs from patches, text or w/e to embeddings that are then passing through the attention layers.
    Parameters:
    - patch_shape: The input patch shape as tuple (c, p_h, p_w).
    - emb_size: The shape of the embedding after projecting the input. This is maintained in all the blocks.
    - max_sequence_size: The maximum sequence size handled by transformer (based on number of patches/tokens).
    - use_class_token: Whether to add one special token for classification in downstream tasks.
    - use_patch_projection: Whether to add a linear layer that converts from (M, B, *patch_shape) to (M, B, E) before
    passing through the transformer block. This is usually done if we stack multiple VITFeaturizers together and the
    output of the first is already embedded.
    - positional_encoding_type. Encoding the positions using sum. Possible options: 'embedding', 'sin_cos' or 'none'.
    """
    def __init__(self, patch_shape: tuple[int, int, int] | None, emb_size: int, max_sequence_size: int,
                 use_class_token: bool, use_patch_projection: bool, positional_encoding_type: str):
        super().__init__()
        assert positional_encoding_type in ("embedding", "sin_cos", "none"), positional_encoding_type
        self.patch_shape = patch_shape
        self.emb_size = emb_size
        self.max_sequence_size = max_sequence_size
        self.use_class_token = use_class_token
        self.use_patch_projection = use_patch_projection
        self.positional_encoding_type = positional_encoding_type

        # Other params, like patch_projection, positional embeddings/encodings our output class token
        if self.use_patch_projection:
            self.patch_projection = nn.Linear(math.prod(patch_shape), emb_size, bias=False)
        else:
            assert patch_shape is None, "'patch_shape' mustn't be set if 'use_patch_projection' is False"
        self.positional_encoding = None
        if self.positional_encoding_type == "embedding":
            self.positional_encoding = nn.Embedding(max_sequence_size, emb_size)
        elif self.positional_encoding_type == "sin_cos":
            self.positional_encoding = PositionalEncoding1D(max_sequence_size, emb_size)
        if self.use_class_token:
            self.class_token = nn.Parameter(tr.randn(1, 1, emb_size) * 0.02)

    def forward(self, x: tr.Tensor, x_t: tr.Tensor | None = None) -> tr.Tensor:
        """Preprocessing of the input tensor (positional encoding and patch projection). x :: (B, t, [HxWxC])"""
        device = next(self.parameters()).device
        B, t, E = *x.shape[0:2], self.emb_size
        assert t <= self.max_sequence_size, (x.shape, self.max_sequence_size)
        assert ((pos_enc := self.positional_encoding_type) == "none" and x_t is None) or pos_enc != "none"
        x_emb = x_flat = x.view(B, t, -1)
        # In case of transformer decoders, we already get embedded patches from the encoder, so no need to project them
        if self.use_patch_projection:
            x_emb = self.patch_projection(x_flat) # (B, t, emb_size)
        assert x_emb.shape == (B, t, E), f"Wrong shapes: {x_emb.shape} vs {(B, t, E)}"

        x_pos = 0
        if pos_enc in ("sin_cos", "embedding"):
            x_t = tr.arange(t).to(device) if x_t is None else x_t # (1, t) with values [0, 1, ..., t-1]
            x_t = x_t.repeat(B, 1) if len(x_t.shape) == 1 else x_t # (B, t) with repeated values
            assert len(x_t.shape) == 2 and x_t.shape[1] == t, (x.shape, len(x_t))
            x_pos = self.positional_encoding(x_t).to(device) # (B, t, emb_size)

        x_emb_pos = x_emb + x_pos
        if self.use_class_token: # Note: we don't position embed the class token.
            cls_tok_expanded = self.class_token.repeat(x_emb_pos.shape[0], 1, 1)
            x_emb_pos = tr.cat([cls_tok_expanded, x_emb_pos], dim=1) # (B, t+1, emb_size)
        return x_emb_pos

class VITFeaturizer(nn.Module):
    """
    Generic VIT Featurizer. Takes a batch of sequences (B, t, *patch_shape) and outputs a batch of shape (B, t, E).
    Parameters:
    - vit_input_handler: The class that processes the data for the transformer layers.
    - All other params are forwarded to Transformer class.
    """
    def __init__(self, vit_input_handler: VITInputHandler, **kwargs):
        super().__init__()
        self.vit_input_handler = vit_input_handler
        self.transformer = Transformer(emb_size=vit_input_handler.emb_size, **kwargs)
        self.apply(self.transformer._init_weights)

    def forward(self, x: tr.Tensor, x_t: tr.Tensor | None = None,
                kv: tr.Tensor | None = None) -> tr.Tensor | list[tr.Tensor]:
        """
        Parameters:
        - x: The patches/tokens forwarded to the attention blocks. Shape: (B, t, *patch_size).
        - x_t: The positions of the tokens. If not provided, will be assumed to be range(t). Shape: (t, ) or (B, t).
        - kv: The key-value tensor from an encoder in an encoder-decoder model. Passed to the first transformer block.
        Note: the kv tensor can have a different sequence size (i.e. t2 != t) as it is assumed to come from oter seq.
        Returns: The tensor of the last transformer block or a list of all transformer blocks outputs if
        return_all_states is True.
        """
        x_emb_pos = self.vit_input_handler(x, x_t) # (B, t, E)
        return self.transformer(x_emb_pos, kv=kv) # (B, t, E)

def test_vit_handler():
    B, t, E, T_max = 5, 8, 100, 16
    patch_shape = (3, 8, 8)

    vih1 = VITInputHandler(patch_shape=patch_shape, emb_size=E, positional_encoding_type="embedding",
                           use_class_token=False, use_patch_projection=True, max_sequence_size=T_max)
    assert (y1 := vih1(x1 := tr.randn(B, t, *patch_shape))).shape == (B, t, E), f"{x1=}, {y1=}"
    assert vih1(tr.randn(B, T_max, *patch_shape)).shape == (B, T_max, E)
    try:
        _ = vih1(tr.randn(B, T_max + 1, *patch_shape))
    except AssertionError as e:
        assert f"{T_max + 1} <= {T_max}" in f"{e}"

    vih2 = VITInputHandler(patch_shape=patch_shape, emb_size=E * 2, positional_encoding_type="embedding",
                           use_class_token=False, use_patch_projection=True, max_sequence_size=T_max)
    assert (y2 := vih2(x2 := tr.randn(B, t // 2, *patch_shape))).shape == (B, t // 2, E * 2), f"{x2=}, {y2=}"

    try:
        _ = vih2(tr.randn(B, t // 2, E))
    except RuntimeError as e:
        assert "mat1 and mat2 shapes cannot be multiplied" in f"{e}"

    try:
        VITInputHandler(patch_shape=None, emb_size=E, positional_encoding_type="embedding",
                        use_class_token=False, use_patch_projection=True, max_sequence_size=T_max)
    except TypeError as e:
        assert "'NoneType' object is not iterable" in f"{e}"

    try:
        VITInputHandler(patch_shape=patch_shape, emb_size=E, positional_encoding_type="embedding",
                        use_class_token=False, use_patch_projection=False, max_sequence_size=T_max)
    except AssertionError as e:
        assert "'patch_shape' mustn't be set if 'use_patch_projection' is False" in f"{e}"

    vih3 = VITInputHandler(patch_shape=None, emb_size=E, positional_encoding_type="embedding",
                           use_class_token=False, use_patch_projection=False, max_sequence_size=T_max)
    try:
        _ = vih3(tr.randn(B, t, E * 2))
    except AssertionError as e:
        assert f"Wrong shapes: torch.Size([{B}, {t}, {E*2}]) vs ({B}, {t}, {E})" in f"{e}"
    assert (y3 := vih3(x3 := tr.randn(B, t, E))).shape == (B, t, E), f"{x3=}, {y3=}"

    vih4_1 = VITInputHandler(patch_shape=patch_shape, emb_size=E * 2, positional_encoding_type="sin_cos",
                             use_class_token=False, use_patch_projection=True, max_sequence_size=T_max)
    vih4_2 = VITInputHandler(patch_shape=patch_shape, emb_size=E * 2, positional_encoding_type="none",
                             use_class_token=False, use_patch_projection=True, max_sequence_size=T_max)
    assert (y4_1 := vih4_1(x4_1 := tr.randn(B, t, *patch_shape))).shape == (B, t, E * 2), f"{x4_1=}, {y4_1=}"
    assert (y4_2 := vih4_2(x4_2 := tr.randn(B, t, *patch_shape))).shape == (B, t, E * 2), f"{x4_2=}, {y4_2=}"

    # x_t testing
    assert (y1_xt1 := vih1(x1, x_t1 := tr.arange(t).repeat(B, 1))).shape == (B, t, E), f"{x1=}, {x_t1=}, {y1_xt1=}"
    assert (y1_xt2 := vih1(x1, x_t2 := tr.arange(t))).shape == (B, t, E), f"{x1=}, {x_t2=}, {y1_xt2=}"
    try:
        _ = vih1(x1, tr.arange(t // 2))
    except AssertionError:
        pass

    vih5 = VITInputHandler(patch_shape=patch_shape, emb_size=E, positional_encoding_type="embedding",
                           use_class_token=True, use_patch_projection=True, max_sequence_size=T_max)
    assert (y5 := vih5(x5 := tr.randn(B, t, *patch_shape))).shape == (B, t + 1, E), f"{x5=}, {y5=}"
