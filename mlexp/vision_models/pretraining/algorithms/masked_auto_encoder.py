"""Masked Auto Encoder (MAE) algorithm for various supported models for computer vision"""
from math import ceil
import torch as tr
from lightning_module_enhanced import LME
from mlexp.utils import patch_tensor

def vit_mae(model: LME, batch: dict, drop_percent: float,
            patch_size: tuple[int, int]) -> tuple[tuple, dict[str, tr.Tensor]]:
    assert hasattr(model.base_model.decoder, "mask_token")
    assert 0 < drop_percent <= 95, drop_percent
    patched_image = patch_tensor(batch["data"], patch_size=patch_size).permute(0, 4, 1, 2, 3) # (B, T, C, ph, pw)

    mb, orig_seq_size, n_ch = patched_image.shape[0:3]
    n_kept = ceil((100 - drop_percent) * orig_seq_size // 100)
    assert n_kept > 0, n_kept

    perm = tr.randperm(orig_seq_size).to(model.device)
    kept_ix, unkept_ix = perm[0:n_kept], perm[n_kept:]
    rand_patches = patched_image[:, kept_ix]
    y_encoder = model.base_model.encoder.forward(rand_patches, kept_ix)
    # reconstruct the patches in correct order
    mask_tok_expanded = model.base_model.decoder.mask_token.repeat(mb, orig_seq_size, 1)
    mask_tok_expanded[:, kept_ix] = y_encoder
    y_decoder = model.base_model.decoder.forward(mask_tok_expanded)
    y_decoder_proj = model.base_model.proj_out(y_decoder)

    y_final = y_decoder_proj.view(mb, orig_seq_size, n_ch, *patch_size)[:, unkept_ix]
    gt = patched_image[:, unkept_ix]
    l2 = (y_final - gt).pow(2).mean()

    return (y_decoder_proj, kept_ix), {"loss": l2}, (mask_tok_expanded, y_decoder), gt

def masked_l2(y: tr.Tensor, gt: tr.Tensor, mask: tr.Tensor) -> tr.Tensor:
    return ((y - gt).pow(2) * mask).sum() / mask.sum()

def unravel_index(indices: tr.Tensor, shape: tuple) -> tr.Tensor:
    """copy pasta: https://github.com/pytorch/pytorch/issues/35674#issuecomment-739492875"""
    shape = indices.new_tensor((*shape, 1))
    coefs = shape[1:].flipud().cumprod(dim=0).flipud()
    return tr.div(indices[..., None], coefs, rounding_mode="trunc") % shape[:-1]

def conv_mae_pixelwise(model: LME, batch: dict, drop_percent: float) -> tuple[tuple, dict[str, tr.Tensor]]:
    assert 0 < drop_percent <= 95, drop_percent
    n_pixels = batch["data"].shape[-2] * batch["data"].shape[-1]
    dropped_pixels = tr.randperm(n_pixels)[0:ceil(n_pixels * drop_percent / 100)]

    mask = tr.zeros(n_pixels).to(model.device)
    mask[dropped_pixels] = 1
    mask = mask.view(batch["data"].shape[-2:]).repeat(*batch["data"].shape[0:-2], 1, 1)
    masked_images = batch["data"] * (1 - mask)
    y_encoder = model.base_model.encoder.forward(masked_images)
    y_decoder = model.base_model.decoder.forward(y_encoder)
    l2 = masked_l2(y_decoder, batch["data"], mask)
    return (y_decoder, mask), {"loss": l2}, (masked_images, y_decoder), batch["data"]

def conv_mae_patch(model: LME, batch: dict, drop_percent: float,
                   patch_size: tuple[int, int]) -> tuple[tuple, dict[str, tr.Tensor]]:
    assert 0 < drop_percent <= 95, drop_percent
    h, w = batch["data"].shape[-2:]
    # sample at random top left pixels of patches and then reproject to (h, w)
    n_mask = h * w // (patch_size[0] * patch_size[1])
    n_kept_mask = drop_percent * n_mask // 100
    kept_mask_ix = tr.randperm(n_mask)[0:n_kept_mask]
    unraveled = unravel_index(kept_mask_ix, (h // patch_size[0], w // patch_size[1]))
    dropped_pixels_start_2d = unraveled * tr.tensor((patch_size[0], patch_size[1]))
    dropped_pixels_end_2d = dropped_pixels_start_2d + tr.tensor(patch_size)
    mask = tr.zeros((h, w)).to(model.device)
    for i in range(n_kept_mask):
        mask[dropped_pixels_start_2d[i, 0]: dropped_pixels_end_2d[i, 0],
             dropped_pixels_start_2d[i, 1]: dropped_pixels_end_2d[i, 1]] = 1
    mask = mask.repeat(*batch["data"].shape[0:-2], 1, 1)
    masked_images = batch["data"] * (1 - mask)
    y_encoder = model.base_model.encoder.forward(masked_images)
    y_decoder = model.base_model.decoder.forward(y_encoder)
    l2 = masked_l2(y_decoder, batch["data"], mask)
    return (y_decoder, mask), {"loss": l2}, (masked_images, y_decoder), batch["data"]
