"""model algorithms module"""
from functools import partial
from omegaconf import DictConfig
from typing import Callable

from .masked_auto_encoder import vit_mae, conv_mae_patch, conv_mae_pixelwise

def build_model_algorithm(cfg: DictConfig) -> Callable:
    """builds the model algorithm for this pretraining task given the model config and the instantiated model/reader"""
    assert cfg.model.type in ("vit", "safeuav"), cfg.model.type
    algorithm_fn: Callable | None = None

    if cfg.model.type == "vit":
        if cfg.train.type == "vit_mae":
            algorithm_fn = partial(vit_mae, drop_percent=cfg.train.drop_percent, patch_size=cfg.train.patch_size)
    elif cfg.model.type == "safeuav":
        assert cfg.train.type in ("mae_pixelwise", "mae_patch"), cfg.train.type
        if cfg.train.type == "mae_pixelwise":
            algorithm_fn = partial(conv_mae_pixelwise, drop_percent=cfg.train.drop_percent)
        elif cfg.train.type == "mae_patch":
            algorithm_fn = partial(conv_mae_patch, drop_percent=cfg.train.drop_percent, patch_size=cfg.train.patch_size)

    if algorithm_fn is None:
        raise ValueError(f"No algorithm found for model: '{cfg.model.type}' and train type: '{cfg.train.type}'")
    return algorithm_fn
