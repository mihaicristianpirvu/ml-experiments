#!/usr/bin/env python3
from argparse import ArgumentParser, Namespace
from pathlib import Path
from datetime import datetime
import json
import shutil
from math import sqrt
import torch as tr
from torch.utils.data import DataLoader
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger, WandbLogger
from pytorch_lightning.utilities import rank_zero_only
from omegaconf import OmegaConf
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotCallback, PlotMetrics

from mlexp.utils import collage_fn, image_write, image_resize

from models import build_model
from readers import build_reader, build_train_val_readers
from algorithms import build_model_algorithm

def vit_plot_fn(x, y, gt, out_dir, model):
    pred, kept_ix = y[0]
    mb = len(x)
    all_imgs = []
    for i in range(mb):
        _x = x[i].permute(1, 2, 0)
        new_img = _x.clone()
        Y = model.base_model.encoder.vit_input_handler.patch_shape
        _y = pred[i].view(-1, *Y).permute(0, 2, 3, 1)
        X = int(sqrt(_y.shape[0]))
        for j in range(X):
            for k in range(X):
                if j * X + k in kept_ix:
                    continue
                new_img[j * Y[1]:(j + 1) * Y[1], k * Y[2]:(k + 1) * Y[2]] = _y[j * X + k].clip(0, 1)
                _x[j * Y[1]:(j + 1) * Y[1], k * Y[2]:(k + 1) * Y[2]] = 0 # remove this line to get full GT image
        all_imgs.append((tr.cat([_x, new_img], dim=1).to("cpu") * 255).byte().numpy())
    collage = collage_fn(all_imgs) if len(all_imgs) > 1 else all_imgs[0]
    collage = collage.repeat(3, axis=-1) if collage.shape[-1] == 1 else collage
    collage = image_resize(collage, 1024, None) if collage.shape[0] < 1024 else collage
    if out_dir is None:
        return collage
    shutil.rmtree(out_dir, ignore_errors=True)
    image_write(collage, f"{out_dir}.png")

def conv_plot_fn(x, y, gt, out_dir, model):
    pred, mask = y[0]
    assert len(x.shape) == len(pred.shape) == 4, f"Expected (B, C, H, W) got {x.shape}, {pred.shape}"
    pred_for_img = x * (1 - mask) + pred.detach() * mask
    x_imgs = ((x * (1 - mask)).permute(0, 2, 3, 1).to("cpu") * 255).byte() # remove * (1 - mask) to get full GT image
    y_imgs = (pred_for_img.permute(0, 2, 3, 1).to("cpu") * 255).byte()
    imgs = []
    for x_img, y_img in zip(x_imgs, y_imgs):
        imgs.append(tr.cat([x_img, y_img], dim=1).numpy())
    collage = collage_fn(imgs) if len(imgs) > 1 else imgs[0]
    collage = collage.repeat(3, axis=-1) if collage.shape[-1] == 1 else collage
    collage = image_resize(collage, 1024, None) if collage.shape[0] < 1024 else collage
    if out_dir is None:
        return collage
    shutil.rmtree(out_dir, ignore_errors=True)
    image_write(collage, f"{out_dir}.png")

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True)
    parser.add_argument("--dataset", choices=["mnist", "cifar10"], required=True)
    parser.add_argument("--experiments_dir", type=Path)
    parser.add_argument("--resume_from", type=Path, help="Path to model to resume from")
    parser.add_argument("--wandb_project", help="If set, will use wandb to log the training process")
    parser.add_argument("--wandb_run", help="If set, will use or reuse an existing version. Used to resume trains")
    args = parser.parse_args()
    if args.experiments_dir is None:
        args.experiments_dir = Path(__file__).parent / "experiments" / args.dataset
    return args

def main():
    """main fn"""
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    base_reader = build_reader(args.dataset)
    model = LME(build_model(cfg, base_reader.images_shape))
    model.optimizer = getattr(tr.optim, cfg.train.optimizer.type)(model.parameters(), **cfg.train.optimizer.parameters)
    model.model_algorithm = build_model_algorithm(cfg)
    model.hparams["model_cfg"] = json.dumps(OmegaConf.to_container(cfg, resolve=True))
    model.callbacks = [PlotCallback(vit_plot_fn if cfg.model.type == "vit" else conv_plot_fn), PlotMetrics()]
    print(model.summary)

    train_reader, val_reader = build_train_val_readers(base_reader, cfg.train.train_val_ratio)
    train_loader = DataLoader(train_reader, **{**cfg.data.loader_params, "shuffle": True})
    val_loader = DataLoader(val_reader, **cfg.data.loader_params)

    pl_loggers = [CSVLogger(save_dir=f"{args.experiments_dir}/{args.config_path.stem}",
                            name=datetime.strftime(datetime.now(), "%Y%m%d"),
                            version=datetime.strftime(datetime.now(), "%H%M%S"))]
    Path(pl_loggers[0].log_dir).mkdir(parents=True, exist_ok=True)
    OmegaConf.save(cfg, f"{pl_loggers[0].log_dir}/config.yaml", resolve=True)
    if args.wandb_project and rank_zero_only.rank == 0:
        wandb_logger = WandbLogger(project=args.wandb_project, version=args.wandb_run)
        wandb_logger.experiment.config.update(OmegaConf.to_container(cfg), allow_val_change=True)
        pl_loggers.append(wandb_logger)
    Trainer(logger=pl_loggers, **cfg.train.trainer_params).fit(model, train_loader, val_loader, None, args.resume_from)

if __name__ == "__main__":
    main()
