"""builds models for MAE"""
from math import prod
import torch as tr
from torch import nn
from omegaconf import DictConfig

from .vit import VITFeaturizer, VITInputHandler
from .safeuav import SafeUAVEncoder, conv

class DecoderVITMae(VITFeaturizer):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        # We need to add these to the decoder before optimizer. However, I'd like to not include them in ViT to
        # not make it too verbose.
        self.mask_token = tr.nn.Parameter(tr.randn(1, 1, self.vit_input_handler.emb_size))

def _build_vit(cfg: DictConfig, images_shape: tuple[int, int, int]) -> nn.Module:
    n_ch = 1 if len(images_shape) == 2 else images_shape[-1]
    patch_shape: tuple[int, int, int] = (n_ch, cfg.train.patch_size[0], cfg.train.patch_size[1])
    max_sequence_size = (images_shape[0] // cfg.train.patch_size[0] * images_shape[1] // cfg.train.patch_size[1])
    model = nn.ModuleDict({
        "encoder": VITFeaturizer(VITInputHandler(patch_shape, max_sequence_size=max_sequence_size,
                                                 **cfg.model.encoder.input_handler), **cfg.model.encoder.parameters),
        "decoder": DecoderVITMae(VITInputHandler(None, max_sequence_size=max_sequence_size,
                                                 **cfg.model.decoder.input_handler), **cfg.model.decoder.parameters),
        "proj_out": tr.nn.Linear(cfg.model.emb_size, prod(patch_shape)),
    })
    # Note: this one is experimental. It's unclear whether we should share the positional embedding or not.
    model.decoder.vit_input_handler.positional_encoding = model.encoder.vit_input_handler.positional_encoding
    return model

class DecoderSafeUAVMAE(tr.nn.Module):
    def __init__(self, n_ch: int, n_convs: int, num_filters: int):
        super().__init__()
        self.n_convs = n_convs
        self.num_filters = num_filters

        self.convs = tr.nn.ModuleList()
        for i in range(n_convs):
            nf = num_filters if i < n_convs - 1 else n_ch # nf on all layers except the last one
            self.convs.append(conv(d_in=num_filters, d_out=nf, kernel_size=3, padding=1, stride=1, dilation=1))

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = x
        for _conv in self.convs:
            y = _conv(y)
        return y

def _build_safeuav(cfg: DictConfig, images_shape: tuple[int, int, int]) -> nn.Module:
    n_ch = 1 if len(images_shape) == 2 else images_shape[-1]
    model = nn.ModuleDict({
        "encoder": SafeUAVEncoder(n_ch, **cfg.model.encoder.parameters),
        "decoder": DecoderSafeUAVMAE(n_ch, **cfg.model.decoder.parameters)
    })
    return model

def build_model(cfg: DictConfig, images_shape: tuple[int, int, int]) -> nn.Module:
    if cfg.model.type == "vit":
        model = _build_vit(cfg, images_shape)

    if cfg.model.type == "safeuav":
        model = _build_safeuav(cfg, images_shape)

    return model
