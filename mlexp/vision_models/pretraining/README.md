# Masked Auto Encoders (MAE)

<img src="logo.png">

Pre-training algorithm by masking portions of an image and letting a model reconstruct it. All models here are reusing
the same code from the [classification models](../classification-models/models) (and in future regression models!)
submodule.

Supported models:
- [ViT](models/vit.py)
- [SafeUAV](models/safeuav.py)

## Usage

```
./pretrain.py --config_path cfgs/vit_mae_min_4x4.yaml --dataset cifar10
```

After training, run `visualize.ipynb` and set the checkpoint accordingly.

For fine-tuning after the pretraining is done, use only the 'encoder' part of the architecture. For ViT, this is done
by only loading the `ViTFeatureizer`. As an example, see `--fine_tune_from` in
[this training script](../classification-models/train.py).

## Experiments

### Masked Auto Encoder

#### Fine tuning on CIFAR10

We are going to take the baseline ViT and various convnets and try to see how it compares against it.

| Model | Hparams | Best Epoch | Val Loss | Val F1 | Epoch ≥0.72 | Val loss ≥0.72 | Val F1 ≥0.72 |
|:-|:-|:-|:-|:-|:-|:-|:-|
| ViT-1M (baseline)|scratch,4x4,global_pool|44|1.26|0.60|n/a|n/a|n/a|
| [ViT-1M](cfgs/vit_1M_globalpool_4x4.yaml)|ft_from_mae,global_pool,drop=75%|100|1.41|0.71|n/a|n/a|n/a
| SafeUAV-1M (baseline)|scratch|91|1.31|0.74|11|0.77|0.72
| [SafeUAV-1M](cfgs/safeuav_1.1M_4x4_75.yaml)|ft_from_mae_4x4,drop=75%|66|1.19|**0.75**|**4**|0.68|0.72|
| [SafeUAV-1M](cfgs/safeuav_1.1M_4x4_85.yaml)|ft_from_mae_4x4,drop=85%|73|1.21|0.73|7|0.82|0.73|
| [SafeUAV-1M](cfgs/safeuav_1.1M_4x4_95.yaml)|ft_from_mae_4x4,drop=95%|100|1.36|0.72|100|1.36|0.72|
| [SafeUAV-1M](cfgs/safeuav_1.1M_pixelwise_75.yaml)|ft_from_mae_pixel,drop=75%|94|1.28|0.74|6|0.78|0.72|
| [SafeUAV-1M](cfgs/safeuav_1.1M_pixelwise_85.yaml)|ft_from_mae_pixel,drop=85%|80|1.39|0.72|80|1.39|0.72|
| [SafeUAV-1M](cfgs/safeuav_1.1M_pixelwise_95.yaml)|ft_from_mae_pixel,drop=95%|99|1.45|0.71|n/a|n/a|n/a|

Conclusion so far: using MAE with patches and 75% drop yields to best results overall as well as fastest model to reach
0.72 Val F1-score. Transformers of the same size are unable to reach 0.72 Val F1-Score on the same number of parameters.
Without MAE the results are much worse, only 0.60 F1-Score concluding that ConvNets are much easier to train even
without pretraining, however, pretraining helps for ConvNets to converge faster as well.

TODO: Scale to bigger nets and add augmentation.