"""init file"""
from omegaconf import OmegaConf

from .image import *
from .utils import *
from .patch_tensor import *

OmegaConf.register_new_resolver("len", len)
OmegaConf.register_new_resolver("str", str)
OmegaConf.register_new_resolver("sorted", sorted)
OmegaConf.register_new_resolver("eq", lambda a, b: a == b)
OmegaConf.register_new_resolver("pretrained", lambda x: x)
OmegaConf.register_new_resolver("dict", dict)
OmegaConf.register_new_resolver("flatten_unique", lambda x: sorted(set(flatten_list(x))))
OmegaConf.register_new_resolver("args", oc_args)
OmegaConf.register_new_resolver("values", lambda x: x.values())
