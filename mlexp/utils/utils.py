"""generic utils"""
import ast
import sys
from pathlib import Path
from loggez import loggez_logger as logger
from argparse import Namespace
from omegaconf import DictConfig, ListConfig

def get_project_root() -> Path:
    """the project root duh"""
    return Path(__file__).parents[2]

def oc_argparse_overwrite(cfg: DictConfig, args: Namespace) -> DictConfig:
    """manually replace an OmegaConf node from the CLI via argparse"""
    for item in args.replace_cfg:
        assert len(item) == 2, f"Expected [cfg.]x.y.z [=] value, got {item}"
        left, right = item[0].split("."), ast.literal_eval(item[1])
        _cfg = cfg
        for k in left[0:-1]:
            assert k in _cfg, f"Key {k} of {item} not in {cfg}"
            _cfg = _cfg[k]
        assert left[-1] in _cfg, f"Key {left[-1]} of {item} not in {cfg}"
        logger.info(f"cfg.{item[0]} ({_cfg[left[-1]]}) replaced by CLI: {right}")
        _cfg[left[-1]] = right
    return cfg

def oc_args(x: str) -> str:
    """parses ${args:xxx} to match CLI args '--xxx yyy' and returns yyy. Only works for positional args"""
    ix = sys.argv.index(f"--{x}")
    assert (L := len(sys.argv)) == ix + 2 or (L >= ix + 3 and sys.argv[ix + 2].startswith("--")), (x, "\n", sys.argv)
    return sys.argv[ix + 1]

def flatten_list(x: list[list[str]]) -> list[str]:
    """Flattens a list of lists of strings."""
    if len(x) == 0:
        return []
    assert not isinstance(x, (tuple, set)), x
    res = []
    for item in x:
        if isinstance(item, (list, ListConfig)):
            res.extend(flatten_list(item))
        else:
            assert isinstance(item, str), (item, type(item))
            res.append(item)
    return res
