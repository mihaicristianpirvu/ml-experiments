"""init file for the whole mlexp project"""
import warnings
from pathlib import Path
from lovely_tensors import monkey_patch

monkey_patch()

def get_project_root() -> Path:
    """project root as absolute path"""
    return Path(__file__).parents[1].absolute()

warnings.filterwarnings("ignore", ".*does not have many workers.*")
warnings.filterwarnings("ignore", "You are using `torch.load` with `weights_only=False`*.")
