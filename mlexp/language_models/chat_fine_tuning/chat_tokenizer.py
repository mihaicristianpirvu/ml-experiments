"""chat  fine tuning tokenizer"""
from __future__ import annotations
import tiktoken
from loggez import loggez_logger as logger

from mlexp.language_models.tokenizers import CharTokenizer

class CustomTokenizer(tiktoken.Encoding):
    def encode(self, *args, **kwargs):
        # assert "allowed_special" not in kwargs and "disallowed_special" not in kwargs, kwargs
        if "allowed_special" in kwargs:
            assert kwargs["allowed_special"] == "all", kwargs["allowed_special"]
        if "disallowed_special" in kwargs:
            assert len(kwargs["disallowed_special"]) == 0, kwargs["disallowed_special"]
        return super().encode(*args, **{**kwargs, "allowed_special": "all", "disallowed_special": ()})

    def __repr__(self):
        return f"[CustomTokenizer] {self.name}. Alphabet: {self.max_token_value}. Special: {self.special_tokens_set}"

def build_chat_tokenizer(base_tokenizer: tiktoken.Encoding, special_tokens: list[str]) -> tiktoken.Encoding:
    logger.info(f"Building a custom tokenizer on top of '{base_tokenizer}'. Special tokens: {special_tokens}")
    assert len(special_tokens) > 0, "must provide special tokens for chat tokenizer"
    assert isinstance(special_tokens, list) and len(special_tokens) > 0

    special_tokens_dict = {k: base_tokenizer.max_token_value + i + 1 for i, k in enumerate(special_tokens)}
    if isinstance(base_tokenizer, CharTokenizer):
        chat_tokenizer = CharTokenizer(base_tokenizer.alphabet, special_tokens_dict)
        chat_tokenizer.name += " (chat)"
    else:
        chat_tokenizer = CustomTokenizer(
            name=f"{base_tokenizer.name} (chat)",
            pat_str=base_tokenizer._pat_str,
            mergeable_ranks=base_tokenizer._mergeable_ranks,
            special_tokens={**base_tokenizer._special_tokens, **special_tokens_dict}
        )
    return chat_tokenizer
