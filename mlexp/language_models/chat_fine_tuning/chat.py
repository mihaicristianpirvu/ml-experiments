#!/usr/bin/env python3
from __future__ import annotations
from argparse import ArgumentParser, Namespace
from omegaconf import OmegaConf
from pathlib import Path
from datetime import datetime
from loggez import loggez_logger as logger
import tiktoken
import torch as tr
from colorama import Fore, Style
from torchinfo import summary

from torch import nn
from mlexp.language_models.models import build_model, AutoregressModel
from mlexp.language_models.tokenizers import build_tokenizer
from mlexp.language_models.chat_fine_tuning.chat_tokenizer import build_chat_tokenizer

device = "cuda" if tr.cuda.is_available() else "cpu"

def get_encoded_seed(buf: list[str], tokenizer: tiktoken.Encoding,
                     max_sequence_size: int, system_prompt: str) -> tuple[list[str], str, tr.Tensor]:
    buf = buf[::-1]
    start_token, _, user_token, bot_token = list(tokenizer._special_tokens.keys())[0:4]

    system_prompt = f"{system_prompt}{user_token}" if len(system_prompt) > 0 else system_prompt
    assert len(buf) % 2 == 1, f"Expected odd sequence, got {len(buf)}"
    str_res = f"{user_token}{system_prompt}{buf[0]}{bot_token}"
    encoded = tokenizer.encode(f"{start_token}{str_res}")
    assert len(encoded) < max_sequence_size
    for i in range((len(buf) - 1) // 2):
        str_res = f"{user_token}{system_prompt}{buf[2 * i + 2]}{bot_token}{buf[2 * i + 1]}{str_res}"
        new_encoded = tokenizer.encode(f"{start_token}{str_res}")
        if len(new_encoded) > max_sequence_size:
            buf = buf[0: 2 * i + 3]
            break
        encoded = new_encoded
    return buf[::-1], f"{start_token}{str_res}", encoded

def _print(msg: str, maxlen: int):
    print(f"{'=' * (maxlen // 2 - len(msg) // 2)}{msg}{'=' * (maxlen // 2 - len(msg) // 2 + (len(msg) % 2 == 1))}")

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("weights_path", type=Path)
    parser.add_argument("--system_prompt", default="")
    args = parser.parse_args()
    return args

@tr.no_grad()
def main(args: Namespace):
    ckpt_data = tr.load(args.weights_path, map_location="cpu")
    cfg = OmegaConf.create(ckpt_data["hyper_parameters"]["model_cfg"])
    max_sequence_size = cfg["model"]["input_handler"].get("max_sequence_size", 1000)
    special_tokens = ckpt_data["hyper_parameters"]["special_tokens"]
    assert len(special_tokens) > 1, "Not a chatbot model, got no special tokens"
    tokenizer_name = ckpt_data["hyper_parameters"]["tokenizer_name"]
    assert tokenizer_name[-6:] == "(chat)", tokenizer_name
    new_tokens = [x[0] for x in sorted(special_tokens.items(), key=lambda kv: kv[1][0])]
    base_tokenizer = build_tokenizer(**OmegaConf.to_container(cfg.data.tokenizer_metadata))
    chat_tokenizer = build_chat_tokenizer(base_tokenizer, new_tokens)
    end_token = list(chat_tokenizer._special_tokens.keys())[-3]

    if cfg.model.type == "transformer":
        cfg.model.encoder.parameters.return_kv = 1
    model: nn.Module | AutoregressModel = build_model(cfg.model, chat_tokenizer)
    model.load_state_dict(ckpt_data["state_dict"], strict=True, assign=True)
    model.eval().to(device)
    print(f"Loaded chat model. Num params: {summary(model, verbose=0).total_params/1e6:.2f}M. Device: {device}.")
    print(f"Reading from prompt. Max: {max_sequence_size} tokens. System prompt: {args.system_prompt}")

    buffer = []
    while True:
        try:
            prompt = input(f"{Fore.GREEN}> Prompt:{Style.RESET_ALL} ")
        except EOFError:
            break
        now = datetime.now()
        n_chars = 0

        if prompt == "/clear":
            print("Clearing buffer")
            buffer = []
            continue

        if prompt == "/print_context":
            print(f"Context {len(buffer)}, {len(encoded)=}:\n{buffer}")
            continue

        buffer.append(prompt)
        try:
            buffer, seed, encoded = get_encoded_seed(buffer, chat_tokenizer, max_sequence_size, args.system_prompt)
        except Exception as e:
            logger.error(f"Failed to encode prompt '{buffer[-1]}'. Threw exception: {str(e)}")
            buffer = []
            continue

        _print(f"Encoded {len(encoded)} tokens ({len(seed)} chars)", 120) # print {seed} as well for debugging
        print(f"{Fore.RED}> Response:{Style.RESET_ALL} ", end="", flush=True)
        response = ""
        for n_toks, ch_ix in enumerate(model.autoregress(tr.LongTensor(encoded).to(device), n=1000)):
            if ch_ix in chat_tokenizer._special_tokens.values():#[end_token]:
                print("")
                break
            ch = chat_tokenizer.decode([ch_ix])
            n_chars += len(ch)
            print(ch, end="", flush=True)
            response += ch
        buffer.append(response)
        took = datetime.now() - now
        _print(f"Generated {n_toks} tokens ({n_chars} chars). Avg: {n_toks / took.total_seconds():.2f} tok/s", 120)

if __name__ == "__main__":
    main(get_args())
