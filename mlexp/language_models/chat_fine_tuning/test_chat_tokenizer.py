import pytest
from mlexp.language_models.chat_fine_tuning.chat_tokenizer import build_chat_tokenizer
from mlexp.language_models.tokenizers import build_tokenizer

def test_chat_tokenizer_bpe_gpt2():
    base_tokenizer = build_tokenizer(tokenizer_type="bpe_gpt2")
    special_tokens = ["<|startofstring|>", "<|endofstring|>", "<|user|>", "<|bot|>"]
    chat_tokenizer = build_chat_tokenizer(base_tokenizer, special_tokens=special_tokens)

    base_encoded_text = base_tokenizer.encode("lala<|startofstring|><|bot|>")
    assert base_encoded_text[-2] != base_tokenizer.max_token_value - 3
    assert base_encoded_text[-1] != base_tokenizer.max_token_value

    chat_encoded_text = chat_tokenizer.encode("lala<|startofstring|><|bot|>")
    assert chat_encoded_text[-2] == chat_tokenizer.max_token_value - 3
    assert chat_encoded_text[-1] == chat_tokenizer.max_token_value

def test_chat_tokenizer_character():
    alphabet = ["<", "|", ">", *[chr(x) for x in range(ord("A"), ord("z"))]]
    base_tokenizer = build_tokenizer(tokenizer_type="character", alphabet_mode="provided", alphabet=alphabet)
    special_tokens = ["◻", "◼", "🙎", "🤖"]
    chat_tokenizer = build_chat_tokenizer(base_tokenizer, special_tokens=special_tokens)

    with pytest.raises(KeyError):
        _ = base_tokenizer.encode("lala◻🤖")
    chat_encoded_text = chat_tokenizer.encode("lala◻🤖")

    assert chat_encoded_text[-2] == chat_tokenizer.max_token_value - 3
    assert chat_encoded_text[-1] == chat_tokenizer.max_token_value
