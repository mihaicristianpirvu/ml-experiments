#!/usr/bin/env python3
from __future__ import annotations
import json
from argparse import ArgumentParser, Namespace
from pathlib import Path
from datetime import datetime
from omegaconf import OmegaConf, DictConfig
from loggez import loggez_logger as logger
import tiktoken
import numpy as np
import pandas as pd
import torch as tr
from torch.utils.data import DataLoader, Dataset
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics
from pytorch_lightning import Trainer, Callback
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.utilities import rank_zero_only

from mlexp.language_models.tokenizers import build_tokenizer
from mlexp.language_models.models import build_model
from mlexp.language_models.pretraining.algorithms import build_model_algorithm

from mlexp.language_models.chat_fine_tuning.chat_dataset import ChatDataset
from mlexp.language_models.chat_fine_tuning.chat_tokenizer import build_chat_tokenizer

class GenerateTextCallback(Callback):
    def __init__(self, tokenizer: tiktoken.Encoding, seeds: tuple[list[str]], max_sequence_size: int = 1000):
        self.tokenizer = tokenizer
        self.max_sequence_size = max_sequence_size
        self.seeds, self.gts = seeds
        self.bot_ix = tokenizer.encode("<|bot|>")[0]

    @tr.no_grad()
    def on_train_epoch_end(self, trainer: Trainer, pl_module: LME) -> None:
        if rank_zero_only.rank != 0:
            return
        ix: int = np.random.randint(len(self.seeds))
        seed, gt = self.seeds[ix], self.gts[ix]
        # the models operate on LongTensors which they will embed, autoregress and return a new long tensor
        tr_seed = tr.LongTensor(self.tokenizer.encode(seed))
        tr_seed = tr_seed[0:self.max_sequence_size // 2] if len(tr_seed) >= self.max_sequence_size // 2 else tr_seed
        tr_generated = [c for c in pl_module.autoregress(seed=tr_seed.to(pl_module.device), n=self.max_sequence_size)]
        if self.tokenizer.eot_token in tr_generated:
            tr_generated = tr_generated[0: tr_generated.index(self.tokenizer.eot_token) + 1]
        generated = self.tokenizer.decode(tr_generated) if len(tr_generated) > 0 else ""
        csv_file = Path(f"{trainer.loggers[0].log_dir}/gen.csv")
        if not csv_file.exists():
            open(csv_file, "w").write("seed,generated,gt\n")

        seed = seed.replace("\n", "<EOL>").replace("\"", "<QUOTE>")
        res = generated.replace("\n", "<EOL>").replace("\"", "<QUOTE>")
        gt = gt.replace("\n", "<EOL>").replace("\"", "<QUOTE>")
        df = pd.read_csv(csv_file)
        df.loc[len(df)] = [seed, res, gt]
        df.to_csv(csv_file, index=False)

def _get_seeds(val_reader: Dataset) -> tuple[list[str], list[str]]:
    seeds, gts = [], []
    for i in range(len(val_reader)):
        seeds.append(f"{val_reader.start_token}{val_reader.user_token}{val_reader.data[i][0]['question']}"
                     f"{val_reader.bot_token}")
        gts.append(f"{val_reader.data[i][1]['answer']}{val_reader.end_token}")
    return seeds, gts

def _get_readers(train_set_path: Path, val_set_path: Path, tokenizer: tiktoken.Encoding,
                 max_seq_size: int) -> tuple[ChatDataset, ChatDataset]:
    return [ChatDataset(json.load(open(path, "r")), tokenizer, max_seq_size) for path in (train_set_path, val_set_path)]

def _load_from_pretrained(model: LME, ckpt_data: dict):
    old_encoder_layers = {k: v for k, v in ckpt_data["state_dict"].items() if k.startswith("encoder.")}
    old_decoder_layers = {k: v for k, v in ckpt_data["state_dict"].items() if k.startswith("decoder.")}
    old_emb_layer = ckpt_data["state_dict"]["input_handler.content_embedding.weight"]

    # Load all possible weights from the pre-trained model:
    # - For embedding, load the first n embeddings before the newly added special tokens (i.e. <|startofstring|>)
    # - For encoder, we can load all weights, nothing changed over there
    # - For decoder, we can load all the weights except for the last layer. In the last layer, we have a fully
    #  connected layer that outputs n_vocab. So we copy all the data for the first embeddings before the newly
    #  added ones. The rest of them will be initialized in the build_transformer_model fn.
    # See here: https://github.com/huggingface/transformers/blob/c48787f347bd604f656c2cfff730e029c8f8c1fe/src/transformers/modeling_utils.py#L1737
    model.base_model.encoder.load_state_dict({k[len("encoder."):]: v for k, v in old_encoder_layers.items()})
    model.base_model.input_handler.content_embedding.weight.data[0: len(old_emb_layer)] = old_emb_layer.data[:]
    model.base_model.input_handler.content_embedding.weight.data[len(old_emb_layer):] = old_emb_layer.mean(0)[None]
    for i, (name, param) in enumerate(model.base_model.decoder.named_parameters()):
        if i < len(old_decoder_layers) - 2:
            param.data[:] = old_decoder_layers[f"decoder.{name}"].data[:]
        else:
            param.data[0: len(old_decoder_layers[f"decoder.{name}"])] = old_decoder_layers[f"decoder.{name}"].data[:]

def _get_cfg_with_overrides(cfg: DictConfig, args: Namespace) -> DictConfig:
    overridable = [("lr", cfg.train.optimizer.parameters), ("max_seq_size", cfg.train),
                   ("batch_size", cfg.train), ("batch_size", cfg.data.loader_params),
                   ("max_epochs", cfg.train.trainer_params)]
    for key, parent in overridable:
        if (val := getattr(args, key)) is not None:
            logger.debug(f"'{key}' updated from {parent[key]} to {val} via CLI")
            parent[key] = val
    return cfg

def get_args() -> Namespace:
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", type=Path, required=True)
    parser.add_argument("--validation_set_path", type=Path, required=True)
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument("--fine_tune_from", type=Path)
    group.add_argument("--resume_from", type=Path)
    parser.add_argument("--experiments_dir", type=Path, default=Path(__file__).parent / "experiments")
    parser.add_argument("--max_seq_size", type=int)
    parser.add_argument("--batch_size", type=int)
    parser.add_argument("--lr", type=float)
    parser.add_argument("--max_epochs", type=int)
    args = parser.parse_args()
    return args

def main(args: Namespace):
    ckpt_data = tr.load(pth := (args.fine_tune_from if args.fine_tune_from else args.resume_from), map_location="cpu")
    logger.info(f"{'Loading' if args.resume_from else 'Fine tuning'} from '{pth}'")
    cfg = _get_cfg_with_overrides(OmegaConf.create(ckpt_data["hyper_parameters"]["model_cfg"]), args)

    if (tokenizer_type := cfg.data.tokenizer_metadata.tokenizer_type) == "bpe_gpt2":
        new_tokens = ["<|startofstring|>", "<|endofstring|>", "<|user|>", "<|bot|>"]
    elif tokenizer_type == "character":
        new_tokens = ["◻", "◼", "🙎", "🤖"]
    else:
        raise ValueError(tokenizer_type)
    base_tokenizer = build_tokenizer(**OmegaConf.to_container(cfg.data.tokenizer_metadata))
    chat_tokenizer = build_chat_tokenizer(base_tokenizer, new_tokens)

    max_seq_size = cfg.model.input_handler.get("max_sequence_size", 1000)
    train_reader, val_reader = _get_readers(args.dataset_path, args.validation_set_path, chat_tokenizer, max_seq_size)
    model = LME(build_model(cfg.model, chat_tokenizer))
    model.hparams["special_tokens"] = {t: chat_tokenizer.encode(t, allowed_special="all") for t in new_tokens}
    model.hparams["tokenizer_name"] = chat_tokenizer.name
    model.hparams["model_cfg"] = json.dumps(OmegaConf.to_container(cfg, resolve=True, throw_on_missing=True))
    if args.fine_tune_from:
        _load_from_pretrained(model, ckpt_data)

    train_loader = DataLoader(train_reader, collate_fn=(collate := train_reader.collate_fn), **cfg.data.loader_params)
    val_loader = DataLoader(val_reader, collate_fn=collate, **{**cfg.data.loader_params, "shuffle": False})

    model.model_algorithm = build_model_algorithm(cfg, chat_tokenizer)
    model.optimizer = getattr(tr.optim, cfg.train.optimizer.type)(model.parameters(), **cfg.train.optimizer.parameters)
    model.callbacks = [GenerateTextCallback(chat_tokenizer, seeds=_get_seeds(val_reader),
                                            max_sequence_size=max_seq_size), PlotMetrics()]
    model.metrics = {"loss_last_pred": (lambda y, gt: (y - gt).abs().mean(), "min")}
    model.checkpoint_monitors = ["loss", "loss_last_pred"]
    print(model.summary)

    pl_loggers = [CSVLogger(save_dir=args.experiments_dir,
                            name=datetime.strftime(datetime.now(), "%Y%m%d"),
                            version=datetime.strftime(datetime.now(), "%H%M%S"))]
    Path(pl_loggers[0].log_dir).mkdir(parents=True, exist_ok=True) if rank_zero_only.rank == 0 else None
    OmegaConf.save(cfg, f"{pl_loggers[0].log_dir}/config.yaml", resolve=True) if rank_zero_only.rank == 0 else None
    trainer = Trainer(logger=pl_loggers, max_epochs=args.max_epochs, enable_model_summary=False)
    trainer.fit(model, train_loader, val_loader, None, args.resume_from)

if __name__ == "__main__":
    main(get_args())
