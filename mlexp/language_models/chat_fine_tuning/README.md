# Fine tuning language models for chatbot usage

This module handles fine tuning of language models pretrained using the [pretraining submodule](../pretraining/). By
fine tuning, I refer to full fine tuning, not domain adaptation (freezing) or PEFT/LORA. We need to do a bunch of things
such that fine tuning works.

First, we'll start by trying to create a chat bot. Later one, we may do classification (i.e. SQuAD) or smth else.


## 1. Dataset
We need an user/assistant dataset with back and forth chats.

Possible datasets:
- [some qa dataset](https://github.com/Pawandeep-prog/finetuned-gpt2-convai/blob/main/chat_data.json) -
we are using this one first because it's simpest (no back and forth). Conclusion: it's crap.
- [Open Assistant V2 dataset](https://huggingface.co/datasets/OpenAssistant/oasst2). This is the 2nd one we are working with. Conclusion so far: it works fine for experimentation, but we need more time to adjust the training code. It can kinda give the feeling of understanding.
- [openassistant guanaco](https://huggingface.co/datasets/timdettmers/openassistant-guanaco?row=0)
- [ultrachat](https://huggingface.co/datasets/stingning/ultrachat)

## 2. Assitant/SFT tokens
We are going to format our data to behave like a chatbot/assistant. Generally, people use some special token for this,
namely: beginning of sentence (bos), end of sentence (eos) and pad token (pad), also <bot> and some others
maybe. See [here](https://youtu.be/elUCn_TFdQc?t=919) an example

Useful links:
- https://medium.com/@lokaregns/preparing-text-data-for-transformers-tokenization-mapping-and-padding-9fbfbce28028 (pad token)
- https://towardsdatascience.com/padding-large-language-models-examples-with-llama-2-199fb10df8ff (pad token)

## 3. Adding new tokens to the pretrained embedding layer.
Since our embedding layer was trained on the GPT-2 tokenizer, we only have 1 embedding for each token in the GPT2
'alphabet'. We need to add new ones. In the video above, the user uses `model.resize_token_embeddings(new_len)`, but
this only works if we are using the `transformers` library. In our case, we just use torch and 'bare' `nn.Embedding`,
so we need to add them ourselves. Basically, we need to expand the `nn.Embedding` a bit. Here's a snippet on how to
do it: [link](https://stackoverflow.com/questions/72775559/resize-token-embeddings-on-the-a-pertrained-model-with-different-embedding-size).

For the new ones, we will initialize them with the average of the existing embeddings.

## 4. Loading pretranined weights
Furthermore, we also load all the possible weights from the pretrained model.
For encoder: we load all the weights. For the decoder, we load all the weights except the last layer. In the last layer,
as it is a FC layer, we load all the weights except the newly added embedding' weights. These are randomly init and will
be learned from scratch.

This is inspired from huggingface: [link](https://github.com/huggingface/transformers/blob/c48787f347bd604f656c2cfff730e029c8f8c1fe/src/transformers/modeling_utils.py#L1873).

## 5. Preparing the data

We will do full SFT for now, updating all the layers (embedding, encoder, decoder). Later we'll try some PEFT/LoRA
stuff. We have two datasets under [here](../../../data/), namely Open Assistant 2 and Ultrachat 200k.

In order to prepare the data:
```bash
cd data/
python ../../../../data/ultrachat_200k/prepare.py train train_ultrachat.jsonl 3000 # 3k ~= 750 max context length
python ../../../../data/ultrachat_200k/prepare.py validation val_ultrachat.jsonl 3000 # 3k ~= 750 max context length
python ../../../../data/oasst2/prepare.py train train_oasst2.jsonl
python ../../../../data/oasst2/prepare.py validation val_oasst2.jsonl
jq -s "add" train_oasst2.jsonl train_ultrachat.jsonl > train.jsonl
jq -s "add" val_oasst2.jsonl val_ultrachat.jsonl > val.jsonl

ls -lh
total 864M
-rw-rw-r-- 1 mihai mihai 387M Feb 16 11:28 train.jsonl
-rw-rw-r-- 1 mihai mihai  53M Feb 16 11:16 train_oasst.jsonl
-rw-rw-r-- 1 mihai mihai 344M Feb 16 11:25 train_ultrachat.jsonl
-rw-rw-r-- 1 mihai mihai  41M Feb 16 11:28 val.jsonl
-rw-rw-r-- 1 mihai mihai 2.8M Feb 16 11:16 val_oasst.jsonl
-rw-rw-r-- 1 mihai mihai  39M Feb 16 11:25 val_ultrachat.jsonl
```

Finally, train on this data:

```bash
cd ..
./train.py \
    --dataset_path data/train.jsonl \
    --validation_set_path data/val.jsonl \
    --fine_tune_from ../pretrain/ckpts/pytorch_model.ckpt \ # see ../pretrain on how to get this gpt-2 base ckpt
    --batch_size 4
```
