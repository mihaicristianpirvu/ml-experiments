"""Chat Fine tuning dataset"""
from __future__ import annotations
import os
import tiktoken
import random
import torch as tr
from loggez import loggez_logger as logger
from torch.utils.data import Dataset
from torch.nn.utils.rnn import pad_sequence

class ChatDataset(Dataset):
    def __init__(self, data: list[str], tokenizer: tiktoken.Encoding, max_sequence_size: int):
        self.data = data
        self.tokenizer = tokenizer
        self.max_sequence_size = max_sequence_size
        self.special_tokens = list(self.tokenizer._special_tokens.keys())
        if len(t := self.special_tokens) != 4:
            logger.warning(f"Expected start/end/user/bot, got: {t}")

        self.start_token, self.end_token, self.user_token, self.bot_token = self.special_tokens[-4:]

    def collate_fn(self, batch: list[tr.Tensor]) -> tuple[tr.Tensor, tr.Tensor]:
        """collate_fn for the dataloader"""
        assert all(isinstance(x, tr.Tensor) for x in batch), batch
        padded_res = pad_sequence(batch, batch_first=True, padding_value=self.tokenizer._special_tokens[self.end_token])
        return padded_res

    def __getitem__(self, ix: int) -> tr.Tensor:
        assert isinstance(ix, int)
        raw_data = self.data[ix]

        # if len(raw_data) > 2:
        #     breakpoint()
        # else:
        #     raise ValueError

        chat_data = self.start_token
        for i, line in enumerate(raw_data):
            key = "question" if i % 2 == 0 else "answer"
            assert key in line and len(line) == 1, line
            chat_data += f"{self.user_token if i % 2 == 0 else self.bot_token}{line[key]}"
        chat_data += self.end_token

        encoded_data = self.tokenizer.encode(chat_data)
        if len(encoded_data) > self.max_sequence_size:
            encoded_data = [*encoded_data[0: self.max_sequence_size - 1], self.tokenizer.encode(self.end_token)[0]]
        encoded = tr.LongTensor(encoded_data)
        return encoded

    def __len__(self) -> int:
        return 2 if os.getenv("DEBUG", "0") == "1" else len(self.data)

    def __repr__(self):
        return (f"[ChatDataset]. Len: {len(self)}. Max sequence size: {self.max_sequence_size}. "
                f"Special tokens ({len(self.special_tokens)}): {self.special_tokens}")

if __name__ == "__main__":
    import json
    from mlexp.language_models.chat_fine_tuning.chat_tokenizer import build_chat_tokenizer
    def _print(tokenizer: tiktoken.Encoding, data: tr.Tensor) -> str:
        str_data = tokenizer.decode(data.tolist())
        breakpoint()

    base_tokenizer = tiktoken.get_encoding("gpt2")
    new_tokens = ["<|startofstring|>", "<|endofstring|>", "<|user|>", "<|bot|>"]
    chat_tokenizer = build_chat_tokenizer(base_tokenizer, new_tokens)
    path = "../../../data/oasst2/val.json"
    max_seq_size = 1000
    chat_dataset = ChatDataset(json.load(open(path, "r")), chat_tokenizer, max_seq_size)

    _print(chat_tokenizer, chat_dataset[random.randint(0, len(chat_dataset) - 1)])
