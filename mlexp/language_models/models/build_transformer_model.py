"""build transformer model"""
from __future__ import annotations
from typing import Iterator, Callable
from functools import partial
import math
from omegaconf import DictConfig
from torch import nn
from torch.nn import functional as F
import torch as tr
from loggez import loggez_logger as logger

from .build_embedding_layer import build_embedding_layer
from .transformer import Transformer

class PositionalEncoding1D(nn.Module):
    """Pre-compute positional encodings using sin/cos"""
    def __init__(self, sequence_size: int, emb_size: int):
        super().__init__()
        assert emb_size % 2 == 0, "Must be odd for sin/cos positional encoding"
        self.register_buffer("pe", tr.zeros(sequence_size, emb_size))
        position = tr.arange(0, sequence_size).unsqueeze(1).float()
        div_term = (tr.arange(0, emb_size, 2, dtype=tr.float) * -(math.log(10000) / emb_size)).exp()
        self.pe[:, 0::2] = tr.sin(position * div_term)
        self.pe[:, 1::2] = tr.cos(position * div_term)

    def __call__(self, ix: tr.Tensor) -> tr.Tensor:
        return self.pe[ix.to("cpu")]

    def __repr__(self):
        return f"[PositionalEncoding1D] sequence_size={self.pe.shape[0]} emb_size={self.pe.shape[1]}"

class LMTransformerInputHandler(nn.Module):
    def __init__(self, max_sequence_size: int, n_tokens: int, emb_size: int, embedding_type: str,
                 positional_encoding_type: str):
        super().__init__()
        self.max_sequence_size = max_sequence_size
        self.n_tokens = n_tokens
        self.emb_size = emb_size
        self.embedding_type = embedding_type
        self.positional_encoding_type = positional_encoding_type

        self.content_embedding = build_embedding_layer(embedding_type, n_tokens, emb_size=emb_size)
        self.positional_encoding = None
        if self.positional_encoding_type == "embedding":
            self.positional_encoding = nn.Embedding(max_sequence_size, emb_size)
        elif self.positional_encoding_type == "sin_cos":
            self.positional_encoding = PositionalEncoding1D(max_sequence_size, emb_size)

    def forward(self, x: tr.Tensor, x_t: tr.Tensor | None = None) -> tr.Tensor:
        """Preprocessing of the input tensor (positional encoding and patch projection). x :: (B, t, [HxWxC])"""
        device = next(self.parameters()).device
        B, t = x.shape[0: 2]
        assert t <= self.max_sequence_size, (x.shape, self.max_sequence_size)
        assert x.shape == (B, t), f"Wrong shapes: {x.shape} vs {(B, t)}"
        x_emb = self.content_embedding(x) # (B, t, E)
        x_pos = 0
        if self.positional_encoding_type in ("sin_cos", "embedding"):
            x_t = (tr.arange(t) if x_t is None else x_t).to(device)
            x_pos = self.positional_encoding(x_t) # (B, t, E)
        return x_emb + x_pos

class MLPDecoder(nn.Module):
    """simple decoder, take all the tokens, do a bunch of FCs and return the last one"""
    def __init__(self, emb_size: int, hidden_sizes: list[int], output_size: int, bias: bool=True):
        super().__init__()
        self.emb_size = emb_size
        self.hidden_sizes = hidden_sizes
        self.output_size = output_size
        fc_sizes = [emb_size, *hidden_sizes, output_size]
        self.fcs = nn.ModuleList(nn.Linear(i, o, bias) for i, o in zip(fc_sizes[:-1], fc_sizes[1:]))

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y_fcs = x
        for fc in self.fcs[0:-1]:
            y_fcs = F.relu(fc(y_fcs))
        y_out = self.fcs[-1](y_fcs)
        return y_out

@tr.no_grad()
def transformer_autoregress(seed: tr.LongTensor, n: int | None, model: nn.Module,
                            temperature: float = 1.0) -> Iterator[int]:
    """
    Take a conditioning sequence of indices seed (LongTensor of shape (b,t)) and complete
    the sequence n times, feeding the predictions back into the model each time.
    Most likely you'll want to make sure to be in model.eval() mode of operation for this.
    """
    if n is None or n > model.input_handler.max_sequence_size - len(seed):
        n = model.input_handler.max_sequence_size - len(seed)
        logger.debug2(f"N not set or too big. Setting to {n}. {model.input_handler.max_sequence_size=}, {len(seed)=}")
    seed = seed[None]
    kv_cache = None
    for i in range(n):
        # if the sequence context is growing too long we must crop it at block_size
        seed = seed[:, 2:] if seed.size(dim=1) > model.input_handler.max_sequence_size - 1 else seed
        # forward the model to get the logits for the index in the sequence
        x_emb = model.input_handler(seed, x_t=tr.LongTensor([kv_cache[0].shape[2]]) if kv_cache else None) # (B, t, E)
        y_enc = model.encoder(x_emb, kv_cache=kv_cache) # (B, t, E), [(B, 2*t, E)]
        if model.encoder.return_kv:
            y_enc, kv_cache = y_enc
        logits = model.decoder(y_enc) # (B, t, vocab)
        # pluck the logits at the final step and scale by desired temperature
        logits = logits[:, -1] / temperature # top-k can help here too
        # apply softmax to convert logits to (normalized) probabilities
        probs = F.softmax(logits, dim=-1)
        # either sample from the distribution or take the most likely element
        seed_next = tr.multinomial(probs, num_samples=1) # next seed becomes the sampled data with kv cache
        # append sampled index to the running sequence and continue. If kv_cache, next seed is just this prediction.
        seed = seed_next if model.encoder.return_kv else tr.cat((seed, seed_next), dim=1)
        yield seed[0][-1].item()

def build_transformer_model(model_cfg: DictConfig, n_tokens: int) -> tuple[nn.Module, Callable]:
    input_handler = LMTransformerInputHandler(n_tokens=n_tokens, **model_cfg.input_handler)
    encoder = Transformer(input_handler.emb_size, **model_cfg.encoder.parameters)
    decoder = MLPDecoder(input_handler.emb_size, **model_cfg.decoder.parameters, output_size=n_tokens)

    model = nn.ModuleDict({"input_handler": input_handler, "encoder": encoder, "decoder": decoder})
    return model, partial(transformer_autoregress, model=model)
