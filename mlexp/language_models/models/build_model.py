"""model builder for Language Models"""
from omegaconf import DictConfig
from abc import ABC, abstractmethod
from typing import Iterable
from torch import nn
import torch as tr
import tiktoken

# Model architectures
from .build_transformer_model import build_transformer_model
from .build_rnn_model import build_rnn_model
from .build_ngram_model import build_ngram_model

class AutoregressModel(nn.Module, ABC):
    @abstractmethod
    def autoregress(self, seed: tr.Tensor, n: int, temperature: float = 1) -> Iterable[int]:
        """the autoregress fn"""

def build_model(model_cfg: DictConfig, tokenizer: tiktoken.Encoding) -> nn.Module | AutoregressModel:
    """
    Builds model for language models project
    Parameters:
    - cfg: The model config object used to construct the model, data and training algorithm
    - tokenizer: The tokenizer used to encode text. Also contains the number of embeddings that we need to use.
    Returns: The instantiated object ready to be used for training or inference.
    """
    model, autogress = None, None
    if model_cfg.type == "ngram":
        model, autogress = build_ngram_model(model_cfg, n_tokens=tokenizer.max_token_value)

    if model_cfg.type == "rnn":
        model, autogress = build_rnn_model(model_cfg, n_tokens=tokenizer.max_token_value + 1)

    if model_cfg.type == "transformer":
        model, autogress = build_transformer_model(model_cfg, n_tokens=tokenizer.max_token_value + 1)

    if model is None:
        raise NotImplementedError(model_cfg.type)

    model.autoregress = autogress
    return model
