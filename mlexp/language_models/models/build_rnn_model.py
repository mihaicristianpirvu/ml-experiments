"""builder for rnn model for language models"""
from __future__ import annotations
from typing import Iterator, Callable
from functools import partial
from torch import nn
import torch as tr
from torch.nn import functional as F
from omegaconf import DictConfig
from loggez import loggez_logger as logger

from mlexp.models.rnn import pad, unpad, RNN
from .build_embedding_layer import build_embedding_layer

@tr.no_grad()
def rnn_autoregress(seed: tr.Tensor, n: int | None, model: nn.Module, temperature: float = 1.0) -> Iterator[int]:
    # (1, T, alphabet_size)
    if n is None:
        logger.warning("N not set, defaulting to 1000")
        n = 1000
    h = None
    x = seed[None]
    res = tr.zeros(n).long().to(seed.device)
    for i in range(n):
        x_emb = model.input_handler(x)
        y_enc, h = model.encoder.forward(x_emb, h)
        y_dec = model.decoder.forward(y_enc[-1][-1][None])[0]
        y_dec_t = y_dec / temperature
        ch_ix = tr.multinomial(y_dec_t.softmax(dim=-1), 1)
        res[i] = ch_ix
        x = ch_ix[None]
        yield ch_ix.item()

class RNNDecoder(nn.Module):
    """Simple RNN FC decoder getting the last hidden state from the encoder"""
    def __init__(self, emb_size: int, hidden_sizes: list[int], output_size: int):
        super().__init__()
        self.emb_size = emb_size
        self.hidden_sizes = [emb_size, *hidden_sizes, output_size]
        self.fcs = nn.ModuleList(nn.Linear(i, o) for i, o in zip(self.hidden_sizes[:-1], self.hidden_sizes[1:]))

    def forward(self, x: list[tr.Tensor]) -> tr.Tensor:
        # x = unpack_sequence(x_packed)
        y_fcs, sizes = pad(x)
        for fc in self.fcs[0:-1]:
            y_fcs = F.relu(fc(y_fcs))
        y_out = self.fcs[-1](y_fcs)
        y_out_unpadded = unpad(y_out, sizes)
        return y_out_unpadded

class LMRNNInputHandler(nn.Module):
    def __init__(self, n_tokens: int, emb_size: int, embedding_type: str):
        super().__init__()
        self.n_tokens = n_tokens
        self.emb_size = emb_size
        self.embedding_type = embedding_type
        self.content_embedding = build_embedding_layer(embedding_type, n_tokens, emb_size=emb_size)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        """Preprocessing of the input tensor (positional encoding and patch projection). x :: (B, t, [HxWxC])"""
        assert len(x.shape) == 2, f"Expected (B, t), got {x.shape}"
        x_emb = self.content_embedding(x) # (B, t, E)
        return x_emb

def build_rnn_model(model_cfg: DictConfig, n_tokens: int) -> tuple[nn.Module, Callable]:
    """builds the rnn model"""
    input_handler = LMRNNInputHandler(n_tokens=n_tokens, **model_cfg.input_handler)
    encoder = RNN(input_handler.emb_size, **model_cfg.encoder.parameters)
    decoder = RNNDecoder(input_handler.emb_size, **model_cfg.decoder.parameters, output_size=n_tokens)
    model = nn.ModuleDict({"input_handler": input_handler, "encoder": encoder, "decoder": decoder})
    return model, partial(rnn_autoregress, model=model)
