"""build embedding layer for the language models"""
from __future__ import annotations
import torch as tr
from torch import nn
from torch.nn import functional as F
from loggez import loggez_logger as logger

class OneHot(nn.Module):
    def __init__(self, num_embeddings: int):
        super().__init__()
        self._num_embeddings = num_embeddings
        self.dummy = nn.Linear(1, 1, bias=False) # so we can use nn.Module.

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        res = F.one_hot(x, self.num_embeddings).float()
        return res

    @property
    def emb_size(self) -> int:
        return self.num_embeddings

    @property
    def num_embeddings(self) -> int:
        return self._num_embeddings

    def __call__(self, x: tr.Tensor) -> tr.Tensor:
        return self.forward(x)

def build_embedding_layer(embedding_type: str, n_embeddings: int, emb_size: int | None) -> nn.Embedding:
    assert embedding_type in ("embedding_layer", "one_hot"), embedding_type
    logger.info(f"Building embedding. Type: '{embedding_type}'. Dim: {emb_size}. N: {n_embeddings}")
    if embedding_type == "embedding_layer":
        return nn.Embedding(n_embeddings, emb_size)
    else:
        assert emb_size == n_embeddings, (emb_size, n_embeddings)
        return OneHot(n_embeddings)
