"""build ngram model"""
from typing import Iterator, Callable
from functools import partial
from omegaconf import DictConfig
import torch as tr
from torch import nn
from torch.nn import functional as F

from .ngram import NGram
from .build_embedding_layer import build_embedding_layer

@tr.no_grad()
def ngram_autoregress(seed: tr.Tensor, n: int, model: nn.Module, temperature: float = 1.0) -> Iterator[int]:
    # The user may provide a smaller seed than the ngram size, in which case we pad it with zeros.
    assert temperature == 1, temperature
    if len(seed) < model.ngram:
        seed = F.pad(seed, [model.ngram - len(seed), 0])
    res = tr.zeros(n + len(seed)).long().to(seed.device)
    res[0: len(seed)] = seed
    for i in range(len(seed), len(res)):
        x = res[i - model.ngram:i][None]
        y = model.forward(x)
        y_sample = tr.multinomial(F.softmax(y, dim=-1).squeeze(), num_samples=1)
        res[i] = y_sample
        yield y_sample.item()

def build_ngram_model(model_cfg: DictConfig, n_tokens: int) -> tuple[nn.Module, Callable]:
    """builds the ngram model given cfg and n_tokens"""
    embedding_layer = build_embedding_layer(model_cfg.embedding.type, n_tokens,
                                        model_cfg.embedding.parameters.emb_size)
    model = NGram(embedding_layer, **model_cfg.parameters)
    return model, partial(ngram_autoregress, model=model)
