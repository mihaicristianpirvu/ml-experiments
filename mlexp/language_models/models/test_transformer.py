import torch as tr
from mlexp.language_models.models.transformer import Transformer

def test_transformer():
    B, t, E = 5, 16, 100
    x = tr.randn(B, t, E)

    # testing the model itself w/ regular projected inputs
    nn1 = Transformer(emb_size=E, n_blocks=1, n_heads=2, ff_multiplier=1, dropout=0.1,
                      causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    assert (y := nn1(x)).shape == (B, t, E), y

    nn2 = Transformer(emb_size=E, n_blocks=2, n_heads=E // 2, ff_multiplier=1, dropout=0.1,
                      causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    assert (y2 := nn2(x)).shape == (B, t, E), y2

    try:
        _ = Transformer(emb_size=E, n_blocks=2, n_heads=2 * E, ff_multiplier=1, dropout=0.1,
                        causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    except AssertionError as e:
        assert f"({E}, {2 * E})" in f"{e}"

    nn3 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=2, dropout=0.1,
                      causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    assert (y3 := nn3(x)).shape == (B, t, E), y3

    try:
        _ = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=-1,
                        causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    except ValueError as e:
        assert "dropout probability has to be between 0 and 1, but got -1" in f"{e}"

    nn4 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=False, return_all_states=False, attn_bias=False, ln_blocks=False)
    assert (y4_1 := nn4(x, kv=x)).shape == (B, t, E), y4_1
    assert (y4_2 := nn4(x, kv=tr.randn(B, t * 2, E))).shape == (B, t, E), y4_2 # xatt allows kv of different t shape !
    assert (y4_2 := nn4(x, kv=y3)).shape == (B, t, E), y4_2 # xatt allows other transformer outputs as well in kv

    nn5 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=True, return_all_states=False, attn_bias=False, ln_blocks=False) # causal (iGPT or LMs)
    assert (y5 := nn5(x)).shape == (B, t, E), y5

    nn6 = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0,
                      causal=False, return_all_states=True, attn_bias=False, ln_blocks=False)
    assert len(y6 := nn6(x)) == 2 and all(_y.shape == (B, t, E) for _y in y6), y6

def test_transformer_vanilla_attention():
    B, t, E = 5, 16, 100
    x = tr.randn(B, t, E)

    nn1_sdpa = Transformer(emb_size=E, n_blocks=1, n_heads=1, ff_multiplier=1, dropout=0.0, causal=False,
                           return_all_states=False, attn_bias=False, ln_blocks=False)
    nn1_va = Transformer(emb_size=E, n_blocks=1, n_heads=1, ff_multiplier=1, dropout=0.0, causal=False,
                         return_all_states=False, attn_bias=False, ln_blocks=False, mha_fn="vanilla_attention")
    nn1_va.load_state_dict(nn1_sdpa.state_dict())
    y1_sdpa, y1_va = nn1_sdpa(x), nn1_va(x)
    assert tr.allclose(y1_va, y1_sdpa, rtol=1e-2), (y1_va, y1_sdpa)

    nn2_sdpa = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0.0, causal=False,
                           return_all_states=False, attn_bias=False, ln_blocks=False)
    nn2_va = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0.0, causal=False,
                         return_all_states=False, attn_bias=False, ln_blocks=False, mha_fn="vanilla_attention")
    nn2_va.load_state_dict(nn2_sdpa.state_dict())
    y2_sdpa, y2_va = nn2_sdpa(x), nn2_va(x)
    assert tr.allclose(y2_va, y2_sdpa, rtol=1e-2), (y2_va, y2_sdpa)

    nn3_sdpa = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0.0, causal=True,
                           return_all_states=False, attn_bias=False, ln_blocks=False)
    nn3_va = Transformer(emb_size=E, n_blocks=2, n_heads=2, ff_multiplier=1, dropout=0.0, causal=True,
                         return_all_states=False, attn_bias=False, ln_blocks=False, mha_fn="vanilla_attention")
    nn3_va.load_state_dict(nn3_sdpa.state_dict())
    y3_sdpa, y3_va = nn3_sdpa(x), nn3_va(x)
    assert tr.allclose(y3_va, y3_sdpa, rtol=1e-2), (y3_va, y3_sdpa)
