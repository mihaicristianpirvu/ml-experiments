from functools import lru_cache
import numpy as np
import pytest
from mlexp import get_project_root
from mlexp.language_models.tokenizers import build_tokenizer, build_tokenizer_metadata

@lru_cache(maxsize=1)
@pytest.fixture
def data() -> str:
    return open(f"{get_project_root()}/data/shakespeare/shakespeare.txt", "r").read()

def test_text_tokenizer_character(data):
    alphabet = sorted(set(data))
    raw_data = data[0:150]

    tokenizer = build_tokenizer(tokenizer_type="character", alphabet_mode="provided", alphabet=alphabet[::-1])
    metadata = build_tokenizer_metadata(tokenizer)
    assert metadata["tokenizer_type"] == "character" and tokenizer.alphabet == metadata["alphabet"]
    encoded_data = tokenizer.encode(raw_data)
    decoded_data = tokenizer.decode(encoded_data)
    assert raw_data == decoded_data

    tokenizer2 = build_tokenizer(tokenizer_type="character", alphabet_mode="build_from_dataset", raw_data=data)
    encoded_data2 = tokenizer2.encode(raw_data)
    decoded_data2 = tokenizer2.decode(encoded_data2)
    assert raw_data == decoded_data2
    assert not np.allclose(encoded_data2, encoded_data)

    metadata2 = build_tokenizer_metadata(tokenizer2)
    assert metadata2["tokenizer_type"] == "character" and tokenizer2.alphabet == metadata2["alphabet"]
    assert metadata2["alphabet_mode"] == "provided"
    tokenizer3 = build_tokenizer(**metadata2)
    encoded_data3 = tokenizer3.encode(raw_data)
    decoded_data3 = tokenizer3.decode(encoded_data2)
    assert raw_data == decoded_data3
    assert np.allclose(encoded_data2, encoded_data3)

def test_text_tokenizer_bpe_gpt2(data, caplog):
    alphabet = sorted(set(data))
    _ = build_tokenizer(tokenizer_type="bpe_gpt2", alphabet=alphabet)
    assert "No kwargs expected for gpt-2. Found: dict_keys(['alphabet'])" in caplog.text
    tokenizer = build_tokenizer(tokenizer_type="bpe_gpt2")
    raw_data = data[0:150]
    encoded_data = tokenizer.encode(raw_data)
    decoded_data = tokenizer.decode(encoded_data)
    assert raw_data == decoded_data

    metadata = build_tokenizer_metadata(tokenizer)
    tokenizer2 = build_tokenizer(**metadata)
    encoded_data2 = tokenizer2.encode(raw_data)
    decoded_data2 = tokenizer2.decode(encoded_data)
    assert raw_data == decoded_data2
    assert encoded_data == encoded_data2
