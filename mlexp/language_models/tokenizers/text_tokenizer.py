#!/usr/bin/env python3
"""text_tokenizer module"""
from __future__ import annotations
from argparse import ArgumentParser, Namespace
from pathlib import Path
import json
import numpy as np
from loggez import loggez_logger as logger
import tiktoken

def build_tokenizer(tokenizer_type: str, **kwargs) -> tiktoken.Encoding:
    """builds the tokenizer (i.e. character or bpe_gpt2 for now). Returns a tiktoken.Encoding object"""
    assert tokenizer_type in ("character", "bpe_gpt2"), f"Unknown tokenizer: {tokenizer_type}"
    res = None
    if tokenizer_type == "character":
        from mlexp.language_models.tokenizers import CharTokenizer
        assert "alphabet_mode" in kwargs, kwargs
        alphabet = None
        if kwargs["alphabet_mode"] == "build_from_dataset":
            alphabet = CharTokenizer.alphabet_from_data(kwargs["raw_data"])
        elif kwargs["alphabet_mode"] == "ascii":
            alphabet = CharTokenizer.ascii_alphabet()
        elif kwargs["alphabet_mode"] == "provided":
            alphabet = kwargs["alphabet"]
        else:
            raise ValueError(f"Unknown alphabet_mode: {kwargs['alphabet_mode']}")
        assert isinstance(alphabet, list), type(alphabet)
        res = CharTokenizer(alphabet)
    if tokenizer_type == "bpe_gpt2":
        if len(kwargs) > 0:
            logger.warning(f"No kwargs expected for gpt-2. Found: {kwargs.keys()}")
        res = tiktoken.get_encoding("gpt2")
    assert res is not None, f"Unknown {tokenizer_type=}"
    logger.info(f"Built tokenizer {res.name}. Alphabet len: {res.max_token_value}")
    return res

def build_tokenizer_metadata(tokenizer: tiktoken.Encoding) -> dict[str, "Any"]:
    """Returns a json that can be used to reconstruct the tokenizer via build_tokenizer above. Used in models cfg."""
    if hasattr(tokenizer, "char_to_ix"):
        return {"tokenizer_type": "character", "alphabet_mode": "provided", "alphabet": tokenizer.alphabet}
    if tokenizer.name == "gpt2":
        return {"tokenizer_type": "bpe_gpt2"}
    raise ValueError(f"Unknown tokenizer: '{tokenizer}'")

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("raw_dataset_path", type=Path)
    parser.add_argument("--encoding", required=True, help="The type of tokenization. Atm:'character' or 'bpe_gpt2'")
    parser.add_argument("--output_path", "-o", type=Path, required=True)
    parser.add_argument("--output_path_metadata", "-o_meta", type=Path, required=True,
                        help=("where to store information about the tokenizer. Useful when loading the "
                              "tokenized data to build the tokenizer"))
    parser.add_argument("--mode", choices=["build_from_dataset", "ascii"])
    parser.add_argument("--overwrite", action="store_true")
    args = parser.parse_args()
    assert args.encoding in ("character", "bpe_gpt2"), args.encoding
    assert not args.output_path.exists() or args.overwrite, f"'{args.output_path}' exists. Use --overwrite"
    assert not args.output_path_metadata.exists() or args.overwrite, \
        f"'{args.output_path_metadata}' exists. Use --overwrite"
    return args

def main(args: Namespace):
    """main fn"""
    raw_data = open(args.raw_dataset_path, "r").read()
    logger.info(f"Read {len(raw_data)} characters from '{args.raw_dataset_path}'")

    text_tokenizer = build_tokenizer(tokenizer_type=args.encoding, alphabet_mode=args.mode, raw_data=raw_data)
    tokenizer_meta = build_tokenizer_metadata(text_tokenizer)

    encoded_data = text_tokenizer.encode(raw_data)
    logger.info(f"Encoded data: {len(encoded_data)} tokens")
    np.save(args.output_path, encoded_data)
    logger.info(f"Saved encoded data to '{args.output_path}'")
    json.dump(tokenizer_meta, open(args.output_path_metadata, "w"), indent=2)
    logger.info(f"Saved tokenizer metadata to '{args.output_path_metadata}'")

if __name__ == "__main__":
    main(get_args())
