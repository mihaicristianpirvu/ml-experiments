"""Character level tokenizer"""
from __future__ import annotations
import numpy as np
import tiktoken
from overrides import overrides
from loggez import loggez_logger as logger

class CharTokenizer(tiktoken.Encoding):
    """Wrapper on top of tiktoken.Encoding to support char encoding and some properties we use from them later on"""

    def __init__(self, alphabet: list[str], special_tokens: dict[str, int] = {}):
        assert isinstance(alphabet, list), f"Expected a list of characters, got {type(alphabet)}"
        assert all(len(ch) >= 0 for ch in alphabet), f"Expected character level alphabet: {alphabet}"
        assert all(len(ch) == 1 for ch in special_tokens), f"Expected character level tokens: {special_tokens}"
        char_to_ix: dict[str, int] = {c: i for c, i in zip(alphabet, range(len(alphabet)))}
        super().__init__(name="CharTokenizer", pat_str="", special_tokens=special_tokens,
                         mergeable_ranks={c.encode("utf8"): i for c, i in char_to_ix.items()}) # wants bytes

        self.alphabet = alphabet
        self.char_to_ix = {**char_to_ix, **special_tokens}
        self.ix_to_char: dict[int, str] = {i: c for c, i in self.char_to_ix.items()}

    @property
    def eot_token(self) -> int:
        return -1

    @overrides
    def encode(self, text: str, **kwargs) -> list[int]:
        """encode a text given a specific encoding type"""
        return [self.char_to_ix[c] for c in text]

    @overrides(check_signature=False)
    def decode(self, encoded_text: list[int] | np.ndarray, **kwargs) -> str:
        """decode a text given a specific encoding type"""
        assert isinstance(encoded_text, list) or \
               isinstance(encoded_text, np.ndarray) and encoded_text.dtype == int, type(encoded_text)
        return "".join([self.ix_to_char[ix] for ix in encoded_text])

    @staticmethod
    def alphabet_from_data(data: list[str]) -> list[str]:
        """builds the alphabet from the data itself"""
        alphabet = sorted(set(data))
        logger.info(f"Alphabet size generated from text: {len(alphabet)}")
        return alphabet

    @staticmethod
    def ascii_alphabet() -> list[str]:
        """returns the ascii alphabet"""
        return [chr(i) for i in range(128)]

    def __repr__(self):
        return f"[CharTokenizer]. Alphabet: {len(self.alphabet)}. Special tokens: {self.special_tokens_set}"
