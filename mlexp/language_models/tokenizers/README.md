# Language model data submodule

## Preface

### You can skip to usage if you know about tokens and embeddings

Models work with floating point numbers, not words or characters. We need to make an encoding of these words into numbers. These are usually called embeddings and can come in 2 forms: volatile (changing during training) or fixed (learned from another previous model).

When pre-training a language model, we usually want to also learn the embeddings, while when fine-tuning, usually we keep them fixed, so only the downstream task (i.e. chatbot) is learned, while the words low-level semantics and relationships are kept fixed.

In order to achieve this, usually we encode the text from chars and words into tokens by assining a natural number. Once this vocabulary is set, it will be used for all the tasks. We can add new words to the vocabulary later (i.e. special characters for chat bots interaction like taking turns when speaking), however, for pre-training, we usually want to map all the possible characters or words into a singular unique number.

Multiple conversion schemas `word/subword/character -> token_id` have been invented. In this repo, we support only 2 schemas: [character level](http://karpathy.github.io/2015/05/21/rnn-effectiveness/) and byte-pair encoding(https://en.wikipedia.org/wiki/Byte_pair_encoding) using the [tiktoken](https://github.com/openai/tiktoken) library introduced for GPT-2 by OpenAI.

Byte-pair encoding have compression-level advantages, as it maps popular subwords given by statistics into token indexes, rather than assigning the same weight to all of them. For example, the word `the` might get its own index, while the word `therefore` will probably be split into `there` + `for` + `e`. We use GPT2's BPE encoding here, which has a bias towards english words. Note: this allows us to encode larger texts better by effecitvely compressing it to token ids, however, it will add a bias towards some words. A good side-effect is that it will also minimize typos. [Here's a summary](https://twitter.com/karpathy/status/1759996551378940395) of the good and bad parts of (byte pair encoding) tokenization:
![alt text](tokenization_issues.png).

## How to use the tokenizer

```bash
./text_tokenizer.py \
    /path/to/text_file.txt \
    -o /path/to/tokenized_file.npy \
    [--encoding {character/bpe_gpt2}] \
    [--alphabet_out_path /path/to/alphabet.npy] # only for 'character' encoding. Defaults to /path/to/tokenized_file/../alphabet.npy
```

We have 2 modes we support:

### 1. Text -> Chararcter-level Encoding

- Input: one text file (.txt)
- Outputs: one tokenized file (.npy) and one alphabet file (.npy).

The alphabet file is just a mapping between all unique characters in the text file.

Example:
```bash
./text_tokenizer.py ../../data/shakespeare/shakespeare.txt -o char_shakespeare.npy --encoding character
```
Outputs:
```
[20240444 09:43-WARNING] --alphabet_out_path not set. Defaulting to 'alphabet.npy (text_tokenizer.py:122)
[20240444 09:43-INFO] Alphabet size generated from text: 61 (text_tokenizer.py:131)
[20240444 09:43-INFO] Saved alphabet to 'alphabet.npy' (text_tokenizer.py:133)
[20240444 09:43-INFO] Encoded data: 94275 tokens (text_tokenizer.py:137)
[20240444 09:43-INFO] Saved encoded data to 'char_shakespeare.npy' (text_tokenizer.py:139)
```

### 2. Text -> Byte Pair Encoding (GPT-2)

- Input: one text file (.txt)
- Output: one tokenized file (.npy)

Alphabet is provided by the GPT-2 BPE statistics and is loaded from the tiktoken load library. Therefore, we don't need to provide it.

```bash
./text_tokenizer.py ../../data/shakespeare/text -o bpe_shakespeare.npy --encoding bpe_gpt2
```

Optimized variant (outputs 2 .bin files) for `openwebtext`: [link](https://github.com/karpathy/nanoGPT/blob/master/data/openwebtext/prepare.py).
