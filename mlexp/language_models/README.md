# Language Models

This repo implements language models. There are two steps usually done: pretraining on a large corpus of text with some
prediction criterion: next token prediction (GPT) or masked auto encoder (BERT). Then, using the pretrained model,
you can use the resulting checkpoint to fine-tune on a secondary (or downstream) task with greater performance than
training from scratch. The downstream task can be classification (i.e. SQUAD dataset), assistent fine tuning (i.e.
chatbot), text summarization, role playing or anything language related.

Start with [pretraining](./pretraining) and then go into [chat fine tuning](./chat_fine_tuning).

## Supported models

### Models
- N gram: [link](https://www.youtube.com/watch?v=PaCmpygFfXo)
  - deeper net as well as more n-grams.
- Char RNN & LSTM: [link](http://karpathy.github.io/2015/05/21/rnn-effectiveness/)
  - RNN from scratch
  - LSTM from pytorch
- Transformer
  - Transformer from scratch ([Karpathy's video](https://youtu.be/kCc8FmEb1nY))
  - Transformer using flash attention from pytorch.
- TODO: mamba/s4, rwvk
