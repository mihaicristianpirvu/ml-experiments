#!/usr/bin/env python3
"""simple script to run a pre-trained model that generates random text"""
from __future__ import annotations
from argparse import Namespace, ArgumentParser
from pathlib import Path
from datetime import datetime
from loggez import loggez_logger as logger
import torch as tr
import os
from omegaconf import OmegaConf
from lightning_module_enhanced import LME

from mlexp.language_models.tokenizers import build_tokenizer
from mlexp.language_models.models import build_model, AutoregressModel

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("weights_path", type=Path)
    parser.add_argument("--seed", default="Hello")
    parser.add_argument("--n", type=int, default=100)
    parser.add_argument("--temperature", type=float, default=1)
    args = parser.parse_args()
    return args

@tr.no_grad()
def main(args: Namespace):
    """main fn"""
    ckpt_data = tr.load(args.weights_path, map_location="cpu")
    cfg = OmegaConf.create(ckpt_data["hyper_parameters"]["model_cfg"])
    tokenizer = build_tokenizer(**OmegaConf.to_container(cfg.data.tokenizer_metadata))
    if cfg.model.type == "transformer":
        cfg.model.encoder.parameters.return_kv = os.getenv("KVCACHE", "1") == "1"
    model: LME | AutoregressModel = build_model(cfg.model, tokenizer)
    model.load_state_dict(ckpt_data["state_dict"], strict=True)
    model.to(device := ("cuda" if tr.cuda.is_available() else "cpu")).eval()
    logger.info(f"Loaded model. Num params: {LME(model).num_params/1e6:.2f}M. Device: {device}.")

    print(f"Seed: {args.seed}")
    now = datetime.now()
    tr_seed = tr.LongTensor(tokenizer.encode(args.seed)).to(device)
    for ch in model.autoregress(seed=tr_seed, n=args.n, temperature=args.temperature):
        print(tokenizer.decode([ch]), end="", flush=True)
    print("")
    logger.info(f"Took: {(took := (datetime.now() - now).total_seconds()):.2f} seconds. Avg tok/s: {args.n / took:.2f}")

if __name__ == "__main__":
    main(get_args())
