"""Build model algorithm for pretraining Language Models"""
from typing import Callable
from omegaconf import DictConfig
import tiktoken

from .next_token_prediction import rnn_next_token_prediction, transformer_next_token_prediction, \
    ngram_next_token_prediction

def build_model_algorithm(cfg: DictConfig, tokenizer: tiktoken.Encoding) -> Callable:
    """Returns a model_algorithm function given a model type for pretraining language models"""
    algorithm_fn: Callable | None = None
    if cfg.model.type == "ngram":
        if cfg.train.type == "next_token_prediction":
            algorithm_fn = ngram_next_token_prediction

    if cfg.model.type == "rnn":
        if cfg.train.type == "next_token_prediction":
            algorithm_fn = rnn_next_token_prediction

    if cfg.model.type == "transformer":
        if cfg.train.type == "next_token_prediction":
            algorithm_fn = transformer_next_token_prediction

    if algorithm_fn is None:
        raise ValueError(f"No algorithm found for model: '{cfg.model.type}' and train type: '{cfg.train.type}'")
    return algorithm_fn
