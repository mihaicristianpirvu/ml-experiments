"""Next token prediction algorithms for various supported models for language modeling"""
from __future__ import annotations
from lightning_module_enhanced import LME, ModelAlgorithmOutput
import torch as tr
from torch.nn import functional as F

def rnn_next_token_prediction(model: LME, batch: tuple[tr.Tensor, tr.Tensor]) -> ModelAlgorithmOutput:
    """Next Token Prediction algorithm for RNN"""
    x_emb = model.base_model.input_handler(batch)
    # get sequences of random lens from the batch, which has a fixed length (from RandomFixedSizedSliceDataset)
    random_lens = tr.randint(2, batch.shape[1], (len(batch), )).to(model.device)
    x_enc = [x[0: l - 1] for x, l in zip(x_emb, random_lens)]
    gt = [x[1: l] for x, l in zip(batch, random_lens)] # note: gt is not embedded

    y_enc, _ = model.base_model.encoder(x_enc) # y_enc is just the last rnn cell's full state (hidden is 2nd return)
    y_dec = model.base_model.decoder(y_enc)

    # loss through all the intermediate predictions (not just the last one), this is why we use y_enc, not h
    y_loss = tr.cat(y_dec)
    gt_loss = tr.cat(gt)
    loss = F.cross_entropy(y_loss, gt_loss)
    loss_last_pred = F.cross_entropy(tr.stack([_y[-1] for _y in y_dec]).to(model.device),
                                     tr.tensor([_y[-1] for _y in gt]).long().to(model.device))
    return y_dec, {"loss": loss, "loss_last_pred": loss_last_pred}, (x_enc, y_enc), gt

def transformer_next_token_prediction(model: LME, batch: tuple[tr.Tensor, tr.Tensor]) -> ModelAlgorithmOutput:
    x_emb = model.base_model.input_handler(batch)
    x_enc, gt = x_emb[:, 0:-1], batch[:, 1:]

    y_enc = model.base_model.encoder(x_enc)
    y_dec = model.base_model.decoder(y_enc)
    loss = F.cross_entropy(y_dec.view(-1, model.base_model.input_handler.n_tokens), gt.reshape(-1))
    loss_last_pred = F.cross_entropy(y_dec[:, -1], gt[:, -1])
    return y_dec, {"loss": loss, "loss_last_pred": loss_last_pred}, (x_enc, y_enc), gt

def ngram_next_token_prediction(model: LME, batch: tuple[tr.Tensor, tr.Tensor]) -> ModelAlgorithmOutput:
    """ngram model algorithm"""
    # batch: (MB, max_seq_size) => x_ngram: (MB, max_seq_size - ngram_size, ngram_size)
    # gt_ngram: (MB, max_seq_size - ngram_size, 1)
    mb, max_seq_size = batch.shape
    ngram = model.base_model.ngram
    x_ngram = tr.zeros(mb, max_seq_size - ngram, ngram).long()
    gt_ngram = tr.zeros(mb, max_seq_size - ngram).long()

    for i in range(max_seq_size - ngram):
        x_ngram[:, i] = batch[:, i: i + ngram]
        gt_ngram[:, i] = batch[:, i + ngram]
    x_ngram_reshaped = x_ngram.reshape(-1, ngram).to(model.device)
    gt_ngram_reshaped = gt_ngram.reshape(-1).to(model.device)
    gt_ngram = gt_ngram.to(model.device)

    y_reshaped = model.forward(x_ngram_reshaped)
    metrics = {
        "loss": F.cross_entropy(y_reshaped, gt_ngram_reshaped),
        "loss_last_pred": F.cross_entropy(y_reshaped.reshape(*gt_ngram.shape, -1)[:, -1], gt_ngram[:, -1]),
    }
    return y_reshaped, metrics, x_ngram_reshaped, gt_ngram
