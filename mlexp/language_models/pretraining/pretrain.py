#!/usr/bin/env python3
from __future__ import annotations
from argparse import ArgumentParser, Namespace
from pathlib import Path
from datetime import datetime
import json
import os
from omegaconf import OmegaConf, DictConfig
from loggez import loggez_logger as logger
import tiktoken
import torch as tr
import numpy as np
import pandas as pd
from torch.utils.data import DataLoader, Dataset
from pytorch_lightning import Trainer, Callback
from pytorch_lightning.callbacks import ModelCheckpoint
from pytorch_lightning.loggers import CSVLogger, WandbLogger
from pytorch_lightning.utilities import rank_zero_only
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics

from mlexp.language_models.pretraining.lm_dataset import LanguageModelingDataset
from mlexp.language_models.pretraining.algorithms import build_model_algorithm
from mlexp.language_models.models import build_model, AutoregressModel
from mlexp.language_models.tokenizers import build_tokenizer

class GenerateTextCallback(Callback):
    def __init__(self, tokenizer: tiktoken.Encoding, val_reader: Dataset, sequence_size: int, seed_min_length: int,
                 qa_seeds: bool = False):
        self.tokenizer = tokenizer
        self.sequence_size = sequence_size
        self.seeds = self._get_seeds(val_reader, seed_min_length, qa_seeds)

    @tr.no_grad()
    def on_train_epoch_end(self, trainer: Trainer, pl_module: LME | AutoregressModel) -> None:
        if rank_zero_only.rank != 0:
            return
        seed: str = np.random.choice(self.seeds)
        # the models operate on LongTensors which they will embed, autoregress and return a new long tensor
        tr_seed = tr.LongTensor(self.tokenizer.encode(seed, allowed_special=set()))
        tr_generated = [c for c in pl_module.autoregress(seed=tr_seed.to(pl_module.device), n=self.sequence_size)]
        generated = self.tokenizer.decode(tr_generated) if len(tr_generated) > 0 else ""
        csv_file = Path(f"{trainer.loggers[0].log_dir}/gen.csv")
        if not csv_file.exists():
            open(csv_file, "w").write("seed,generated\n")

        seed = seed.replace("\n", "<EOL>").replace("\"", "<QUOTE>")
        res = generated.replace("\n", "<EOL>").replace("\"", "<QUOTE>")
        df = pd.read_csv(csv_file)
        df.loc[len(df)] = [seed, res]
        df.to_csv(csv_file, index=False)

    def _get_seeds(self, val_reader: Dataset, seed_min_length: int, qa_seeds: bool) -> list[str]:
        if qa_seeds:
            return [x for x in self.tokenizer.decode(val_reader.data[0:].numpy()).split("\n") if x.startswith("Q:")]
        text_data = self.tokenizer.decode(val_reader.data[0:10000].numpy()).replace("\n", " ")
        return [x for x in set(text_data.split()) if len(x) >= seed_min_length]

def _build_data(cfg: DictConfig, train_path: Path, validation_path: Path | None) -> tuple[Dataset, Dataset]:
    """reads the data. If only train_set is provided, then splits it before making a reader"""
    def _load(path: Path) -> np.ndarray:
        assert path.suffix in (".npy", ".bin"), f"Unknown suffix: '{path.suffix}'"
        return np.load(path) if path.suffix == ".npy" else np.memmap(path, dtype=np.uint16, mode="r")
    _train_set = _load(train_path)
    if validation_path is None:
        logger.warning(f"No validation set provided. Splitting train set ({len(_train_set)}) using an 80-20% ratio.")
        _train_set, _val_set = _train_set[0: int(len(_train_set) * 0.8)], _train_set[-int(len(_train_set) * 0.2):]
    else:
        _val_set = _load(validation_path)
    cfg.train.trainer_params.max_epochs = 2 if os.getenv("DEBUG", "0") == "1" else cfg.train.trainer_params.max_epochs
    train_set = LanguageModelingDataset(_train_set, cfg.data.max_seq_size, cfg.train.epoch_size)
    val_set = LanguageModelingDataset(_val_set, cfg.data.max_seq_size, cfg.train.val_epoch_size)
    return train_set, val_set

def load_cfg(args: Namespace) -> DictConfig:
    """load cfg and update the '???' parts from argparse"""
    cfg = OmegaConf.load(args.config_path)
    cfg.data.tokenizer = args.tokenizer # fill the '???' in the config from argparse
    cfg.data.alphabet = np.load(args.alphabet_path).tolist() if args.alphabet_path is not None else "gpt2"
    return cfg

def get_args() -> Namespace:
    """cli args"""
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True)
    parser.add_argument("--dataset_path", type=Path, required=True, help="Path to npy/bin dataset that is tokenized")
    parser.add_argument("--tokenizer_metadata_path", type=Path, required=True)
    parser.add_argument("--validation_set_path", type=Path)
    parser.add_argument("--experiments_dir", type=Path, default=Path(__file__).parent / "experiments")
    parser.add_argument("--resume_from", type=Path, help="Path to model to resume from")
    parser.add_argument("--wandb_project", help="If set, will use wandb to log the training process")
    parser.add_argument("--wandb_run", help="If set, will use or reuse an existing version. Used to resume trains")
    args = parser.parse_args()
    return args

def main():
    """main fn"""
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    # data
    tokenizer = build_tokenizer(**(tokenizer_metadata := json.load(open(args.tokenizer_metadata_path, "r"))))
    cfg.data.tokenizer_metadata = tokenizer_metadata
    train_reader, val_reader = _build_data(cfg, args.dataset_path, args.validation_set_path)
    logger.info(f"Built dataset.\n- Train set: {train_reader}.\n- Validation set: {val_reader}")

    # model
    model: LME | AutoregressModel = LME(build_model(cfg.model, tokenizer))
    model.hparams["model_cfg"] = json.dumps(OmegaConf.to_container(cfg, resolve=True, throw_on_missing=True))
    model.optimizer = getattr(tr.optim, cfg.train.optimizer.type)(model.parameters(), **cfg.train.optimizer.parameters)
    model.model_algorithm = build_model_algorithm(cfg, tokenizer)
    model.callbacks = [GenerateTextCallback(tokenizer, val_reader, **cfg.train.generate_text_callback), PlotMetrics()]
    model.metrics = {"loss_last_pred": (lambda y, gt: (y - gt).abs().mean(), "min")}
    model.checkpoint_monitors = ["loss", "loss_last_pred"]
    print(model.summary)

    # training
    train_loader = DataLoader(train_reader, **{**cfg.data.loader_params, "shuffle": True})
    val_loader = DataLoader(val_reader, **{**cfg.data.loader_params, "shuffle": False})
    pl_loggers = [CSVLogger(save_dir=f"{args.experiments_dir}/{args.config_path.stem}",
                            name=datetime.strftime(datetime.now(), "%Y%m%d"),
                            version=datetime.strftime(datetime.now(), "%H%M%S"))]
    Path(pl_loggers[0].log_dir).mkdir(parents=True, exist_ok=True)
    OmegaConf.save(cfg, f"{pl_loggers[0].log_dir}/config.yaml", resolve=True)
    if args.wandb_project and rank_zero_only.rank == 0:
        wandb_logger = WandbLogger(project=args.wandb_project, version=args.wandb_run)
        wandb_logger.experiment.config.update(OmegaConf.to_container(cfg), allow_val_change=True)
        pl_loggers.append(wandb_logger)
    trainer_params = {**cfg.train.trainer_params, "enable_model_summary": False}
    Trainer(logger=pl_loggers, **trainer_params).fit(model, train_loader, val_loader, ckpt_path=args.resume_from)

if __name__ == "__main__":
    main()
