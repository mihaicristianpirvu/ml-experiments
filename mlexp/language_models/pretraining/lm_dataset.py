"""LanguageModelingDataset module."""
from __future__ import annotations
import os
from torch.utils.data import Dataset
import torch as tr
import numpy as np
from loggez import loggez_logger as logger

class LanguageModelingDataset(Dataset):
    """
    Wrapper that gives us 2 things:
    - max_seq_size: Ability to get random substrings of fixed size from the text
    - epoch_length. The length of the dataset, so we can control the epoch size
    """
    def __init__(self, data: np.ndarray, max_seq_size: int, epoch_length: int | None = None):
        if epoch_length is None:
            logger.info(f"Explicit dataset length not set. Defaulting to data size: {epoch_length := len(data)}")
        if epoch_length > len(data):
            logger.warning(f"Raw data is smaller than requested length: {len(data)} vs {epoch_length}")
        assert len(data) > max_seq_size, f"{len(self.data)=}, {max_seq_size=}"
        self.data = tr.from_numpy(data)
        self.max_seq_size = max_seq_size
        self.epoch_length = epoch_length

    def __getitem__(self, ix: int) -> tr.Tensor:
        rand_ix = np.random.randint(1, len(self.data) - self.max_seq_size) # slice random indexes of fixed size
        data = self.data[rand_ix: rand_ix + self.max_seq_size].long()
        return data

    def __len__(self) -> int:
        return self.epoch_length if os.getenv("DEBUG", "0") == "0" else 2

    def __repr__(self):
        return f"[LanguageModelingDataset] Raw length={len(self.data)}, {self.max_seq_size=}, {self.epoch_length=}"
