# Pre-training Language Models

This is the 'meat' of language modeling. We start by training a model from scratch on some generic task, such as next
token prediction or masked auto encoder.

Supported models can be found in the [models](./models/), while supported model algorithms can be found in the [algorithms](./algorithms/) directory. Each model/algorithm pair is accompanied by a [model config](./cfgs/).

The dataset can be a text file ([shakespeare.txt](../../data/shakespeare/shakespeare.txt)) or a tokenized dataset ([shakespeare.npy](../../data/shakespeare/shakespeare.npy)). In order to tokenize a dataset, see the [tokenizer](../tokenizer/) directory.

## Pretraining a model

```bash
./pretrain.py \
    --config_path /path/to/config \
    --dataset_path /path/to/dataset.{txt/npy} \
    [--validation_set_path /path/to/validation_set.{txt/npy}] \
    [--alphabet_path /path/to/alphabet.npy] \
    [--wandb_project W] \
    [--resume_from /path/to/ckpt]
```
