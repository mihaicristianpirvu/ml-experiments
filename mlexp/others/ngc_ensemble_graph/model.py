import itertools
from typing import List
from functools import reduce
import torch as tr
import torch.nn as nn
import torch.nn.functional as F
from nwgraph import Graph
from nwgraph.nodes import DimNode
from nwgraph.layers import EnsembleMp

def prod(x: list[int]) -> int:
    """returns the product of the elements of an integer list"""
    return reduce(lambda a, b: a * b, x, 1)

class MNISTFCEdge(nn.Module):
    def __init__(self, in_features: int, out_features: int):
        super().__init__()
        self.fc1 = nn.Linear(in_features, 100)
        self.fc2 = nn.Linear(100, out_features)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        x = x.reshape(-1, self.fc1.in_features)
        y1 = F.relu(self.fc1(x))
        y2 = self.fc2(y1)
        return y2

class MNISTConvEdge(nn.Module):
    def __init__(self, in_channels: List[int], out_channels: List[int]):
        super().__init__()
        self.conv1 = nn.Conv2d(in_channels[0], 32, kernel_size=(3, 3), padding=1)
        self.conv2 = nn.Conv2d(32, 32, kernel_size=(3, 3), padding=1)
        self.conv3 = nn.Conv2d(32, 32, kernel_size=(3, 3), padding=1)
        self.conv4 = nn.Conv2d(32, out_channels[0], kernel_size=(3, 3), padding=1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y1 = F.relu(self.conv1(x))
        y2 = F.relu(self.conv2(y1))
        y3 = F.relu(self.conv3(y2))
        y4 = self.conv4(y3)
        return y4

def get_conv_ens_mp() -> EnsembleMp:
    node_names = ["rgb_top_left", "rgb_top_right", "rgb_bottom_left", "rgb_bottom_right"]
    # 1 channel in, 1 channel out.
    node_args = {"dims": (1, 28, 28)}
    edges = [*itertools.combinations(node_names, 2), *[(x[1], x[0]) for x in itertools.combinations(node_names, 2)]]
    graph = Graph(edge_indexes=edges, node_names=node_names, node_types=DimNode, node_args=node_args)
    ens_mp = EnsembleMp(graph, edge_model_type=MNISTConvEdge, ensemble_type="mean")
    return ens_mp


def get_fc_ens_mp() -> EnsembleMp:
    node_names = ["rgb_top_left", "rgb_top_right", "rgb_bottom_left", "rgb_bottom_right"]
    node_args = {"dims": (28 * 28,)}
    edges = [*itertools.combinations(node_names, 2), *[(x[1], x[0]) for x in itertools.combinations(node_names, 2)]]
    graph = Graph(edge_indexes=edges, node_names=node_names, node_types=DimNode, node_args=node_args)
    ens_mp = EnsembleMp(graph, edge_model_type=MNISTFCEdge, ensemble_type="mean")
    return ens_mp

class Model(nn.Module):
    def __init__(self, graph_type: str):
        super().__init__()
        assert graph_type in ("conv", "fc")
        self.ens_mp = get_conv_ens_mp() if graph_type == "conv" else get_fc_ens_mp()
        self.fc1 = nn.Linear(len(self.ens_mp.graph.nodes) * prod(self.ens_mp.graph.nodes[0].dims), 100)
        self.fc2 = nn.Linear(100, 10)
        self.graph_type = graph_type

    def forward(self, x: dict):
        # reshape for fully connected message passing nodes.
        # x = x if self.graph_type == "conv" else {k: v.reshape(v.shape[0], -1) for k, v in x.items()}
        y1 = self.ens_mp(x).readout()
        y1_fc = tr.cat(tuple(y1.values()), dim=1).reshape(-1, self.fc1.in_features)
        y2 = F.relu(self.fc1(y1_fc))
        y3 = self.fc2(y2)
        return y3
