#!/usr/bin/env python3
from argparse import ArgumentParser
from datetime import datetime
from omegaconf import OmegaConf
from pathlib import Path
from typing import Tuple, Dict
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from functools import partial
from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, PlotCallback
from lightning_module_enhanced.metrics import MultiClassF1Score
from torch import optim
from torch.nn import functional as F
from torch.utils.data import DataLoader
import torch as tr
import numpy as np

from mlexp.utils import image_write, image_add_title, collage_fn, to_image

from reader import build_reader, train_val_split
from model import Model

device = "gpu" if tr.cuda.is_available() else "cpu"
tr.set_float32_matmul_precision("medium")

def plot_fn(x: Dict[str, tr.Tensor], y: tr.Tensor, gt: tr.Tensor, out_dir: str, model, grid_size: Tuple[int, int]):
    x = {k: v[0: np.prod(grid_size), 0].to("cpu").numpy() for k, v in x.items()}
    y = y[0: np.prod(grid_size)].to("cpu").softmax(dim=1).numpy()
    gt = gt[0: np.prod(grid_size)].to("cpu").numpy()
    y_max = y.max(axis=1)
    y_ind = y.argmax(axis=1)

    titles = [f"{ind} ({conf:.2f}); gt={_gt}" for ind, conf, _gt in zip(y_ind, y_max, gt)]
    subgrids = []
    for i in range(np.prod(grid_size)):
        item = collage_fn([image_resize(to_image(_x), width=100, height=100)
                           for _x in [x[node][i] for node in x.keys()]], rows_cols=(2, 2))
        subgrids.append(item)
    collage = collage_fn(subgrids, rows_cols=grid_size, titles=titles, size_px=25)
    image_write(collage, out_dir / f"{model.trainer.current_epoch}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("config_path", type=Path)
    parser.add_argument("--dataset_path", default="data/")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    model = LME(Model(cfg.model.graph_type))
    reader, collate_fn = build_reader(args.dataset_path)
    train_reader, val_reader = train_val_split(reader, cfg.data.train_val_split, cfg.data.debug)
    train_loader = DataLoader(train_reader, collate_fn=collate_fn, **cfg.data.loader_params)
    val_loader = DataLoader(val_reader, collate_fn=collate_fn, **cfg.data.loader_params)

    model.criterion_fn = F.cross_entropy
    model.metrics = {"F1-Score": MultiClassF1Score(num_classes=10)}
    assert cfg.train.optimizer.type == "Adam", cfg.train.optimizer.type
    model.optimizer = optim.Adam(model.parameters(), **cfg.train.optimizer.params)
    model.callbacks = [PlotMetrics(), PlotCallback(partial(plot_fn, grid_size=(5, 5)))]
    print(model.summary)

    pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))
    model.base_model.ens_mp.graph.to_graphviz().render(f"{pl_logger.log_dir}/graph", format="png", cleanup=True)
    trainer = Trainer(accelerator=device, logger=pl_logger, **cfg.train.trainer_params)
    trainer.fit(model=model, train_dataloaders=train_loader, val_dataloaders=val_loader)

if __name__ == "__main__":
    main()
