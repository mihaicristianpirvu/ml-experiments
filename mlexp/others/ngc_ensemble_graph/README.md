# MNIST-gnn

We train a GNN on the MNIST task. We have 4 nodes in a fully-connected graph (24 unidirectional edges). Each node sees just a quarter of the MNIST digits, so the only way to properly understand the whole image is to communicate between them. We use a graph level readout after 3 iterations of message-passing, followed by two fully connected for the classification task.

## Usage
```bash
./train.py --config_path cfgs/fc_graph.yaml
```
## Graph

<img src="resources/graph.png"/>

## Results on the validation set

<img src="resources/val_results.png"/>
