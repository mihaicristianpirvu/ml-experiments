from pathlib import Path
from typing import Callable, Tuple, List, Dict
from torch.utils.data import Dataset, Subset
from torchvision.datasets import MNIST as _MNIST
import torch as tr
import numpy as np


class MNIST(_MNIST):
    @staticmethod
    def collate_fn(batch: List[Dict]) -> Dict:
        node_names = batch[0]["data"].keys()
        mb = len(batch)
        gt = tr.Tensor([y["labels"] for y in batch]).type(tr.long)
        data = {node: tr.stack([batch[i]["data"][node] for i in range(mb)])[:, None] for node in node_names}
        return {"data": data, "labels": gt}

    def top_left(x: tr.Tensor) -> tr.Tensor:
        y = tr.zeros_like(x)
        y[0:14, 0:14] = x[0:14, 0:14]
        return y

    def top_right(x: tr.Tensor) -> tr.Tensor:
        y = tr.zeros_like(x)
        y[0:14, 14:] = x[0:14, 14:]
        return y

    def bottom_left(x: tr.Tensor) -> tr.Tensor:
        y = tr.zeros_like(x)
        y[14:, 0:14] = x[14:, 0:14]
        return y

    def bottom_right(x: tr.Tensor) -> tr.Tensor:
        y = tr.zeros_like(x)
        y[14:, 14:] = x[14:, 14:]
        return y

    def __getitem__(self, index: int):
        image, label = super().__getitem__(index)
        image = tr.Tensor(np.array(image, dtype=np.float32)) / 255
        # names of the keys must match the graph's node names
        data = {
            "rgb_top_left": MNIST.top_left(image.clone()),
            "rgb_top_right": MNIST.top_right(image.clone()),
            "rgb_bottom_left": MNIST.bottom_left(image.clone()),
            "rgb_bottom_right": MNIST.bottom_right(image.clone()),
        }
        return {"data": data, "labels": label}

def build_reader(dataset_path: Path) -> Tuple[MNIST, Callable]:
    return MNIST(dataset_path, download=True), MNIST.collate_fn

def train_val_split(reader: Dataset, ratio: float, debug) -> Tuple[Dataset, Dataset]:
    assert 0 < ratio < 1, ratio
    n_val = len(reader) - int(ratio * len(reader))
    # this is used for debugging.
    if debug:
        train_reader, val_reader = Subset(reader, np.arange(n_val, n_val * 2)), Subset(reader, np.arange(n_val))
    else:
        train_reader, val_reader = Subset(reader, np.arange(n_val, len(reader))), Subset(reader, np.arange(n_val))
    return train_reader, val_reader
