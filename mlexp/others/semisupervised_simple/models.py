from typing import Dict, List
import torch as tr
from torch import nn
from torch.nn import functional as F

def get_model(cfg: Dict) -> nn.Module:
    if cfg.type == "model_fc":
        return ModelFC(**cfg.parameters, input_shape=[28, 28], num_classes=10)
    assert False, f"TODO: {cfg.type}"

class ModelFC(nn.Module):
    # (28, 28, 1) => (10, 1)
    def __init__(self, input_shape: List[int], hidden_shapes: List[int], num_classes: int):
        super().__init__()

        hidden_shapes = [input_shape[0] * input_shape[1], *hidden_shapes]
        self.fcs = nn.ModuleList([nn.Linear(hidden_shapes[i], hidden_shapes[i + 1])
                                 for i in range(len(hidden_shapes) - 1)])
        self.fc_out = nn.Linear(hidden_shapes[-1], num_classes)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = x.view(x.shape[0], -1)
        for fc in self.fcs:
            y = F.relu(fc(y))
        y_out = self.fc_out(y)
        return y_out

# class ModelConv(nn.Module):
#     def __init__(self, input_shape, num_classes):
#         super().__init__()

#         if len(input_shape) == 2:
#             input_shape = (*input_shape, 1)

#         self.input_shape = input_shape
#         self.fc1input_shape = (input_shape[0] - 4) * (input_shape[1] - 4) * 10

#         self.conv1 = nn.Conv2d(in_channels=input_shape[2], out_channels=50, kernel_size=3, stride=1)
#         self.conv2 = nn.Conv2d(in_channels=50, out_channels=10, kernel_size=3, stride=1)
#         self.fc1 = nn.Linear(self.fc1input_shape, 100)
#         self.fc2 = nn.Linear(100, num_classes)

#     def forward(self, x):
#         x = x.view(-1, self.input_shape[2], self.input_shape[0], self.input_shape[1])
#         y1 = F.relu(self.conv1(x))
#         y2 = F.relu(self.conv2(y1))
#         y2 = y2.view(-1, self.fc1input_shape)
#         y3 = F.relu(self.fc1(y2))
#         y4 = self.fc2(y3)
#         return y4
