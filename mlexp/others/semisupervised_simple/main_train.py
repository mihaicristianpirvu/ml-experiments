from pathlib import Path
from typing import Tuple, Dict, List
from datetime import datetime
from argparse import ArgumentParser

from omegaconf import OmegaConf
from loggez import loggez_logger as logger
from tqdm import trange
import lovely_tensors
import torch as tr
import numpy as np
import pandas as pd

from lightning_module_enhanced import LME
from lightning_module_enhanced.callbacks import PlotMetrics, CopyBestCheckpoint, PlotCallback
from torch.utils.data import DataLoader, Dataset, ConcatDataset
from torch.nn import functional as F
from torch import optim
from torchvision.datasets import MNIST
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from lightning_module_enhanced.metrics import MultiClassConfusionMatrix
from torchmetrics.functional.classification import multiclass_f1_score

from mlexp.utils import image_write, image_add_title, image_resize, to_image

from models import get_model

lovely_tensors.monkey_patch()

def plot_fn(x: tr.Tensor, y: tr.Tensor, gt: tr.Tensor, out_dir: Path, model: LME):
    MB = len(y)
    x = x.cpu().numpy()
    y = y.softmax(dim=-1).detach().cpu().numpy()
    gt = gt.cpu().numpy()
    for i in range(MB):
        ix = np.argmax(gt[i], axis=-1)
        prediction = y[i][ix]
        img = image_resize(to_image(x[i]), height=300, width=300)
        img = image_add_title(img, f"Label: {ix}. Result: {prediction:.3f}", size_px=30, top_padding=45)
        image_write(img, f"{out_dir}/{i}.png")

class TensorDataset(Dataset):
    def __init__(self, x: tr.Tensor, gt: tr.Tensor):
        self.x = x
        self.gt = gt

    def __len__(self) -> int:
        return len(self.x)

    def __getitem__(self, index):
        return self.x[index], self.gt[index]

    @staticmethod
    def collate_fn(batch: List) -> Dict:
        x = tr.stack([y[0] for y in batch])
        gt = tr.Tensor([y[1] for y in batch])
        gt_onehot = F.one_hot(gt.type(tr.long)).type(tr.float)
        return {"data": x, "labels": gt_onehot}

def get_test_data(dataset_path: Path):
    """same as get_data for train/val/semisup, but caches and returns test data"""
    test_reader = MNIST(dataset_path, train=False, download=True)
    npz_path = dataset_path / f"mnist_test.npz"
    if not npz_path.exists():
        x_test = np.zeros((len(test_reader), 28, 28), dtype=np.float32)
        gt_test = np.zeros((len(test_reader),), dtype=int)
        for i, item in enumerate(test_reader):
            x_test[i] = np.array(item[0], dtype=float) / 255
            gt_test[i] = np.array(item[1])
        np.savez(npz_path, {"test": (x_test, gt_test)})
    data = np.load(npz_path, allow_pickle=True)["arr_0"].item()
    x_test, gt_test = tr.from_numpy(data["test"][0]), tr.from_numpy(data["test"][1])
    return x_test, gt_test

def get_data(dataset_path: Path, split: Tuple[int, int, int]):
    """basic split in train/val/semisup sets. Semisup gets only inputs"""
    assert np.fabs(sum(split) - 1) < 1e-5, split
    logger.info(f"Getting mnist dataset split (trian/val/semisup) from '{dataset_path}'. Split: {split}")
    npz_path = dataset_path / f"mnist_{'_'.join([str(x) for x in split])}.npz"
    if not npz_path.exists():
        base_reader = MNIST(dataset_path, train=True, download=True)
        rand_ixs = np.random.permutation(len(base_reader))
        lens = (np.array(split) * len(base_reader)).astype(int)
        cums = [0, *np.cumsum(lens)]
        ixs = [rand_ixs[cums[i]: cums[i+1]] for i in range(len(cums)-1)]
        x_train, gt_train = np.zeros((lens[0], 28, 28), dtype=np.float32), np.zeros((lens[0], ), dtype=int)
        x_val, gt_val = np.zeros((lens[1], 28, 28), dtype=np.float32), np.zeros((lens[1], ), dtype=int)
        x_semisup = np.zeros((lens[2], 28, 28), dtype=np.float32)

        for i, ix in enumerate(ixs[0]):
            item = base_reader[ix]
            x_train[i] = np.array(item[0], dtype=float) / 255
            gt_train[i] = item[1]

        for i, ix in enumerate(ixs[1]):
            item = base_reader[ix]
            x_val[i] = np.array(item[0], dtype=float) / 255
            gt_val[i] = item[1]

        for i, ix in enumerate(ixs[2]):
            item = base_reader[ix]
            x_semisup[i] = np.array(item[0], dtype=float) / 255

        np.savez(npz_path, {"train": (x_train, gt_train), "val": (x_val, gt_val), "semisup": x_semisup})

    data = np.load(npz_path, allow_pickle=True)["arr_0"].item()
    x_train, gt_train = tr.from_numpy(data["train"][0]), tr.from_numpy(data["train"][1])
    x_val, gt_val = tr.from_numpy(data["val"][0]), tr.from_numpy(data["val"][1])
    x_semisup = tr.from_numpy(data["semisup"])
    return (x_train, gt_train), (x_val, gt_val), x_semisup

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path, required=True, help="Path to the train & graph config path")
    parser.add_argument("--logs_dir", required=False, type=Path,
                        default=Path(__file__).parent / "logs/" / datetime.strftime(datetime.now(), "%Y%m%d%H%M%S"),
                        help="Where the training logs/ckpts will be stored")
    parser.add_argument("--dataset_path", type=Path, required=False, default="data/")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    train_set, val_set, semisup_x = get_data(args.dataset_path, cfg.train.data_split)
    collate_fn = TensorDataset.collate_fn
    val_reader = TensorDataset(x=val_set[0], gt=val_set[1])
    train_reader = TensorDataset(x=train_set[0], gt=train_set[1])

    model = LME(get_model(cfg.model))
    print(model.summary)

    model.criterion_fn = lambda y, gt: F.cross_entropy(y, gt.argmax(-1)).mean()
    model.metrics = {
        "F1 Score": (lambda y, gt: multiclass_f1_score(y.argmax(-1), gt.argmax(-1), num_classes=10), "max"),
        "CF matrix": MultiClassConfusionMatrix(num_classes=10)
    }
    model.callbacks = [PlotMetrics(), PlotCallback(plot_fn), CopyBestCheckpoint()]
    logger.info(f"Experiment dir: '{args.logs_dir}'")

    # Step 1) Train supervised model
    supervised_ckpt = args.logs_dir / "supervised/checkpoints/model_best.ckpt"
    if not supervised_ckpt.exists():
        model.optimizer = optim.AdamW(model.parameters(), lr=0.001)
        sup_logger = CSVLogger(save_dir=args.logs_dir / "supervised", name="", version="")
        train_loader = DataLoader(train_reader, **cfg.loader_params, collate_fn=collate_fn)
        val_loader = DataLoader(val_reader, **{**cfg.loader_params, "shuffle": False}, collate_fn=collate_fn)
        Trainer(logger=sup_logger, **cfg.train.trainer_params).fit(model, train_loader, val_loader)

    # Step 2) Generate pseudolabels!
    pseudolabels_file = args.logs_dir / "pseudolabels.npz"
    if not pseudolabels_file.exists():
        pseudolabels = np.zeros(len(semisup_x), dtype=int)
        model.load_state_dict(tr.load(supervised_ckpt)["state_dict"], strict=True)
        for i in trange(len(semisup_x) // cfg.loader_params.batch_size, desc="pseudolabels"):
            with tr.no_grad():
                l, r = i * cfg.loader_params.batch_size, min((i + 1) * cfg.loader_params.batch_size, len(semisup_x))
                y = model.forward(semisup_x[l:r])
                # we get softmax predictions. We take the cut using argmax to get the highest class for pseudolabels.
                pseudolabels[l:r] = y.argmax(-1).cpu().numpy()
        np.savez(pseudolabels_file, pseudolabels)

    # Step 3) Train semi-supervised model
    pseudolabels = tr.from_numpy(np.load(pseudolabels_file)["arr_0"])
    pseudolabels_reader = TensorDataset(x=semisup_x, gt=pseudolabels)
    semisup_reader = ConcatDataset([train_reader, pseudolabels_reader])
    semisupervised_ckpt = args.logs_dir / "semisupervised/checkpoints/model_best.ckpt"
    if not semisupervised_ckpt.exists():
        model.reset_parameters()
        model.optimizer = optim.AdamW(model.parameters(), lr=0.001)
        semisup_logger = CSVLogger(save_dir=args.logs_dir / "semisupervised", name="", version="")
        semisup_loader = DataLoader(semisup_reader, **cfg.loader_params, collate_fn=collate_fn)
        val_loader = DataLoader(val_reader, **{**cfg.loader_params, "shuffle": False}, collate_fn=collate_fn)
        Trainer(logger=semisup_logger, **cfg.train.trainer_params).fit(model, semisup_loader, val_loader)

    # Step 4) Evaluate the two models
    test_set = get_test_data(args.dataset_path)
    test_reader = TensorDataset(x=test_set[0], gt=test_set[1])
    test_loader = DataLoader(test_reader, **cfg.loader_params, collate_fn=collate_fn)

    # Evaluate supervised model
    metrics_supervised = Trainer(logger=False).test(model, test_loader, ckpt_path=supervised_ckpt)
    metrics_semisup = Trainer(logger=False).test(model, test_loader, ckpt_path=semisupervised_ckpt)
    df = pd.DataFrame(metrics_supervised + metrics_semisup, index=["supervised", "semisupervised"])
    df.to_csv(args.logs_dir / "test_metrics.csv")

if __name__ == "__main__":
    main()
