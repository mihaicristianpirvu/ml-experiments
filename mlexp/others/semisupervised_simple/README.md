# MNIST Semi-supervised learning

The simplest case of semi-supervivsed learning. We define 4 sets: train set, validation set, semi-supervised set and
test set. The first three are derived from the same MNIST dataset, split in buckets of 40/20/40 (defined in cfg).

We train our seed model, also called iteration 1, or supervised iteration, on the train set + validation set and
evaluate it on the test set.

Then, using the seed model and the semi-supervised set, we generate pseudo-labels. The semi-supervised set has the
same inputs as the train set, but no actual labels.

Finally, we train, from scratch (or not, but in this repo we do), the same original network on the train set plus
the semi-supervised set (gt input, pseudo labels) + validation set. We evaluate it on the same test set.

To run:
```
python main_train.py --config_path cfg.yaml [--dir experiment] [--dataset_path /path/to/dataset]
```

`--dataset_path` is optional and will be downloaded using torchvision if doesn't exist. Defaults to `data/`.

`--dir` is optional and will default to logs/timestmap if doesn't exist. Use it if you don't want to start from
scratch every time, but load a pre-trained checkpoint (supervised or semisupervised)
