#!/usr/bin/env python3
"""Simplest baseline: bag of words for Cora dataset."""
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime
from omegaconf import OmegaConf
import lovely_tensors
import torch_geometric
import torch as tr
import pandas as pd
from torch import nn
from torch import optim
from torch.nn import functional as F
from torch.utils.data import Dataset, DataLoader
from lightning_module_enhanced import LME
from lightning_module_enhanced.metrics import MultiClassF1Score
from lightning_module_enhanced.callbacks import PlotMetrics
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger


lovely_tensors.monkey_patch()

class ModelBoW(nn.Module):
    def __init__(self, input_shape: int, hidden_shape: int, output_shape: int):
        super().__init__()
        self.fc1 = nn.Linear(input_shape, hidden_shape)
        self.fc2 = nn.Linear(hidden_shape, output_shape)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y1 = F.relu(self.fc1(x))
        y2 = self.fc2(y1)
        return y2

class CoraBoW(Dataset):
    def __init__(self, which: str):
        assert which in ("train", "val", "test"), which
        self.raw_data = torch_geometric.datasets.Planetoid(root=Path(__file__).parent / "data", name="Cora")[0]
        self.which = which
        self.x = self.raw_data.x[getattr(self.raw_data, f"{which}_mask")]
        self.gt = self.raw_data.y[getattr(self.raw_data, f"{which}_mask")]
        self.num_classes = self.raw_data.y.max().item() + 1

    def __len__(self):
        return len(self.x)

    def __getitem__(self, ix: int):
        return {"data": self.x[ix], "labels": self.gt[ix]}

def get_args():
    parser = ArgumentParser()
    parser.add_argument("config_path", type=Path)
    parser.add_argument("--weights_path", type=Path)
    args = parser.parse_args()
    assert args.weights_path is None or args.weights_path.exists(), args.weights_path
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    assert cfg.model.type == "bag_of_words"

    train_loader = DataLoader(CoraBoW("train"), **cfg.data.loader_params)
    val_loader = DataLoader(CoraBoW("val"), **cfg.data.loader_params)
    test_set = DataLoader(CoraBoW("test"), **cfg.data.loader_params)

    model = LME(ModelBoW(val_loader.dataset.x.shape[1], cfg.model.parameters.hidden_shape,
                         val_loader.dataset.num_classes))
    model.optimizer = optim.Adam(model.parameters(), lr=0.001)
    model.criterion_fn = F.cross_entropy
    model.metrics = {"f1_score": MultiClassF1Score(num_classes=val_loader.dataset.num_classes)}
    model.callbacks = [PlotMetrics()]

    ckpt_path = args.weights_path
    # train only if --weights_path was not provided. Otherwise, just test.
    if args.weights_path is None:
        pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                              name=datetime.strftime(datetime.now(), "%Y%m%d"),
                              version=datetime.strftime(datetime.now(), "%H%M%S"))
        trainer = Trainer(**cfg.trainer.trainer_params, logger=pl_logger)
        trainer.fit(model, train_loader, val_loader)
        ckpt_path = trainer.checkpoint_callback.best_model_path
    res = Trainer().test(model, test_set, ckpt_path=ckpt_path)
    pd.DataFrame(res).add_prefix("test_").to_csv(Path(ckpt_path).parents[1] / "test_metrics.csv")

if __name__ == "__main__":
    main()
