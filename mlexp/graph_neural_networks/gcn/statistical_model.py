#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
import lovely_tensors
import torch_geometric
import torch as tr
import pandas as pd
from loggez import loggez_logger as logger
from torch.utils.data import Dataset
from tqdm import tqdm
from torchmetrics.functional import f1_score

lovely_tensors.monkey_patch()

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--experiment_dir", type=Path, default=Path("experiments/statistical"))
    args = parser.parse_args()
    return args

def get_results(data: Dataset, which: str) -> tr.Tensor:
    """Gets all the neighbours from the train set and uses majority to pick the class. which: val or test"""
    assert which in ("val", "test")
    mask = data.val_mask if which == "val" else data.test_mask
    result = tr.zeros(mask.sum()).type(tr.LongTensor)
    n_nodes = []
    for i, val_ix in enumerate(tqdm(tr.where(mask)[0])):
        train_edges = tr.where(data.edge_index[1] == val_ix)[0]
        current_nodes = data.edge_index[:, train_edges].t()[:, 0].tolist()
        if len(current_nodes) == 0:
            current_nodes.append(1)
        n_nodes.append(len(current_nodes))
        majority = data.y[current_nodes].mode().values.item()
        result[i] = majority
    logger.info(f"Avg neighbours: {sum(n_nodes) / len(n_nodes):.2f}")
    return result

def main():
    args = get_args()
    data = torch_geometric.datasets.Planetoid(root=Path(__file__).parent / "data", name="Cora")[0]
    logger.info(f"CORA dataset loaded. Nodes: {len(data.x)}. Edges: {len(data.edge_index.t())}.")

    results = get_results(data, "val")
    val_f1_score = f1_score(results, data.y[data.val_mask], "multiclass", num_classes=7, average="macro").item()
    logger.info(f"Val f1 score: {val_f1_score:.2f}")
    results = get_results(data, "test")
    test_f1_score = f1_score(results, data.y[data.test_mask], "multiclass", num_classes=7, average="macro").item()
    logger.info(f"Test f1 score: {test_f1_score:.2f}")

    args.experiment_dir.mkdir(exist_ok=True, parents=True)
    out_file = args.experiment_dir / "results.csv"
    df = pd.DataFrame([[val_f1_score, test_f1_score]], columns=["val_f1_score", "test_f1_score"])
    logger.info(f"Stored results at '{out_file}'")
    df.to_csv(out_file)

if __name__ == "__main__":
    main()
