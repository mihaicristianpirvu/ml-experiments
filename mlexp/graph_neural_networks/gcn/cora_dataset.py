#!/usr/bin/env python3
import torch_geometric
import networkx as nx
from argparse import ArgumentParser
from pathlib import Path
import matplotlib.pyplot as plt
from loggez import loggez_logger as logger

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--dataset_path", type=Path, default=Path(__file__).parent / "data")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    data = torch_geometric.datasets.Planetoid(root=args.dataset_path, name="Cora")[0]
    logger.info(f"CORA dataset loaded. Nodes: {len(data.x)}. Edges: {len(data.edge_index.t())}.")

    G = nx.Graph()
    G.add_nodes_from(range(data.num_nodes))
    G.add_edges_from(data.edge_index.t().tolist())

    plt.figure(figsize=(15, 5))
    nx.draw(G, with_labels=False, arrows=True, node_color="r", node_size=10)
    plt.tight_layout()
    plt.savefig("data/full_graph.png")

    train_subgraph = G.edge_subgraph([(u, v) for u, v in G.edges() if data.train_mask[u] or data.train_mask[v]])
    val_subgraph = G.edge_subgraph([(u, v) for u, v in G.edges() if data.val_mask[u] or data.val_mask[v]])
    test_subgraph = G.edge_subgraph([(u, v) for u, v in G.edges() if data.test_mask[u] or data.test_mask[v]])

    plt.figure(figsize=(15, 5))
    plt.subplot(1, 3, 1)
    nx.draw(train_subgraph, with_labels=False, arrows=True, node_color="r", node_size=10)
    plt.title("Train Set")

    plt.subplot(1, 3, 2)
    nx.draw(val_subgraph, with_labels=False, arrows=True, node_color="g", node_size=10)
    plt.title("Validation Set")

    plt.subplot(1, 3, 3)
    nx.draw(test_subgraph, with_labels=False, arrows=True, node_color="b", node_size=10)
    plt.title("Test Set")

    plt.tight_layout()
    plt.savefig("data/split_graph.png")

if __name__ == "__main__":
    main()
