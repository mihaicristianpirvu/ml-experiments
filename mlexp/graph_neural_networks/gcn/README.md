# Cora GNN

<img src="data/split_graph.png" width=75%>

This repository will hold various implementations of classification on the Cora dataset using GNNs. We'll try using
both torchgeomtric as well as nwgraph. We'll also have a non-GNN baseline model, which is a simple neural network.

## Overall results

| Model   |      Val F1 Score      |  Test F1 Score |
|----------|:-------------:|------:|
| Bag of Words |  0.595 | 0.573 |
| Statistical majority | 0.862 | 0.851 |
| GNN-torchgeometric | 0.742 | 0.788 |
| GNN-nwgraph | 0.751 | 0.759 |


## Basline model: Bag of Words

```
./bow.py cfgs/bow.yaml [--weights_path /path/to/model.ckpt]
```
If `--weights_path` is provided, it'll only evaluate (on the test set) otherwise it will train as well as evaluate.

It should reach 1.00 train f1-score, 0.60 val f1-score and 0.55-0.57 test f1-score. Model is clearly overfitting.

## Baeline model 2: Statistical model

This model simply looks at all the nodes from the val/test set and their neighbors from the train set. Then, based
on all these neighbours, computes a majority class. This majority class is assumed to be the class of the curret node.

To run simply call:`./statistical_model.py`. The statistical model is so much better than the BoW...

## GNN: GCN using torchgeometric

This model uses the entire graph as one batch (1 item = 1 graph = "1 image" in standard CV/conv context). It stacks
`torch_geomtric.GCNConv` layers. To run, use the same semantics as the `bow.py` script:

```
./gcn_torchgeometric.py cfgs/gcn_torchgeometric.yaml [--weights_path /path/to/model.ckpt]
```

## GNN: GCN using nwgraph

We need a very specific git tag for this to work (it's still in big refactoring mode)

```
./gcn_nwgraph.py cfgs/gcn_nwgraph.yaml [--weights_path /path/to/model.ckpt]
```
