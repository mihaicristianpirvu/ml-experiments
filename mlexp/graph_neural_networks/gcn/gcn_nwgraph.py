#!/usr/bin/env python3
"""
Based on: https://lightning.ai/docs/pytorch/stable/notebooks/course_UvA-DL/06-graph-neural-networks.html.
Uses nwgraph (WIP branch) for graph definition & message propagation inspired by torch_geometric.
"""
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime
from typing import Dict, List
from omegaconf import OmegaConf
import torch_geometric
import torch as tr
import pandas as pd
from torch import nn
from torch import optim
from torch.nn import functional as F
from torch.utils.data import Dataset, DataLoader
from lightning_module_enhanced import LME
from lightning_module_enhanced.utils import to_device
from lightning_module_enhanced.metrics import MultiClassF1Score
from lightning_module_enhanced.callbacks import PlotMetrics
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger

from nwgraph import Graph
from nwgraph.layers import GCN


class GNNModel(nn.Module):
    def __init__(self, edge_index: tr.Tensor, input_shape: int, hidden_shape: List[int], output_shape: int):
        super().__init__()
        shapes = [input_shape, *hidden_shape, output_shape]
        # graph.to_graphviz().render(filename="asd.png", cleanup=True, format="png")
        # graph.to_networkx().draw(with_labels=False, arrows=True, node_color="r", node_size=10)
        self.gcns = nn.ModuleList()
        graph = Graph(edge_index.tolist())
        for in_c, out_c in zip(shapes[0:-1], shapes[1:]):
            self.gcns.append(GCN(graph, in_c, out_c))

    def forward(self, x: tr.Tensor):
        y = x
        for gcn in self.gcns[0:-1]:
            y = F.relu(gcn(y).node_states)
        y_out = self.gcns[-1](y).node_states
        return y_out

class GNNModule(LME):
    def model_algorithm(self, train_batch: dict):
        x, _, gt, gt_mask = train_batch
        # cora has just one batch item
        x_gnn = to_device(x[0], self.device)
        y_gnn = self.forward(x_gnn)
        # mask the gt and the gnn's prediction.
        gt = to_device(gt[gt_mask], self.device)
        y = y_gnn[gt_mask[0]]
        return self.lme_metrics(y, gt)

class CoraGNN(Dataset):
    def __init__(self, which: str):
        assert which in ("train", "val", "test"), which
        self.raw_data = torch_geometric.datasets.Planetoid(root=Path(__file__).parent / "data", name="Cora")[0]
        self.num_classes = self.raw_data.y.max().item() + 1
        self.mask = getattr(self.raw_data, f"{which}_mask")

    def __len__(self):
        return 1

    def __getitem__(self, ix: int):
        assert ix == 0, ix
        # return 4 items: always x and edge_index. the gt of all items and the train/val/test mask for loss/metrics
        return self.raw_data.x, self.raw_data.edge_index, self.raw_data.y, self.mask

def get_args():
    parser = ArgumentParser()
    parser.add_argument("config_path", type=Path)
    parser.add_argument("--weights_path", type=Path)
    args = parser.parse_args()
    assert args.weights_path is None or args.weights_path.exists(), args.weights_path
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    assert cfg.model.type == "gcn_nwgraph"
    # operate on the entire graph at once (1 batch = 1 graph, that is equivalent of 1 image in cnns)
    assert cfg.data.loader_params.batch_size == 1, "only batch size of 1 is allowed for this dataset/implementation"

    train_loader = DataLoader(CoraGNN("train"), **cfg.data.loader_params)
    val_loader = DataLoader(CoraGNN("val"), **cfg.data.loader_params)
    test_set = DataLoader(CoraGNN("test"), **cfg.data.loader_params)

    model = GNNModule(GNNModel(val_loader.dataset.raw_data.edge_index.t(),
                               val_loader.dataset.raw_data.x.shape[1],
                               hidden_shape=cfg.model.parameters.hidden_shape,
                               output_shape=val_loader.dataset.num_classes))
    model.optimizer = optim.Adam(model.parameters(), lr=0.001)
    model.criterion_fn = F.cross_entropy
    model.metrics = {"f1_score": MultiClassF1Score(num_classes=val_loader.dataset.num_classes)}
    model.callbacks = [PlotMetrics()]
    print(model.summary)

    ckpt_path = args.weights_path
    # train only if --weights_path was not provided. Otherwise, just test.
    if args.weights_path is None:
        pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                              name=datetime.strftime(datetime.now(), "%Y%m%d"),
                              version=datetime.strftime(datetime.now(), "%H%M%S"))
        trainer = Trainer(**cfg.trainer.trainer_params, logger=pl_logger, enable_model_summary=False)
        trainer.fit(model, train_loader, val_loader)
        ckpt_path = trainer.checkpoint_callback.best_model_path
    res = Trainer().test(model, test_set, ckpt_path=ckpt_path)
    pd.DataFrame(res).add_prefix("test_").to_csv(Path(ckpt_path).parents[1] / "test_metrics.csv")

if __name__ == "__main__":
    main()
