# Models submodule

Generic submodule that defines 1 file pytorch models that are also tested (inputs/outputs) for various params.

These are generally linked inside more specific use cases, such as [vision models](../vision-models/models/) or
[language models](../language-models/models/) and then are updated for those situations.

Supported so far:
- [SafeUAV](./safeuav.py)
- [Transformer](./transformer.py)

