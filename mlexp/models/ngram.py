"""ngram model"""
from __future__ import annotations
import torch as tr
from torch import nn
from torch.nn import functional as F

class NGram(nn.Module):
    def __init__(self, embedding: nn.Embedding, ngram: int, max_seq_size: int, hidden_sizes: list[int]):
        super().__init__()
        assert ngram < max_seq_size, (ngram, max_seq_size)
        self.embedding = embedding
        self.ngram = ngram
        self.max_seq_size = max_seq_size
        self.fcs = nn.ModuleList()

        hidden_sizes = [ngram * embedding.emb_size, *hidden_sizes]
        for l, r in zip(hidden_sizes[0:-1], hidden_sizes[1:]):
            self.fcs.append(nn.Linear(l, r))

        self.fc_out = nn.Linear(hidden_sizes[-1], embedding.num_embeddings)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        # x_emb::(MB, MB-seq_size, ngram, emb_size) => (MB, MB-seq_size, ngram * emb_size)
        x_emb = self.embedding(x).reshape(x.shape[0], -1)
        y = x_emb
        for fc in self.fcs:
            y = F.relu(fc(y))
        y_out = self.fc_out(y)
        return y_out
