"""Transformer model"""
from __future__ import annotations
import math
import os
from typing import Callable
import torch as tr
from torch import nn
from torch.nn import functional as F
from loggez import loggez_logger as logger

from lovely_tensors import monkey_patch
monkey_patch()

def vanilla_attention(_q: tr.Tensor, _k: tr.Tensor, _v: tr.Tensor, dropout_p: float=0,
                      is_causal: bool=False) -> tr.Tensor:
    """q, k, v :: (B, t, n_heads, E // n_heads). Returns (B, t, E)"""
    res = []
    for i in range(_q.shape[1]):
        q, k, v = _q[:, i], _k[:, i], _v[:, i]
        w = q @ k.permute(0, 2, 1) # (B, t, t2)
        w_scaled = w / math.sqrt(q.shape[-1]) # q.shape[-1] == E

        # mask (if causal) & normalize the weights (sum to 1) via softmax
        if is_causal:
            tril = tr.tril(tr.ones_like(w_scaled))
            w_scaled[tril == 0] = -math.inf
        w_scaled = F.softmax(w_scaled, dim=-1)
        w_scaled = F.dropout(w_scaled, dropout_p) # (B, t, t2)

        y_out = w_scaled @ v # (B, t, E)
        res.append(y_out)
    return tr.stack(res, dim=1)

class MultiHeadAttention(nn.Module):
    def __init__(self, emb_size: int, n_heads: int, dropout: int, causal: bool, attn_bias: bool, mha_fn: Callable):
        super().__init__()
        assert emb_size % n_heads == 0, (emb_size, n_heads)
        # key, query, value projections for all heads, but in a batch
        self.q, self.k, self.v = (nn.Linear(emb_size, emb_size, bias=attn_bias) for _ in range(3))
        self.c_proj = nn.Linear(emb_size, emb_size, bias=True)
        self.resid_dropout = nn.Dropout(dropout)
        self.n_heads = n_heads
        self.dropout = dropout
        self.causal = causal # True for LLMs, False for VIT/MAEs
        self.mha_fn = mha_fn

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None,
                kv_cache: tr.Tensor | None = None) -> tuple[tr.Tensor, tr.Tensor]:
        """x :: (B, t, E), k :: (B, t2, E) | None"""
        kv = x if kv is None else kv # self attention vs cross attention is decided here.
        assert len(x.shape) == 3 and len(kv.shape) == 3, (x, kv)
        B, t, t2, E = x.shape[0], x.shape[1], kv.shape[1], x.shape[2]
        q, k, v = self.q(x), self.k(kv), self.v(kv) # (B, t, E), (B, t2, E), (B, t2, E)
        if kv_cache is not None:
            k = tr.cat([kv_cache[:, 0], k], dim=1)
            v = tr.cat([kv_cache[:, 1], v], dim=1)
            t2 = k.shape[1]
        # Split them in equal parts for multi head attention
        q_mha = q.view(B, t, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t, hs)
        k_mha = k.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)
        v_mha = v.view(B, t2, self.n_heads, E // self.n_heads).transpose(1, 2) # (B, nh, t2, hs)

        dropout_p = self.dropout if self.training else 0
        is_causal = self.causal if kv_cache is None else False
        y: tr.Tensor = self.mha_fn(q_mha, k_mha, v_mha, dropout_p=dropout_p, is_causal=is_causal) # F.sdpa()
        y = y.transpose(1, 2).contiguous().view(B, t, E) # (B, t, E)

        # output projection
        y = self.resid_dropout(self.c_proj(y)) # (B, t, E)
        return y, tr.stack([k, v], dim=1)

class Block(nn.Module):
    """One block of MultiHeaded attention (communication) and MLP (processing)"""
    def __init__(self, emb_size: int, n_heads: int, ff_multiplier: float, dropout: int,
                 causal: bool, attn_bias: bool, mha_fn: Callable):
        super().__init__()
        self.mha = MultiHeadAttention(emb_size=emb_size, n_heads=n_heads, dropout=dropout, causal=causal,
                                      attn_bias=attn_bias, mha_fn=mha_fn)
        self.ff = nn.Sequential(nn.Linear(emb_size, round(emb_size * ff_multiplier)), nn.GELU(approximate="tanh"),
                                nn.Linear(round(emb_size * ff_multiplier), emb_size), nn.Dropout(dropout))
        self.ln1 = nn.LayerNorm(emb_size)
        self.ln2 = nn.LayerNorm(emb_size)

    def forward(self, x: tr.Tensor, *args, **kwargs) -> tuple[tr.Tensor, tr.Tensor]:
        x_a, kv_a = self.mha(self.ln1(x), *args, **kwargs) #kv_a is returned for kv_cache in autoregressive mode
        y1 = x + x_a
        y2 = y1 + self.ff(self.ln2(y1))
        return y2, kv_a

class Transformer(nn.Module):
    """
    Transformer class. Stacks N blocks on top of each other. Also sets up parameters and cross attention if needed
    Parameters:
    - emb_size: The shape of the embedding, maintained in all the blocks (width of the network).
    - n_blocks: The number of MultiHeadAttention-FeedForward (MHA-FF) blocks (depth of the network).
    - n_heads: The number of heads in each MHA layer.
    - dropout: Dropout of each MHA-FF layer. Applied once in MHA, once after MHA and then again in FF.
    - causal: Whether the Attention tokens attend to previous tokens or not (LM vs VIT/BERT).
    - attn_bias: Whether the Attention nn.Linear have biases. GPT-2 original has them, newer models don't.
    - return_all_states: Whether the forward method returns all the MHA-FF outputs or just the last one (used in DPT).
    - mha_fn: The function that implements the (multi headed) attention mechanism. Default to use torch's F.sdpa().
    - return_kv: If set to true, the forward pass returns the kv values. Needed for kv caching in autoregression.
    """
    def __init__(self, emb_size: int, n_blocks: int, n_heads: int, ff_multiplier: float, dropout: int,
                 causal: bool, return_all_states: bool, attn_bias: bool=False, ln_blocks: bool=False,
                 mha_fn: list[type] | list[str] | None = None, return_kv: bool = False):
        super().__init__()
        self.emb_size = emb_size
        if mha_fn is None:
            mha_fn = vanilla_attention if os.getenv("VANILLA_ATTENTION", "0") == "1" else F.scaled_dot_product_attention
            logger.debug(f"mha_fn is not set. Defaulting to {mha_fn}")
        mha_fn = globals()[mha_fn] if isinstance(mha_fn, str) else mha_fn
        self.blocks = nn.ModuleList(Block(emb_size=emb_size, n_heads=n_heads, ff_multiplier=ff_multiplier,
                                          dropout=dropout, causal=causal, attn_bias=attn_bias, mha_fn=mha_fn)
                                    for i in range(n_blocks))
        self.ln_blocks = nn.LayerNorm(emb_size) if ln_blocks else nn.Identity()
        self.causal = causal
        self.return_all_states = return_all_states
        self.return_kv = return_kv
        self.reset_parameters()

    def forward(self, x: tr.Tensor, kv: tr.Tensor | None = None, kv_cache: list[tr.Tensor] | None = None) \
            -> tuple[tr.Tensor | list[tr.Tensor], list[tr.Tensor]]:
        """
        Returns a tuple of 2 items:
        - The last prediction or all of them as a list if self.return_all_states is True. Default to just a tensor.
        - kv_res A list of internal kv states for each block in the Transformer. Used for kv cache in autogressive mode.
    """
        kv_cache = kv_cache or ([None] * len(self.blocks))
        res, kv_res = [x], []
        for i, block in enumerate(self.blocks):
            # by default only 1st layer gets kv for x-attn.
            y_block, kv_block = block(res[-1], kv if i == 0 else None, kv_cache=kv_cache[i])
            res.append(y_block)
            kv_res.append(kv_block)
        res[-1] = self.ln_blocks(res[-1])
        _res = res[1:] if self.return_all_states else res[-1]
        return (_res, kv_res) if self.return_kv else _res

    def _init_weights(self, module: nn.Module):
        if isinstance(module, nn.Linear):
            nn.init.normal_(module.weight, mean=0.0, std=0.02)
            if module.bias is not None:
                nn.init.zeros_(module.bias)
        elif isinstance(module, nn.LayerNorm):
            module.weight.data.fill_(1.0)
            module.bias.data.zero_()
        elif isinstance(module, nn.Embedding):
            nn.init.trunc_normal_(module.weight.data, mean=0, std=0.02)

    def reset_parameters(self):
        self.apply(self._init_weights)
