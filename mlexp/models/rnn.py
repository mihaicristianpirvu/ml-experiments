"""RNN model. Support for LSTM, RNN and RNNVanilla (multi stacked) cells."""
from __future__ import annotations
from typing import List, Union
from torch.nn.utils.rnn import pack_sequence, unpack_sequence, PackedSequence
from torch import nn
from torch.nn import functional as F
import torch as tr

# list of tensors of different lengths or a torch packed sequence object
RNN_IO = Union[List[tr.Tensor], PackedSequence]

def pad(x: list[tr.Tensor]) -> tuple[tr.Tensor, tr.Tensor]:
    """
    Pads with zero at the end and returns the length of the original sequences.
    [(t, n_feats)] (size b) -> (b, T, n_feats) with T = max(t)
    """
    device = x[0].device
    if len(x) == 1:
        return tr.stack([x[0]]), tr.LongTensor([len(x[0])]).to(device)
    lens = tr.LongTensor([len(y) for y in x]).to(device)
    max_len = lens.max()
    n_feats = x[0].shape[1:]
    bs = len(x)
    x_pad = tr.zeros(bs, max_len, *n_feats).to(device)
    diffs = max_len - lens
    for i in range(bs):
        x_pad[i] = F.pad(x[i], (0, 0, 0, int(diffs[i])))
    return x_pad, lens

def unpad(x: tr.Tensor, lens: tr.Tensor) -> list[tr.Tensor]:
    return [y[0: y_len] for y, y_len in zip(x, lens)]

class LayerNormPacked(nn.Module):
    """nn.LayerNorm wrapper because layer norm doesn't work with packed sequences ootb"""
    def __init__(self, n_feats: int):
        super().__init__()
        self.ln = nn.LayerNorm(n_feats)

    def forward(self, x: PackedSequence) -> PackedSequence:
        x_unpacked = unpack_sequence(x)
        x_ln_unpacked = [self.ln(y) for y in x_unpacked]
        x_ln_packed = pack_sequence(x_ln_unpacked, enforce_sorted=False)
        return x_ln_packed

class RNNVanilla(nn.Module):
    """RNN implementation from scratch. Supports padded and unpadded sequences."""
    def __init__(self, input_size: int, hidden_size: int):
        super().__init__()
        self.W_xh = nn.Linear(input_size, hidden_size)
        self.W_hh = nn.Linear(hidden_size, hidden_size)

    def forward(self, x: RNN_IO, h: tr.Tensor | None) -> tuple[RNN_IO, tr.Tensor]:
        device = next(self.parameters()).device
        if h is None:
            h = tr.zeros(self.W_hh.in_features).requires_grad_(False).to(device)
        x_is_packed = isinstance(x, PackedSequence)
        lens = None
        if x_is_packed:
            x = unpack_sequence(x)
        x_padded, lens = pad(x)
        B, T = x_padded.shape[0:2]
        hs = tr.zeros(B, T, self.W_hh.out_features).to(device)
        for t in range(T):
            y_xh = self.W_xh(x_padded[:, t])
            y_hh = self.W_hh(h)
            h = tr.tanh(y_xh + y_hh)
            hs[:, t] = h
        # Think this through when you come back here. Why is there a hidden state for each batch item?
        # It's because all batch items are ran in parallel resulting in independent hidden states. All of them produce
        # a batch-level output later on. For inference this is of BS=1, so we can autoregress on the single batch item
        if x_is_packed:
            h_unpadded = unpad(hs, lens)
            h_last = [_h[-1] for _h in h_unpadded]
            hs = pack_sequence(h_unpadded, enforce_sorted=False)
        else:
            h_last = [_h[-1] for _h in hs]
        return hs, tr.stack(h_last)

class RNNCell(nn.Module):
    """RNN Cell implementation. Can be either RNNVanilla, LSTM or RNN. Supports layer norm."""
    def __init__(self, cell_type: str, emb_size: int, use_layer_norm: bool, **kwargs):
        super().__init__()
        assert cell_type in ("rnn_vanilla", "lstm", "rnn"), cell_type
        self.cell_type = cell_type
        self.emb_size = emb_size
        self.use_layer_norm = use_layer_norm

        self.cell = {
            "rnn_vanilla": RNNVanilla,
            "lstm": nn.LSTM,
            "rnn": nn.RNN,
        }[cell_type](emb_size, emb_size, **kwargs)
        if use_layer_norm:
            self.ln = LayerNormPacked(emb_size)

    def forward(self, x: tr.Tensor, h: tr.Tensor | None = None) -> tuple[RNN_IO, tr.Tensor]:
        x, h_new = self.cell(x, h)
        if self.use_layer_norm:
            x = self.ln(x)
        return x, h_new

class RNN(nn.Module):
    def __init__(self, emb_size: int, cell_type: str, n_cells: int, use_layer_norm: bool = False, **kwargs):
        super().__init__()
        assert cell_type in ("rnn_vanilla", "lstm", "rnn"), cell_type
        assert isinstance(emb_size, int), emb_size
        self.n_cells = n_cells
        # dims = [prod(input_shape), *([emb_size] * n_cells)]
        self.cells = nn.ModuleList([RNNCell(cell_type, emb_size, use_layer_norm, **kwargs)
                                    for i in range(n_cells)])

    def forward(self, x: list[tr.Tensor], hs: list[tr.Tensor] | None = None) -> tuple[RNN_IO, list[tr.Tensor]]:
        # We 'pack' the sequence of variable [(t, n_feats)]. This is basically padding and storing the data in a
        # efficient memory structure for torch rnns resulting in [b, T, n_feats] where T = max(t) in the batch.
        # Returns: all the intermediate steps of the last cell and all the hidden states of all cells as a list.

        x_packed = pack_sequence(x, enforce_sorted=False)
        # Iterate through the rnn cells as well as the layer norm cells
        hs = [None] * self.n_cells if hs is None else hs
        y = x_packed
        h_out = []
        for cell, h in zip(self.cells, hs):
            y, h_new = cell(y, h)
            h_out.append(h_new)
        y_all = unpack_sequence(y)
        # return the last hidden state of the last cell. We can recuperate all the others from y_rnn_all
        return y_all, h_out

def test_rnn():
    """test rnn"""
    # B, t, E
    E = 2
    rnn = RNN(emb_size=E, cell_type="rnn_vanilla", n_cells=2)

    # Fixed length sequences
    x = tr.randn(10, 5, E)
    _, y = rnn(x)
    assert len(y) == 2 and y[-1].shape == (10, E)

    # Fixed length sequences and initial hidden state
    x = tr.randn(10, 5, E)
    hs = [tr.randn(10, E), tr.randn(10, E)] # we know there's 2 cells
    _, y = rnn(x, hs)
    assert y[-1].shape == (10, E)

    # Variable length sequences
    x = [tr.randn(5, E), tr.randn(3, E), tr.randn(4, E)]
    _, y = rnn(x)
    assert y[-1].shape == (3, E)

    # Variable length sequences and initial hidden state
    x = [tr.randn(5, E), tr.randn(3, E), tr.randn(4, E), tr.randn(10, E)]
    hs = [tr.randn(4, E), tr.randn(4, E)]
    _, y = rnn(x, hs)
    assert y[-1].shape == (4, E)
