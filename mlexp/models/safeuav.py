#!/usr/bin/env python3
import torch as tr
import torch.nn as nn
import torch.nn.functional as F

from lightning_module_enhanced import LME
from lovely_tensors import monkey_patch
monkey_patch()

def conv(d_in: int, d_out: int, kernel_size: int, padding: int, stride: int, dilation: int) -> nn.Sequential:
    return nn.Sequential(
        nn.Conv2d(in_channels=d_in, out_channels=d_out, kernel_size=kernel_size, padding=padding,
                  stride=stride, dilation=dilation),
        nn.BatchNorm2d(num_features=d_out),
        nn.ReLU(inplace=True)
    )

def conv_tr(d_in: int, d_out: int, kernel_size: int, padding: int, stride: int, dilation: int) -> nn.Sequential:
    return nn.Sequential(
        nn.ConvTranspose2d(in_channels=d_in, out_channels=d_out, kernel_size=kernel_size, padding=padding,
                           stride=stride, dilation=dilation),
        nn.BatchNorm2d(num_features=d_out),
        nn.ReLU(inplace=True)
    )

def cat(a, b):
    diff_up, diff_left = b.shape[-2] - a.shape[-2], b.shape[-1] - a.shape[-1]
    a = F.pad(a, (0, diff_left, 0, diff_up))
    c = tr.cat([a, b], dim=1)
    return c


class SafeUAVEncoder(nn.Module):
    def __init__(self, d_in: int, num_filters: int):
        self.d_in = d_in
        self.num_filters = num_filters
        super().__init__()

        self.conv1 = conv(d_in=d_in, d_out=num_filters, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv2 = conv(d_in=num_filters, d_out=num_filters, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv3 = conv(d_in=num_filters, d_out=num_filters, kernel_size=3, padding=1, stride=2, dilation=1)

        self.conv4 = conv(d_in=num_filters, d_out=num_filters * 2, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv5 = conv(d_in=num_filters * 2, d_out=num_filters * 2, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv6 = conv(d_in=num_filters * 2, d_out=num_filters * 2, kernel_size=3, padding=1, stride=2, dilation=1)

        self.conv7 = conv(d_in=num_filters * 2, d_out=num_filters * 4, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv8 = conv(d_in=num_filters * 4, d_out=num_filters * 4, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv9 = conv(d_in=num_filters * 4, d_out=num_filters * 4, kernel_size=3, padding=1, stride=2, dilation=1)

        self.dilate1 = conv(d_in=num_filters * 4, d_out=num_filters * 8, kernel_size=3, padding=1,
                            stride=1, dilation=1)
        self.dilate2 = conv(d_in=num_filters * 8, d_out=num_filters * 8, kernel_size=3, padding=2,
                            stride=1, dilation=2)
        self.dilate3 = conv(d_in=num_filters * 8, d_out=num_filters * 8, kernel_size=3, padding=4,
                            stride=1, dilation=4)
        self.dilate4 = conv(d_in=num_filters * 8, d_out=num_filters * 8, kernel_size=3, padding=8,
                            stride=1, dilation=8)
        self.dilate5 = conv(d_in=num_filters * 8, d_out=num_filters * 8, kernel_size=3, padding=16,
                            stride=1, dilation=16)
        self.dilate6 = conv(d_in=num_filters * 8, d_out=num_filters * 8, kernel_size=3, padding=32,
                            stride=1, dilation=32)

        self.conv_transpose10 = conv_tr(d_in=num_filters * 8, d_out=num_filters * 4, kernel_size=3,
                                        padding=1, stride=2, dilation=1)
        self.conv11 = conv(d_in=num_filters * 4 * 2, d_out=num_filters * 4, kernel_size=3,
                           padding=1, stride=1, dilation=1)
        self.conv_transpose12 = conv_tr(d_in=num_filters * 4, d_out=num_filters * 2, kernel_size=3,
                                        padding=1, stride=2, dilation=1)
        self.conv13 = conv(d_in=num_filters * 2 * 2, d_out=num_filters, kernel_size=3, padding=1, stride=1, dilation=1)
        self.conv_transpose14 = conv_tr(d_in=num_filters, d_out=num_filters, kernel_size=3,
                                        padding=1, stride=2, dilation=1)
        self.conv15 = conv(d_in=num_filters * 2, d_out=num_filters, kernel_size=3, padding=1, stride=1, dilation=1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        # x::d_inxHxW
        y_1 = self.conv1(x)
        y_2 = self.conv2(y_1)
        y_3 = self.conv3(y_2)
        y_4 = self.conv4(y_3)
        y_5 = self.conv5(y_4)
        y_6 = self.conv6(y_5)

        y_7 = self.conv7(y_6)
        y_8 = self.conv8(y_7)
        y_9 = self.conv9(y_8)

        y_dilate1 = self.dilate1(y_9)
        y_dilate2 = self.dilate2(y_dilate1)
        y_dilate3 = self.dilate3(y_dilate2)
        y_dilate4 = self.dilate4(y_dilate3)
        y_dilate5 = self.dilate5(y_dilate4)
        y_dilate6 = self.dilate6(y_dilate5)
        y_dilate_sum = y_dilate1 + y_dilate2 + y_dilate3 + y_dilate4 + y_dilate5 + y_dilate6

        y_10 = cat(self.conv_transpose10(y_dilate_sum), y_7)
        y_11 = self.conv11(y_10)
        y_12 = cat(self.conv_transpose12(y_11), y_4)
        y_13 = self.conv13(y_12)
        y_14 = cat(self.conv_transpose14(y_13), y_1)
        y_15 = self.conv15(y_14)
        return y_15

    def __str__(self):
        return f"Encoder Map2Map. d_in: {self.d_in}. NF: {self.num_filters}."

if __name__ == "__main__":
    model = SafeUAVEncoder(d_in=3, num_filters=16)
    print(LME(model).summary)
    x = tr.rand(1, 3, 32, 32)
    y = model(x)
    print(x)
    print(y)
