# Vanilla Variational Auto Encoder (VAE)

## Dataset visualisation

We are reusing the code from [vanilla-gan](../vanilla-gan/) to plot the dataset.

```
python dataset_plot.py --config_path cfg/mnist_conv.yaml
```

