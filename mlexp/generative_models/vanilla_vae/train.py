#!/usr/bin/env python3
from __future__ import annotations
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime
from omegaconf import OmegaConf
from functools import partial
import torch as tr
import numpy as np
import lovely_tensors
from torch.utils.data import DataLoader
from torch import nn, optim
from lightning_module_enhanced.callbacks import PlotMetrics
from lightning_module_enhanced import LME
from lightning_module_enhanced.utils import to_tensor, to_device
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import Callback

from mlexp.utils import image_write

from model import build_model
from reader import build_reader, train_val_split, to_collage

device = "cuda" if tr.cuda.is_available() else "cpu"
lovely_tensors.monkey_patch()
tr.set_float32_matmul_precision("medium")

class VAE(nn.Module):
    def __init__(self, encoder: nn.Module, decoder: nn.Module):
        super().__init__()
        self.encoder = encoder
        self.decoder = decoder

    @staticmethod
    def kl_loss(mean: tr.Tensor, std: tr.Tensor) -> float:
        # KL-Divergence between two Gaussians: N(0, I) and N(mean, std)
        KL = 0.5 * (std**2 + mean**2 - 1 - tr.log(std**2)).sum()
        return KL

    @staticmethod
    def l2_loss(y: tr.Tensor, gt: tr.Tensor) -> float:
        return (y.flatten() - gt.flatten()).pow(2).mean()

    def forward(self, x: tr.Tensor) -> (tr.Tensor, tr.Tensor, tr.Tensor):
        y_mean, y_std = self.encoder(x)
        z_samples = tr.randn_like(y_std) * y_std + y_mean
        y_decoder = self.decoder(z_samples)
        return y_decoder, y_mean, y_std

    @staticmethod
    def vae_algorithm(model: VAE, batch: dict, hp: dict):
        x = to_tensor(to_device(batch["data"], device=device))
        y = model(x)
        metrics = model.lme_metrics(y, x, include_loss=False)

        y_decoder, y_mean, y_std = y
        l2_loss = VAE.l2_loss(y_decoder, x)
        kl_loss = VAE.kl_loss(y_mean, y_std)
        metrics["loss"] = l2_loss + kl_loss * hp.kl_weight
        return y, metrics

class GenerateImagesAtEpochEnd(Callback):
    def __init__(self, grid: tuple[int, int], img_shape: list[int], z_dims: int):
        self.grid = grid
        self.img_shape = img_shape
        self.z_dims = z_dims

    @tr.no_grad()
    def on_train_epoch_end(self, trainer: Trainer, pl_module: VAE) -> None:
        Path(f"{trainer.logger.log_dir}/imgs").mkdir(exist_ok=True, parents=True)

        # generate and convert to [0:255]
        z_noise = tr.randn(np.prod(self.grid), self.z_dims).to(pl_module.device)
        gen_imgs: tr.Tensor = pl_module.base_model.decoder(z_noise).reshape(np.prod(self.grid), *self.img_shape)
        collage = to_collage(gen_imgs.to("cpu"), self.grid, width=100, height=100)
        image_write(collage, f"{trainer.logger.log_dir}/imgs/{trainer.current_epoch + 1}.png")

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path)
    parser.add_argument("--dataset_path", type=Path, default="data",
                        help="Dataset path. If doesn't exist, will be downloaded.")
    args = parser.parse_args()
    assert args.config_path.exists(), f"Config file '{args.config_path}' doesn't exist"
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    reader, collate_fn = build_reader(cfg.data, args.dataset_path)
    train_reader, val_reader = train_val_split(reader, ratio=cfg.data.train_val_split)
    train_loader = DataLoader(train_reader, collate_fn=collate_fn, **cfg.loader_params)
    val_loader = DataLoader(val_reader, collate_fn=collate_fn, **{**cfg.loader_params, "shuffle": False})

    vae = LME(VAE(*build_model(cfg.model)))
    vae.model_algorithm = partial(VAE.vae_algorithm, hp=cfg.model.train_hparams)
    vae.metrics = {"kl": (lambda y, gt: VAE.kl_loss(y[1], y[2]), "min"),
                   "l2": (lambda y, gt: VAE.l2_loss(y[0], gt), "min")}
    vae.criterion_fn = lambda y, gt: y.mean() # placeholder
    vae.optimizer = optim.Adam(vae.parameters(), lr=0.0002)
    vae.callbacks = [PlotMetrics(), GenerateImagesAtEpochEnd(grid=cfg.collage_callback.grid,
                                                             img_shape=cfg.data.image_shape, z_dims=cfg.model.z_dims)]
    print(vae.summary)

    pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))
    trainer = Trainer(logger=pl_logger, **cfg.trainer_params)
    trainer.fit(vae, train_loader, val_loader)

if __name__ == "__main__":
    main()
