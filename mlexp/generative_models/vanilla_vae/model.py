from omegaconf import DictConfig
from torch import nn
from torch.nn import functional as F
import torch as tr
import numpy as np

def build_model(cfg: DictConfig) -> (nn.Module, nn.Module):
    encoder = _build_encoder(cfg.encoder)
    decoder = _build_decoder(cfg.decoder)
    return encoder, decoder

def _build_encoder(cfg: DictConfig) -> nn.Module:
    if cfg.type == "EncoderFC":
        return EncoderFC(**cfg.parameters)
    if cfg.type == "EncoderConv":
        return EncoderConv(**cfg.parameters)
    raise NotImplementedError(f"Unknown encoder type: {cfg.type}")

def _build_decoder(cfg: DictConfig) -> nn.Module:
    if cfg.type == "DecoderFC":
        return DecoderFC(**cfg.parameters)
    if cfg.type == "DecoderConv":
        return DecoderConv(**cfg.parameters)
    raise NotImplementedError(f"Unknown encoder type: {cfg.type}")

class EncoderFC(nn.Module):
    def __init__(self, in_channels: int, z_dims: int):
        super().__init__()
        self.z_dims = z_dims
        self.in_channels = in_channels
        self.fc1 = nn.Linear(in_channels, 100)
        self.fc2 = nn.Linear(100, 100)
        self.mean_fc = nn.Linear(100, z_dims)
        self.mean_std = nn.Linear(100, z_dims)

    def forward(self, x: tr.Tensor):
        x = x.reshape(-1, self.in_channels)
        y1 = F.relu(self.fc1(x))
        y2 = F.relu(self.fc2(y1))
        y_mean = self.mean_fc(y2)
        y_std = self.mean_std(y2)
        return y_mean, y_std

class DecoderFC(nn.Module):
    def __init__(self, z_dims: int, out_shape: tuple[int, int]):
        super().__init__()
        self.fc1 = nn.Linear(z_dims, 300)
        self.fc2 = nn.Linear(300, np.prod(out_shape))
        self.out_shape = out_shape

    def forward(self, z_samples):
        y1 = F.relu(self.fc1(z_samples))
        y_decoder = self.fc2(y1)
        y_decoder = y_decoder.reshape((len(z_samples), *self.out_shape))
        return y_decoder

class EncoderConv(nn.Module):
    def __init__(self, in_shape: list[int], z_dims):
        super().__init__()
        self.z_dims = z_dims
        self.conv1 = nn.Conv2d(in_channels=in_shape[-1], out_channels=32, kernel_size=3, padding=1)
        self.conv2 = nn.Conv2d(in_channels=32, out_channels=64, kernel_size=3, padding=0)
        self.conv3 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=0)
        self.conv4 = nn.Conv2d(in_channels=64, out_channels=64, kernel_size=3, padding=0)
        self.conv5 = nn.Conv2d(in_channels=64, out_channels=32, kernel_size=3, padding=0)
        fc_shape = 32 * np.prod(np.array(in_shape[0:-1]) - 8) # -2 for each conv (4 of them)
        self.fc_out = nn.Linear(fc_shape, z_dims * 2)

    def forward(self, x):
        assert len(x.shape) == 4, x.shape
        mb = len(x)
        x = x.permute(0, 3, 1, 2)
        y1 = F.relu(self.conv1(x))
        y2 = F.relu(self.conv2(y1))
        y3 = F.relu(self.conv3(y2))
        y4 = F.relu(self.conv4(y3))
        y5 = F.relu(self.conv5(y4))
        y_fc_out = self.fc_out(y5.reshape(mb, -1))
        z_mean, z_var = tr.split(y_fc_out, self.z_dims, dim=1)
        return z_mean, z_var

class DecoderConv(nn.Module):
    def __init__(self, z_dims: int, out_shape: list[int, int, int]):
        super().__init__()
        self.z_dims = z_dims
        self.out_shape = out_shape
        out_channels = self.out_shape[-1]

        self.convt1 = nn.ConvTranspose2d(z_dims, out_channels=128, kernel_size=self.out_shape[0] // 4, stride=1,
                                         padding=0, bias=False)
        self.convt2 = nn.ConvTranspose2d(128, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False)
        self.convt3 = nn.ConvTranspose2d(64, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False)
        self.conv4 = nn.Conv2d(64, out_channels=64, kernel_size=3, stride=1, padding=1, bias=False)
        self.conv5 = nn.Conv2d(64, out_channels=out_channels, kernel_size=3, stride=1, padding=1, bias=False)

    def _get_pad(self, y_shape: tuple[float, float]) -> tuple[int, int, int, int]:
        w_pad = int(self.out_shape[0] - y_shape[-2])
        h_pad = int(self.out_shape[0] - y_shape[-1])
        return w_pad // 2, w_pad // 2 + (w_pad % 2 == 1), h_pad // 2, h_pad // 2 + (h_pad % 2 == 1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        mb = len(x)
        x = x.view(mb, self.z_dims, 1, 1)
        y1 = F.relu(self.convt1(x))
        y2 = F.relu(self.convt2(y1))
        y3 = F.relu(self.convt3(y2))
        y4 = F.relu(self.conv4(y3))
        y5 = self.conv5(y4)
        y_out = F.pad(y5, self._get_pad(y5.shape)).permute(0, 2, 3, 1)
        return y_out
