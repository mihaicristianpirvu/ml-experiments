#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from datetime import datetime
from omegaconf import OmegaConf
import torch as tr
import numpy as np
import lovely_tensors
from torch.utils.data import DataLoader
from torch import nn, optim
from lightning_module_enhanced.callbacks import PlotMetrics
from pytorch_lightning import Trainer
from pytorch_lightning.loggers import CSVLogger
from pytorch_lightning.callbacks import Callback

from mlexp.utils import image_write, image_add_title

from model import build_model
from gan import GANModule
from reader import build_reader, train_val_split, to_collage

lovely_tensors.monkey_patch()
tr.set_float32_matmul_precision("medium")

def print_params(model: nn.Module):
    """
    Print the mean and std of the absolute sum of all parameters in a module.
    We use to check is weights are updating
    """
    X = np.array([x.abs().sum().item() for x in model.parameters()])
    print(f"{X.mean():.4f} - {X.std():.4f}")

class GenerateImagesAtEpochEnd(Callback):
    def __init__(self, grid: tuple[int, int]):
        self.grid = grid

    @tr.no_grad()
    def on_train_epoch_end(self, trainer: Trainer, pl_module: GANModule) -> None:
        Path(f"{trainer.logger.log_dir}/imgs").mkdir(exist_ok=True, parents=True)
        img_shape = pl_module.generator.output_shape

        # generate and convert to [0:255]
        z_noise = tr.randn(np.prod(self.grid), pl_module.generator.input_shape).to(pl_module.device)
        gen_imgs: tr.Tensor = pl_module.generator(z_noise).reshape(np.prod(self.grid), *img_shape)
        gen_titles = [f"{x:.3f}" for x in pl_module.discriminator(gen_imgs).to("cpu")]
        fake_collage = to_collage(gen_imgs.to("cpu"), self.grid, gen_titles, width=100, height=100)

        # Get as many real images from the train loader (because shuffle) as the grid needs
        real_imgs = next(iter(trainer.train_dataloader))["data"]
        while len(real_imgs) < np.prod(self.grid):
            real_imgs = tr.cat([real_imgs, next(iter(trainer.train_dataloader))["data"]])
        real_imgs = real_imgs[0: np.prod(self.grid)].to(pl_module.device)
        real_titles = [f"{x:.3f}" for x in pl_module.discriminator(real_imgs).to("cpu")]
        real_collage = self.to_collage(real_imgs.to("cpu"), self.grid, real_titles, width=100, height=100)

        img_cat = np.concatenate([fake_collage, real_collage], axis=1)
        img_out = image_add_title(img_cat, f"Epoch: {trainer.current_epoch + 1}", size_px=40, top_padding=45)
        image_write(img_out, f"{trainer.logger.log_dir}/imgs/{trainer.current_epoch + 1}.png")


def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path)
    parser.add_argument("--dataset_path", type=Path, default="data",
                        help="Dataset path. If doesn't exist, will be downloaded.")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    reader, collate_fn = build_reader(cfg.data, args.dataset_path)
    train_reader, val_reader = train_val_split(reader, ratio=cfg.data.train_val_split)
    train_loader = DataLoader(train_reader, collate_fn=collate_fn, **cfg.loader_params)
    val_loader = DataLoader(val_reader, collate_fn=collate_fn, **{**cfg.loader_params, "shuffle": False})
    collage_cb = GenerateImagesAtEpochEnd(grid=cfg.collage_callback.grid)

    gan = GANModule(*build_model(cfg.model.generator, cfg.model.discriminator))
    gan.optimizer = [optim.Adam(gan.generator.parameters(), lr=0.0002),
                     optim.Adam(gan.discriminator.parameters(), lr=0.0002)]
    gan.callbacks = [PlotMetrics(), collage_cb]
    print(gan.summary)

    pl_logger = CSVLogger(save_dir=f"experiments/{args.config_path.stem}",
                          name=datetime.strftime(datetime.now(), "%Y%m%d"),
                          version=datetime.strftime(datetime.now(), "%H%M%S"))
    Trainer(logger=pl_logger, **cfg.trainer_params).fit(gan, train_loader, val_loader)

if __name__ == "__main__":
    main()
