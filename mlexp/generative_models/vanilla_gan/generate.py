#!/usr/bin/env python3

from argparse import ArgumentParser
from pathlib import Path
import matplotlib.pyplot as plt
from omegaconf import OmegaConf
import torch as tr
import numpy as np
import lovely_tensors

from train import GenerateImagesAtEpochEnd
from model import build_model

device = "cuda" if tr.cuda.is_available() else "cpu"
lovely_tensors.monkey_patch()

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path)
    parser.add_argument("--weights_path", type=Path, help="Path to the pre-trained GAN. Will only use generator")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)
    generator = build_model(cfg.model.generator, cfg.model.discriminator)[0]
    model_state = tr.load(args.weights_path, map_location="cpu")
    params = {k[len("generator."):]: v for k, v in model_state["state_dict"].items() if k.startswith("generator")}
    generator.load_state_dict(params)
    generator = generator.eval().to(device)
    print(f"Successfully loaded weights from '{args.weights_path}'")

    cb = GenerateImagesAtEpochEnd(cfg.collage_callback.grid)

    while True:
        z = tr.randn(np.prod(cb.grid), generator.input_shape).to(device)
        with tr.no_grad():
            y = generator(z)
        y_imgs = cb.to_collage((y.to("cpu").numpy() + 1) * 2, None)

        plt.figure()
        plt.axis("off")
        plt.imshow(y_imgs)
        plt.show()
        plt.clf()
        plt.close()

if __name__ == "__main__":
    main()
