"""Gan module. Training and inference algorithm based on two sub architecture: discriminator and generator."""
from typing import Dict
import torch as tr
from torch import nn
from torch.nn import functional as F
from lightning_module_enhanced import LME
from torchinfo import summary

class GANModule(LME):
    """GANModule implementation"""
    def __init__(self, generator: nn.Module, discriminator: nn.Module):
        class _GAN(nn.Module):
            def __init__(self, generator: nn.Module, discriminator: nn.Module):
                super().__init__()
                self.generator = generator
                self.discriminator = discriminator
        super().__init__(_GAN(generator, discriminator))
        self.generator = generator
        self.discriminator = discriminator
        self.criterion_fn = self.loss_fn
        self.metrics = {"g_loss": (self.loss_fn, "min"), "d_loss": (self.loss_fn, "min")}
        self.checkpoint_monitors = ["loss", "g_loss"]

    def loss_fn(self, y: tr.Tensor, gt: tr.Tensor) -> tr.Tensor:
        return F.binary_cross_entropy(y, gt)

    @property
    def summary(self):
        """Prints the summary (layers, num params, size in MB), with the help of torchinfo module."""
        if self._summary is None:
            self._summary = summary(self.base_model, verbose=0, depth=4)
        return self._summary

    def training_step(self, train_batch: Dict, batch_idx: int, *args, **kwargs):
        """Training step: returns batch training loss and metrics."""
        self._update_metrics_at_batch_end(self.model_algorithm(train_batch))

    def validation_step(self, train_batch: Dict, batch_idx: int, *args, **kwargs):
        self._update_metrics_at_batch_end(self.model_algorithm(train_batch), "val_")

    def model_algorithm(self, train_batch: Dict) -> tr.Tensor:
        x_real = train_batch["data"]
        bs = len(x_real)
        ones = tr.ones(bs).to(self.device)
        zeros = tr.zeros(bs).to(self.device)
        # using self.optimizers(), not self.optimizer. See CoreModule comment as to why this is needed with lightning.
        g_opt, d_opt = self.optimizers()

        # Generator
        g_opt.zero_grad()
        z_fake = tr.randn(bs, self.generator.input_shape).to(self.device)
        x_fake = self.generator(z_fake)
        y_fake = self.discriminator(x_fake)
        g_loss = self.loss_fn(y_fake, ones)
        if self.training:
            self.manual_backward(g_loss)
            g_opt.step()

        # Discriminator
        z_fake = tr.randn(bs, self.generator.input_shape).to(self.device)
        x_fake2: tr.Tensor = self.generator(z_fake)
        d_opt.zero_grad()
        y_real = self.discriminator(x_real)
        y_fake = self.discriminator(x_fake2.detach())
        d_loss = (self.loss_fn(y_real, ones) + self.loss_fn(y_fake, zeros)) / 2
        if self.training:
            self.manual_backward(d_loss)
            d_opt.step()

        return {"loss": d_loss + g_loss, "g_loss": g_loss, "d_loss": d_loss}
