# Vanilla Generative Adversarial Network (GAN)

## Dataset visualisation

No training required, just plots using matplotlib the dataset.

```
python dataset_plot.py --config_path cfg/mnist_conv.yaml
```

## MNIST

### Training

```
python train.py --config_path cfg/mnist_conv.yaml [--dataset_path data/]
```

During training reuslt:

<img src="results/mnist_results.png" height="300px"></img>

### Generation

```
python generate.py --config_path cfg/mnist_conv.yaml --weights_path /path/to/model.ckpt
```

## CIFAR-10

Same as above, but use `cfg/cifar_conv.yaml` for the config file.

Results using `cfgs/cifar10_conv2.yaml`:

<img src="results/cifar10_results.png" height="300px"></img>
