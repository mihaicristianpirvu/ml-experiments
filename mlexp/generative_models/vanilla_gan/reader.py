"""Builds a reader from the supported types. So far: MNIST and CIFAR-10"""
from omegaconf import DictConfig
from pathlib import Path
from typing import Callable, Tuple, Dict
from torch.utils.data import Dataset, Subset
from torchvision.datasets import MNIST, CIFAR10
import torch as tr
import numpy as np

from mlexp.utils import collage_fn, image_resize, to_image

def train_val_split(reader: Dataset, ratio: float) -> Tuple[Dataset, Dataset]:
    assert 0 < ratio < 1, ratio
    n_val = len(reader) - int(ratio * len(reader))
    train_reader, val_reader = Subset(reader, np.arange(n_val, len(reader))), Subset(reader, np.arange(n_val))
    # this is used for debugging.
    # train_reader, val_reader = Subset(reader, np.arange(n_val, n_val * 2)), Subset(reader, np.arange(n_val))
    return train_reader, val_reader

def mnist_cifar_collate_fn(batch: Tuple[tr.Tensor, tr.Tensor]) -> Dict[str, tr.Tensor]:
    """normalizes the mnist/cifar-10 batches to [-1:1]"""
    x = tr.stack([tr.from_numpy(np.array(y[0], dtype=np.float32)) for y in batch]) / 255 * 2 - 1
    gt = tr.Tensor([y[1] for y in batch]).type(tr.long)
    # add 1 channel for mnist
    x = x[..., None] if len(x.shape) == 3 else x
    return {"data": x, "labels": gt}

def to_collage(tr_imgs: tr.Tensor, grid: tuple[int, int], titles: list[str] | None = None,
               width: int = 100, height: int = 100) -> np.ndarray:
    # cut if we have more than grid_h * grid_w images
    tr_imgs = tr_imgs[0: np.prod(grid)]
    # convert to numpy, on cpu, with 3-channels and uint8 [0:255] range
    np_imgs = [to_image(img) for img in (tr_imgs + 1) * 2]
    imgs = [image_resize(img, width=width, height=height) for img in np_imgs]
    img_collage = collage_fn(imgs, grid, pad_right=5, pad_bottom=1, titles=titles, size_px=25, top_padding=20)
    return img_collage


def build_reader(cfg: DictConfig, dataset_path: Path) -> Tuple[Dataset, Callable]:
    if cfg.type == "mnist":
        return MNIST(dataset_path, **cfg.parameters), mnist_cifar_collate_fn
    if cfg.type == "cifar-10":
        return CIFAR10(dataset_path, **cfg.parameters), mnist_cifar_collate_fn

    raise ValueError(f"Unknown dataset type: {cfg.type}")
