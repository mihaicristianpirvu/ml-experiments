from typing import Tuple, List
import torch as tr
import numpy as np
from torch.nn import functional as F
from torch import nn

### Conv -- by chat gpt

class GeneratorConv(nn.Module):
    def __init__(self, input_shape: int, output_shape: Tuple[int, int, int]):
        super().__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape
        out_channels = self.output_shape[-1]

        self.convt1 = nn.ConvTranspose2d(input_shape, out_channels=128, kernel_size=7, stride=1, padding=0, bias=False)
        self.bn1 = nn.BatchNorm2d(128)
        self.convt2 = nn.ConvTranspose2d(128, out_channels=64, kernel_size=4, stride=2, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(64)
        self.convt3 = nn.ConvTranspose2d(64, out_channels=out_channels, kernel_size=4, stride=2, padding=1, bias=False)

    def _get_pad(self, y_shape: Tuple[float, float]) -> Tuple[int, int, int, int]:
        w_pad = int(self.output_shape[0] - y_shape[-2])
        h_pad = int(self.output_shape[0] - y_shape[-1])
        return w_pad // 2, w_pad // 2 + (w_pad % 2 == 1), h_pad // 2, h_pad // 2 + (h_pad % 2 == 1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        x = x.view(x.size(0), self.input_shape, 1, 1)
        y1 = F.relu(self.bn1(self.convt1(x)))
        y2 = F.relu(self.bn2(self.convt2(y1)))
        y3 = F.tanh(self.convt3(y2))
        # pad and permute to B,H,W,C
        y_out = F.pad(y3, self._get_pad(y3.shape)).permute(0, 2, 3, 1)
        return y_out


class GeneratorConv2(nn.Module):
    def __init__(self, input_shape: int, hidden_shapes: List[int], output_shape: Tuple[int, int, int]):
        """Beefier version so maybe cifar-10 learns something"""
        super().__init__()
        self.input_shape = input_shape
        self.output_shape = output_shape

        channels = [input_shape, *hidden_shapes]
        # up part
        self.convts_up = nn.ModuleList()
        self.bns_up = nn.ModuleList()
        for inc, outc in zip(channels[0:-1], channels[1:]):
            self.convts_up.append(nn.ConvTranspose2d(inc, outc, kernel_size=3, stride=1, padding=0, bias=False))
            self.bns_up.append(nn.BatchNorm2d(outc))

        channels_reverse = channels[1:][::-1]
        self.convs_down = nn.ModuleList()
        self.bns_down = nn.ModuleList()
        for inc, outc in zip(channels_reverse[0:-1], channels_reverse[1:]):
            self.convs_down.append(nn.Conv2d(inc, outc, kernel_size=3, stride=1, padding=0, bias=False))
            self.bns_down.append(nn.BatchNorm2d(outc))

        self.fc_out = nn.Linear(channels[1] * 3 * 3, np.prod(output_shape))

    def _get_pad(self, y_shape: Tuple[float, float]) -> Tuple[int, int, int, int]:
        w_pad = int(self.output_shape[0] - y_shape[-2])
        h_pad = int(self.output_shape[0] - y_shape[-1])
        return w_pad // 2, w_pad // 2 + (w_pad % 2 == 1), h_pad // 2, h_pad // 2 + (h_pad % 2 == 1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        b = x.size(0)
        # (b, h) -> (b, h, 1, 1) so we can use conv transposed to upsample
        y = x.view(b, self.input_shape, 1, 1)
        y_ups = []
        for convt, bn in zip(self.convts_up, self.bns_up):
            y_up = F.dropout2d(F.relu(bn(convt(y))), 0.2)
            y_ups.insert(0, y_up)
            y = y_up

        for y_up_res, conv, bn in zip(y_ups[1:], self.convs_down, self.bns_down):
            y_down = F.dropout2d(F.relu(bn(conv(y))), 0.2)
            y = y_up_res + y_down

        y_out = F.tanh(self.fc_out(y.reshape(b, -1)).reshape(b, *self.output_shape))
        return y_out

class DiscriminatorConv(nn.Module):
    def __init__(self, input_shape: Tuple[int, int, int], hidden_shape: List[int]):
        super().__init__()
        assert len(input_shape) == 3, input_shape
        assert len(hidden_shape) == 2, hidden_shape
        self.input_shape = input_shape
        n_dims = self.input_shape[-1]
        assert input_shape[0] == input_shape[1] and input_shape[0] in (28, 32), "Cifar or mnist-like shapes expected"

        hidden_shape = [n_dims, *hidden_shape]
        self.conv1 = nn.Conv2d(in_channels=n_dims, out_channels=hidden_shape[1], kernel_size=4,
                                stride=2, padding=1, bias=False)
        self.conv2 = nn.Conv2d(in_channels=hidden_shape[1], out_channels=hidden_shape[2], kernel_size=4,
                               stride=2, padding=1, bias=False)
        self.bn2 = nn.BatchNorm2d(hidden_shape[2])
        # 8 for 32x32 and 7 for 28x28 to get a (1, 1, 1) channel output at the end for classification
        k_size = input_shape[0] // 4
        self.conv3 = nn.Conv2d(in_channels=hidden_shape[2], out_channels=1, kernel_size=k_size,
                               stride=1, padding=0, bias=False)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        assert len(x.shape) == 4, f"Expected (B, H, W, C), got {x.shape}"
        x = x.permute(0, 3, 1, 2)
        y1 = F.leaky_relu(self.conv1(x), 0.2)
        y2 = F.leaky_relu(self.bn2(self.conv2(y1)), 0.2)
        y3 = tr.sigmoid(self.conv3(y2))
        # (B, 1, 1, 1) expected output
        y_out = y3.squeeze()
        return y_out


class DiscriminatorConv2(nn.Module):
    def __init__(self, input_shape: Tuple[int, int, int], hidden_shape_1: Tuple[int], hidden_shape_2: Tuple[int]):
        super().__init__()
        assert len(input_shape) == 3, input_shape
        self.input_shape = input_shape
        n_dims = self.input_shape[-1]
        assert input_shape[0] == input_shape[1] and input_shape[0] in (28, 32), "Cifar or mnist-like shapes expected"

        self.conv1 = nn.Conv2d(n_dims, hidden_shape_1[0], kernel_size=3, stride=1, padding=1, bias=False)

        self.convs = nn.ModuleList()
        self.bns = nn.ModuleList()
        for inc, outc in zip(hidden_shape_1[0: -1], hidden_shape_1[1:]):
            self.convs.append(nn.Conv2d(inc, outc, kernel_size=3, stride=1, padding=1, bias=False))
            self.bns.append(nn.BatchNorm2d(outc))

        assert hidden_shape_2[-1] > 1
        self.convs_part2 = nn.ModuleList()
        self.bns_part2 = nn.ModuleList()
        for inc, outc in zip(hidden_shape_2[0: -1], hidden_shape_2[1:]):
            self.convs_part2.append(nn.Conv2d(inc, outc, kernel_size=3, stride=1, padding=0, bias=False))
            self.bns_part2.append(nn.BatchNorm2d(outc))

        # output of the conv_part2 blocks is 32 - 4 *2 (for 5 entries) = 24,24,8 (8 being last)
        fc_dim = (self.input_shape[0] - len(self.convs_part2) * 2) * (self.input_shape[1] - len(self.convs_part2) * 2)
        self.fc_out_1 = nn.Linear(fc_dim * hidden_shape_2[-1], fc_dim * hidden_shape_2[-1] // 2)
        self.fc_out_2 = nn.Linear(fc_dim * hidden_shape_2[-1] // 2, 1)

    def forward(self, x: tr.Tensor) -> tr.Tensor:
        assert len(x.shape) == 4, f"Expected (B, H, W, C), got {x.shape}"
        b = x.shape[0]
        y = x.permute(0, 3, 1, 2)
        y = F.leaky_relu(self.conv1(y), 0.2)
        for bn, conv in zip(self.bns, self.convs):
            y = F.dropout2d(F.leaky_relu(bn(conv(y)), 0.2), 0.2) + y

        for bn, conv in zip(self.bns_part2, self.convs_part2):
            y = F.dropout2d(F.leaky_relu(bn(conv(y)), 0.2), 0.2)

        y = F.leaky_relu(self.fc_out_1(y.reshape(b, -1)), 0.2)
        y = tr.sigmoid(self.fc_out_2(y))
        return y.squeeze()
