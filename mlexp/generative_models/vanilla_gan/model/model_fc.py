import torch as tr
from typing import Tuple, List
import numpy as np
from torch.nn import functional as F
from torch import nn

### FC -- by me

class GeneratorFC(nn.Module):
    def __init__(self, input_shape: int, hidden_shapes: List[int], output_shape: Tuple[int, int]):
        super().__init__()
        self.input_shape = input_shape
        self.output_shape = tuple(output_shape)
        hidden_shapes = [input_shape, *hidden_shapes]
        self.fcs = nn.ModuleList([nn.Linear(hidden_shapes[i], hidden_shapes[i + 1])
                                  for i in range(len(hidden_shapes) - 1)])
        self.bns = nn.ModuleList([nn.BatchNorm1d(h) for h in hidden_shapes[1:]])
        self.fc_out = nn.Linear(hidden_shapes[-1], np.prod(output_shape))

    # (b,i) => (b,h,w)
    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = x
        for bn, fc in zip(self.bns, self.fcs):
            y = F.leaky_relu(fc(y), 0.2)
            y = bn(y)
            y = F.dropout(y, 0.2)
        y_out = F.tanh(self.fc_out(y)).reshape(-1, *self.output_shape)
        return y_out

class DiscriminatorFC(nn.Module):
    def __init__(self, input_shape: Tuple[int, int], hidden_shapes: List[int]):
        super().__init__()
        self.input_shape = tuple(input_shape)
        hidden_shapes = [np.prod(input_shape), *hidden_shapes]
        self.fcs = nn.ModuleList([nn.Linear(hidden_shapes[i], hidden_shapes[i + 1])
                                  for i in range(len(hidden_shapes) - 1)])
        self.bns = nn.ModuleList([nn.BatchNorm1d(h) for h in hidden_shapes[0:-1]])
        self.fc_out = nn.Linear(hidden_shapes[-1], 1)

    # (b,h,w) => (b,)
    def forward(self, x: tr.Tensor) -> tr.Tensor:
        y = x.reshape(-1, np.prod(self.input_shape))
        for bn, fc in zip(self.fcs, self.bns):
            y = F.leaky_relu(fc(y), 0.2)
            y = bn(y)
            y = F.dropout(y, 0.2)
        y_out = tr.sigmoid(self.fc_out(y)).squeeze()
        return y_out
