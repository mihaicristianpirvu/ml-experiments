from omegaconf import DictConfig
from typing import Tuple
from torch import nn

from .model_fc import GeneratorFC, DiscriminatorFC
from .model_conv import GeneratorConv, DiscriminatorConv, GeneratorConv2, DiscriminatorConv2

def build_model(generator_cfg: DictConfig, discriminator_cfg: DictConfig) -> Tuple[nn.Module, nn.Module]:
    generator = {
        "GeneratorFC": GeneratorFC,
        "GeneratorConv": GeneratorConv,
        "GeneratorConv2": GeneratorConv2,
    }[generator_cfg.type](**generator_cfg.parameters)
    discriminator = {
        "DiscriminatorFC": DiscriminatorFC,
        "DiscriminatorConv": DiscriminatorConv,
        "DiscriminatorConv2": DiscriminatorConv2,
    }[discriminator_cfg.type](**discriminator_cfg.parameters)
    return generator, discriminator
