#!/usr/bin/env python3
from argparse import ArgumentParser
from pathlib import Path
from omegaconf import OmegaConf
from torch.utils.data import DataLoader

from reader import build_reader, train_val_split, to_collage

def get_args():
    parser = ArgumentParser()
    parser.add_argument("--config_path", type=Path)
    parser.add_argument("--dataset_path", type=Path, default="data",
                        help="Dataset path. If doesn't exist, will be downloaded.")
    args = parser.parse_args()
    return args

def main():
    args = get_args()
    cfg = OmegaConf.load(args.config_path)

    reader, collate_fn = build_reader(cfg.data, args.dataset_path)
    train_reader = train_val_split(reader, ratio=cfg.data.train_val_split)[0]
    train_loader = DataLoader(train_reader, collate_fn=collate_fn, **cfg.loader_params)

    while True:
        it = iter(train_loader)
        imgs = to_collage(next(it)["data"], grid=cfg.collage_callback.grid)
        import matplotlib.pyplot as plt
        plt.imshow(imgs)
        plt.show()

if __name__ == "__main__":
    main()
