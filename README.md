# ML-Experiments

All the submodules from here are standalone (no circular dependencies) and usually have a single main point to run
the training or inference. They should also have a README file describing how to run them as well as a requirements.txt
file if needed.

The top level folders are high level concepts (i.e. classification models, gnns etc.), the next level are specific
standalone implementations.

## Algorithms implemented

### Classification Models
- One [subproject](classification-models/) that combines CNN (MobileNetV3), ViT and a fully connected model. Support for MNIST and CIFAR10 so far.

### Language Models
- [char-lms](language-models/char-lms) - char level language modeling on the shakespeare dataset. 3 models:
ngram, rnn and transformer
- [lm-transformers](language-models/lm-transformers) - Token level language modeling (char and gpt-2 via tiktoken).
Optimized transformer network using flash attention as opposed to the one in `char-lms`, which has a hand coded multi
head attention

### Generative Models
- [vanilla-gan](generative-models/vanilla-gan) - implementation of the classic GAN (conv network) on MNIST and CIFAR-10
- [vanilla-vae](generative-models/vanilla-vae) - implementation of the classical VAE on MNIST and CIFAR-10. Note: still
doesn' generate good results!

### Graph Neural Networks
- [graph-convolutional-network](graph-neural-networks/gcn) - Graph convolutional network (GCN) on Cora dataset using
torchgeometric & nwgraph. Bonus: Bag of words model for baseline.

### Pretraining algorithms
- [masked-auto-encoder](pretraining/mae/) - Masked Auto Encoder implementation for ViT. Shares code base for vit model
and reader with the one implemented in [classification-models](classification-models).

### Others
Note: This is where we'll fit some very specific algorithms that should work on top of any neural network architecture.
- [mnist-semisupervised](others/semisupervised-simple) - extension of the mnist-classification code to support the
training  a subset of the mnist data, and then, using the seed supervised network, to generate pseudo-labels and
retrain  again on the supervised + pseudolabels resulting in semi-supervised learning
- [mnist-ensemble-graph](others/ngc-ensemble-graph) - a simple ngc-like code with 5 nodes: 4 quadrants of an mnist
image and the entire image talking to each other for a few iterations before making a graph-level readout prediction.

## Algorithms that I want to do or WIP.

- conditional-gan - A conditional GAN on MNIST and CIFAR-10
- sfmlearner - implementation of sfm learner on some (single) video dataset
- low-rank-adaptation - implementation of LoRA to fine tune a model (LM or not)
- speculative-autogregression - implementation of speculative autoregression using a small model and a big model to
optimize the autoregression part.
- word2vec
- other pretraining algorithms: VicReg, BYOL etc.
- TODO: new ideas?
